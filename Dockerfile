FROM adoptopenjdk/openjdk11:alpine-slim
COPY build/libs/lodgings-*-all.jar lodgings.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "lodgings.jar"]

# cd <root of the project> and...
# sudo docker build . -t lodgings:<version>
# sudo docker run --network=host lodgings (-d for daemon mode)
# (then, for "serious" production, etc - probably specify env variables for database address and other config
# instead of host network)