# Lodgings

Once upon a time Peter, Tom, and Anna lived in a rented flat.
They agreed that they use some common things (equipment and consumables) equally,
and so, should pay for them equally.
But how to easily manage that, when some people may buy things, when others do not,
some people may buy more things than others and at different prices, etc.

**Lodgings** to the rescue! :)

Briefly, for a given month:

- Peter buys washing liquid for 5zł
- Anna buys paper towels for 4zł
- Tom is a student, has finals, doesn't care, whatever

The total is 9zł - naturally that means everyone should pay 3zł
at the end of the month (effectively).

Lodgings will tell us that:
- Tom pays Anna 1zł
- Tom pays Peter 2zł

Done. This is the main function of the app, but I plan to introduce other conveniences,
also feel free to check wiki and issues :)