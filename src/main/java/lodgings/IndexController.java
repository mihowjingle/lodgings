package lodgings;

import io.micronaut.core.io.ResourceResolver;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.server.types.files.StreamedFile;

import javax.inject.Inject;
import java.util.Optional;

@Controller
public class IndexController {

    @Inject
    ResourceResolver resourceResolver;

    @Get(value = "/{path:[^.]*}", produces = MediaType.TEXT_HTML)
    public Optional<StreamedFile> index(String path) {
        return resourceResolver.getResource("classpath:static/index.html")
                .map(StreamedFile::new);
    }
}
