package lodgings.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Queries {

    /**
     * For Ebean pagination, ordering etc. (but not fetching) Also reflects the fact, that there has to be a db join.
     *
     * @return a "path" to the target property
     */
    public static String join(String... chunks) {
        return String.join(".", chunks);
    }

    /**
     * For Ebean selecting specific properties (columns), partial query
     *
     * @return a list of comma-separated properties which then Ebean interprets into db columns
     */
    public static String properties(String... properties) {
        return String.join(", ", properties);
    }
}
