package lodgings.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * Experiment. For example I find "not(x)" more readable, than "!x" or "! x" ...I think?
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SyntacticSugar {

    public static boolean not(boolean b) {
        return !b;
    }

    public static boolean nullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> T takeNotNullOrElse(T primary, T secondary) {
        if (primary != null) return primary;
        return secondary;
    }
}
