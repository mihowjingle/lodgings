package lodgings.common.data;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnull;

/**
 * I always struggle with using Comparable, the notation is so unintuitive.
 * Made this little utility for myself to make this issue more "human-readable".
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Comparison {

    public enum Operator {
        EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) == 0;
            }
        },
        NOT_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) != 0;
            }
        },
        GREATER_THAN {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) > 0;
            }
        },
        GREATER_THAN_OR_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) >= 0;
            }
        },
        LESS_THAN {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) < 0;
            }
        },
        LESS_THAN_OR_EQUAL_TO {
            @Override
            <T extends Comparable<T>> boolean apply(T left, T right) {
                return left.compareTo(right) <= 0;
            }
        };

        abstract <T extends Comparable<T>> boolean apply(T left, T right);
    }

    public static <T extends Comparable<T>> boolean is(@Nonnull T left, @Nonnull Operator operator, @Nonnull T right) {
        return operator.apply(left, right);
    }
}
