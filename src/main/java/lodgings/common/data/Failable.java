package lodgings.common.data;

import io.micronaut.http.HttpResponse;
import lodgings.common.data.errors.Error;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * This class represents the result of an action, that may fail, such as various validated CRUD actions.
 * It contains the actual result, or a list of errors.
 *
 * @param <S> as in "success" or "successful", the type of the actual result of the operation, when it is successful
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Failable<S> { // "sealed" and pattern matching, or at least the latter, will make this nice

    private final S body;
    private final List<Error> errors;

    public static <S> Failable<S> success(S body) {
        return new Failable<>(body, null);
    }

    public static Failable<Void> success() {
        return new Failable<>(null, null);
    }

    public static <S> Failable<S> error(List<Error> errors) {
        return new Failable<>(null, errors);
    }

    public static <S> Failable<S> error(Error error) {
        return new Failable<>(null, List.of(error));
    }

    public boolean isSuccess() {
        return errors == null;
    }

    public boolean isError() {
        return errors != null;
    }

    public HttpResponse<?> toHttpResponse() {
        if (this.isError()) {
            return HttpResponse.badRequest(errors);
        }
        return HttpResponse.ok(body);
    }
}
