package lodgings.common.data.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

    /**
     * The key, which the error is identified by on the frontend, should also fully explain what's wrong
     */
    private final String key;

    /**
     * The acceptable value, if any, otherwise null. !!! So null doesn't literally mean that null is acceptable, but "n/a"
     *
     * In case of "something too short" validation error, this should be the minimum length, etc.
     */
    private final Object acceptableValue;

    public Error(String key, Object acceptableValue) {
        this.key = key;
        this.acceptableValue = acceptableValue;
    }

    public Error(String key) {
        this.key = key;
        this.acceptableValue = null;
    }

    public static final Error ID_MISSING = new Error("ID_MISSING");
}
