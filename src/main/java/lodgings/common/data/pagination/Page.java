package lodgings.common.data.pagination;

import io.ebean.PagedList;
import lombok.Getter;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * @param <P> as in "projection"
 */
@Getter
public class Page<P> {

    private final List<P> data;
    private final int pageIndex;
    private final int pageSize;
    private final int totalCount;
    private final int totalPageCount;

    /**
     * @param entities objects to be mapped
     * @param projector mapping function
     * @param <E> as in "entity"
     */
    public <E> Page(PagedList<E> entities, Function<E, P> projector) {
        data = entities.getList().stream().map(projector).collect(toList());
        pageIndex = entities.getPageIndex();
        pageSize = entities.getPageSize();
        totalCount = entities.getTotalCount();
        totalPageCount = entities.getTotalPageCount();
    }
}
