package lodgings.common.data.pagination;

import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.entities.Purpose;
import lodgings.domain.users.data.entities.User;

import java.util.Set;

import static lodgings.common.Queries.join;

public enum PagedEntity {
    USER(
            User.Fields.id,
            User.Fields.name,
            User.Fields.login,
            User.Fields.allowedToLogin,
            User.Fields.autoIncludeInNextSettlement,
            User.Fields.passwordResetRequired
    ),
    ITEM(
            Item.Fields.id,
            Item.Fields.name,
            Item.Fields.assessable,
            join(Item.Fields.purpose, Purpose.Fields.name),
            join(Item.Fields.lastModifiedBy, User.Fields.name)
    );

    private final Set<String> sortablePropertyNames;

    PagedEntity(String... sortablePropertyNames) {
        this.sortablePropertyNames = Set.of(sortablePropertyNames);
    }

    public boolean isSortableBy(String property) {
        return property != null && sortablePropertyNames.contains(property);
    }
}
