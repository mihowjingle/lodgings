package lodgings.common.data.pagination;

import lodgings.common.data.errors.Error;

public class PaginationErrors {
    public static final Error NEGATIVE_PAGE_INDEX = new Error("NEGATIVE_PAGE_INDEX", 0);
    public static final Error NON_POSITIVE_PAGE_SIZE = new Error("NON_POSITIVE_PAGE_SIZE", 1);
    public static final Error UNRECOGNIZED_SORT_PROPERTY = new Error("UNRECOGNIZED_SORT_PROPERTY");
    public static final Error UNRECOGNIZED_SORT_ORDER = new Error("UNRECOGNIZED_SORT_ORDER", PaginationRequest.SORT_ORDERS);
}
