package lodgings.common.data.pagination;

import io.ebean.OrderBy;
import io.micronaut.core.annotation.Creator;
import io.micronaut.core.annotation.Introspected;
import lodgings.common.data.errors.Error;

import io.micronaut.core.annotation.Nullable;
import java.util.LinkedList;
import java.util.List;

import static lodgings.common.SyntacticSugar.takeNotNullOrElse;
import static lodgings.common.SyntacticSugar.not;

@Introspected
public class PaginationRequest {

    private static final int DEFAULT_INDEX = 0;
    private static final int DEFAULT_SIZE = 10;
    public static final String ASCENDING = "ASCENDING";
    public static final String DESCENDING = "DESCENDING";

    public static final String SORT_ORDERS = String.join(", ", List.of(ASCENDING, DESCENDING));

    private final Integer index;
    private final Integer size;
    private final String property;
    private final String order;

    @Creator
    public PaginationRequest(@Nullable Integer index,
                             @Nullable Integer size,
                             @Nullable String property,
                             @Nullable String order) {
        this.index = index;
        this.size = size;
        this.property = property;
        this.order = order;
    }

    public int getFirstRow() {
        return takeNotNullOrElse(index, DEFAULT_INDEX) * getSize();
    }

    public Integer getSize() {
        return takeNotNullOrElse(size, DEFAULT_SIZE);
    }

    public <T> OrderBy<T> getOrderBy() {

        if (order == null || property == null) return null;

        final var orderBy = new OrderBy<T>();
        if (order.equals(ASCENDING)) {
            orderBy.asc(property);
        } else if (order.equals(DESCENDING)) {
            orderBy.desc(property);
        } else {
            throw new IllegalStateException("Unrecognized order! Validation failed, or never invoked?");
        }
        return orderBy;
    }

    public List<Error> validateFor(PagedEntity entity) {
        final var errors = new LinkedList<Error>();
        if (index != null && index < 0) {
            errors.add(PaginationErrors.NEGATIVE_PAGE_INDEX);
        }
        if (size != null && size <= 0) {
            errors.add(PaginationErrors.NON_POSITIVE_PAGE_SIZE);
        }
        if (order != null && not(order.equals(ASCENDING)) && not(order.equals(DESCENDING))) {
            errors.add(PaginationErrors.UNRECOGNIZED_SORT_ORDER);
        }
        if (property != null && not(entity.isSortableBy(property))) {
            errors.add(PaginationErrors.UNRECOGNIZED_SORT_PROPERTY);
        }
        return errors;
    }
}
