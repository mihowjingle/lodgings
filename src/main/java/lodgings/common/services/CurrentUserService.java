package lodgings.common.services;

import io.micronaut.security.utils.SecurityService;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class CurrentUserService {

    private final SecurityService securityService;
    private final UserRepository userRepository;

    public User get() {
        return securityService.username().map(
                login -> userRepository.findByLogin(login).orElseThrow(
                        () -> new IllegalStateException("Logged-in user not found in the database! Breach, or just got deleted?")
                )
        ).orElseThrow(() -> new IllegalStateException("User is not logged in!"));
    }
}
