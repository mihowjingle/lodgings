package lodgings.common.services;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Random;

@Slf4j
@Singleton
public class ExceptionTranslator implements ExceptionHandler<Throwable, HttpResponse<?>> {

    private final Random random = new Random();

    @Override
    public HttpResponse<?> handle(HttpRequest request, Throwable throwable) {
        String code = generateLogCode();
        log.error("Unknown error! Code: {}", code, throwable);
        return HttpResponse.serverError(new Response(code));
    }

    @Data
    private static final class Response {
        private final String logCode;
    }

    private String generateLogCode() {
        int length = 12;
        String source = "1234567890abcdefghijkmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(source.charAt(random.nextInt(source.length())));
        }
        return sb.toString();
    }
}


