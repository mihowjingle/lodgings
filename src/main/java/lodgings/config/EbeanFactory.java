package lodgings.config;

import io.ebean.Database;
import io.ebean.DatabaseFactory;
import io.ebean.config.DatabaseConfig;
import io.micronaut.context.annotation.Factory;

import javax.inject.Singleton;
import javax.sql.DataSource;

@Factory
public class EbeanFactory {

    @Singleton
    Database database(DataSource dataSource) {
        DatabaseConfig conf = new DatabaseConfig();
        conf.setDataSource(dataSource);
        conf.setName("ebean");
        conf.setDefaultServer(true);
        conf.setDisableClasspathSearch(false);
        conf.addPackage("lodgings.domain");
        conf.setExpressionNativeIlike(true);
        return DatabaseFactory.create(conf);
    }
}
