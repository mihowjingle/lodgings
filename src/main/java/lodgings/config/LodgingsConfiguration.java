package lodgings.config;

import io.micronaut.context.annotation.ConfigurationProperties;
import lombok.Getter;

@Getter
@ConfigurationProperties("lodgings")
public class LodgingsConfiguration {
    int passwordHashingStrength;
}
