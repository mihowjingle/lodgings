package lodgings.config.security;

import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.*;
import io.reactivex.Single;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.entities.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class LodgingsAuthenticationProvider implements AuthenticationProvider {

    private final UserForAuthenticationRetriever userForAuthenticationRetriever;

    @Override
    public Publisher<AuthenticationResponse> authenticate(HttpRequest<?> httpRequest, AuthenticationRequest<?, ?> authenticationRequest) {
        return Single.<AuthenticationResponse>create(emitter -> {
            String identity = (String) authenticationRequest.getIdentity();
            String secret = (String) authenticationRequest.getSecret();
            log.debug("Request to authenticate user with login: {}.", identity);
            Optional<User> maybeUser = userForAuthenticationRetriever.get(identity);
            if (maybeUser.isPresent() && maybeUser.get().isAllowedToLogin() && maybeUser.get().passwordMatches(secret)) {
                User user = maybeUser.get();
                log.debug("Successfully authenticated user with login: {}.", identity);
                Set<String> roles;
                if (user.isPasswordResetRequired()) {
                    roles = Set.of(Role.RESET_PASSWORD);
                } else {
                    roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toSet());
                }
                emitter.onSuccess(new LodgingsUserDetails(user.getId(), user.getName(), identity, roles));
            } else {
                log.debug("Failed to authenticate user with login: {}!", identity);
                emitter.onError(new AuthenticationException(new AuthenticationFailed()));
            }
        }).toFlowable();
    }
}
