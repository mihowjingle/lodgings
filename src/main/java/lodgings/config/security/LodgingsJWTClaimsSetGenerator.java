package lodgings.config.security;

import com.nimbusds.jwt.JWTClaimsSet;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.token.config.TokenConfiguration;
import io.micronaut.security.token.jwt.generator.claims.ClaimsAudienceProvider;
import io.micronaut.security.token.jwt.generator.claims.JWTClaimsSetGenerator;
import io.micronaut.security.token.jwt.generator.claims.JwtIdGenerator;

import io.micronaut.core.annotation.Nullable;
import javax.inject.Singleton;

@Singleton
@Replaces(JWTClaimsSetGenerator.class)
public class LodgingsJWTClaimsSetGenerator extends JWTClaimsSetGenerator {

    public LodgingsJWTClaimsSetGenerator(
            TokenConfiguration tokenConfiguration,
            @Nullable JwtIdGenerator jwtIdGenerator,
            @Nullable ClaimsAudienceProvider claimsAudienceProvider,
            ApplicationConfiguration applicationConfiguration
    ) {
        super(tokenConfiguration, jwtIdGenerator, claimsAudienceProvider, applicationConfiguration);
    }

    @Override
    protected void populateWithUserDetails(JWTClaimsSet.Builder builder, UserDetails userDetails) {
        super.populateWithUserDetails(builder, userDetails);
        if (userDetails instanceof LodgingsUserDetails) {
            builder.claim("name", ((LodgingsUserDetails) userDetails).getName());
            builder.claim("id", ((LodgingsUserDetails) userDetails).getId());
        } else {
            throw new IllegalStateException("Expected LodgingsUserDetails type!");
        }
    }
}
