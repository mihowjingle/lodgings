package lodgings.config.security;

import io.micronaut.security.authentication.UserDetails;
import lombok.Getter;

import java.util.Collection;

@Getter
public class LodgingsUserDetails extends UserDetails {

    private final Long id;
    private final String name;

    LodgingsUserDetails(Long id, String name, String login, Collection<String> roles) {
        super(login, roles);
        this.id = id;
        this.name = name;
    }
}
