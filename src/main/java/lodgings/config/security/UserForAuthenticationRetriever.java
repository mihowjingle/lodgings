package lodgings.config.security;

import io.ebean.annotation.Transactional;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserForAuthenticationRetriever {

    private final UserRepository userRepository;

    public Optional<User> get(String identity) {
        return userRepository.findForAuthentication(identity);
    }
}
