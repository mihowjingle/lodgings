package lodgings.domain.expenses.data.entities;

import lodgings.domain.items.data.entities.Item;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.users.data.entities.User;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.YearMonth;

@Data
@Entity
@FieldNameConstants
@Table(name = "expenses")
public class Expense {

    public static final int MAX_PRICE_DECIMAL_DIGITS = 2;
    public static final BigDecimal MAX_PRICE = new BigDecimal("9999.99");
    public static final BigDecimal MIN_PRICE = new BigDecimal("0.01");

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "when_bought")
    private LocalDateTime whenBought;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    private User buyer;

    @ManyToOne
    @JoinColumn(name = "settlement_id")
    private Settlement settlement;

    public Expense(
            BigDecimal totalPrice,
            LocalDateTime whenBought,
            Item item,
            User buyer,
            Settlement settlement
    ) {
        this.totalPrice = totalPrice;
        this.whenBought = whenBought;
        this.item = item;
        this.buyer = buyer;
        this.settlement = settlement;
        settlement.getExpenses().add(this);
    }

    public boolean isFromBeforeCurrentMonth() {
        return settlement.getYearMonth().isBefore(YearMonth.now());
    }
}
