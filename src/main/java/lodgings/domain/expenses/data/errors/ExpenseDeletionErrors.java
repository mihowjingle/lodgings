package lodgings.domain.expenses.data.errors;

import lodgings.common.data.errors.Error;

public class ExpenseDeletionErrors {
    public static final Error ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS = new Error(
            "ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS"
    );
    public static final Error EXPENSE_NOT_FOUND_CANNOT_DETERMINE_SETTLEMENT = new Error(
            "EXPENSE_NOT_FOUND_CANNOT_DETERMINE_SETTLEMENT"
    );
}
