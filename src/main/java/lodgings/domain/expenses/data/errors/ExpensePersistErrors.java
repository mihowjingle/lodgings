package lodgings.domain.expenses.data.errors;

import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.entities.Expense;

public class ExpensePersistErrors {
    public static final Error TOTAL_PRICE_MISSING = new Error("TOTAL_PRICE_MISSING");
    public static final Error TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS = new Error("TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS", Expense.MAX_PRICE_DECIMAL_DIGITS);
    public static final Error TOTAL_PRICE_TOO_HIGH = new Error("TOTAL_PRICE_TOO_HIGH", Expense.MAX_PRICE);
    public static final Error TOTAL_PRICE_TOO_LOW = new Error("TOTAL_PRICE_TOO_LOW", Expense.MIN_PRICE);
    public static final Error ITEM_NAME_MISSING = new Error("ITEM_NAME_MISSING");
    public static final Error ITEM_NOT_FOUND = new Error("ITEM_NOT_FOUND");
}
