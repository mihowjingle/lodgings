package lodgings.domain.expenses.data.errors;

import lodgings.common.data.errors.Error;

public class ExpenseUpdateErrors {
    public static final Error EXPENSE_NOT_FOUND = new Error("EXPENSE_NOT_FOUND");
    public static final Error ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS = new Error("ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS");
}
