package lodgings.domain.expenses.data.projections;

import lodgings.domain.expenses.data.entities.Expense;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ExpenseForUpdateProjection {
    private final Long id;
    private final BigDecimal totalPrice;
    private final LocalDateTime whenBought;
    private final String item;
    private final List<String> availableItems;

    public ExpenseForUpdateProjection(Expense expense, List<String> itemNames) {
        this.id = expense.getId();
        this.totalPrice = expense.getTotalPrice();
        this.whenBought = expense.getWhenBought();
        this.item = expense.getItem().getName();
        this.availableItems = itemNames;
    }
}
