package lodgings.domain.expenses.data.projections;

import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.settlement.data.projections.SettlementUserProjection;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ExpenseProjection {
    private final Long id;
    private final BigDecimal totalPrice;
    private final LocalDateTime whenBought;
    private final String item;
    private final SettlementUserProjection buyer;

    public ExpenseProjection(Expense expense) {
        this.id = expense.getId();
        this.totalPrice = expense.getTotalPrice();
        this.whenBought = expense.getWhenBought();
        this.item = expense.getItem().getName();
        this.buyer = new SettlementUserProjection(expense.getBuyer());
    }
}
