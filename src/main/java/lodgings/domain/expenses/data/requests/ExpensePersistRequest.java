package lodgings.domain.expenses.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ExpensePersistRequest {
    private final BigDecimal totalPrice;
    private final String itemName;

    @JsonCreator
    public ExpensePersistRequest(
            @JsonProperty("totalPrice") BigDecimal totalPrice,
            @JsonProperty("itemName") String itemName
    ) {
        this.totalPrice = totalPrice;
        this.itemName = itemName;
    }
}
