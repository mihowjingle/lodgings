package lodgings.domain.expenses.services;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lodgings.domain.expenses.data.projections.ExpenseForUpdateProjection;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;
import lodgings.domain.expenses.services.scenarios.ExpenseCreationHandler;
import lodgings.domain.expenses.services.scenarios.ExpenseDeletionHandler;
import lodgings.domain.expenses.services.scenarios.ExpenseUpdateHandler;
import lodgings.domain.expenses.services.scenarios.ViewExpenseForUpdateHandler;
import lodgings.domain.users.data.entities.Role;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
@Controller("/api/expenses")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class ExpenseController {

    private final ExpenseCreationHandler creationHandler;
    private final ViewExpenseForUpdateHandler viewExpenseForUpdateHandler;
    private final ExpenseDeletionHandler deletionHandler;
    private final ExpenseUpdateHandler updateHandler;

    @Post
    @Secured(Role.USER)
    public HttpResponse<?> save(@Body ExpensePersistRequest request) {
        return creationHandler.handle(request).toHttpResponse();
    }

    @Get("/{id}/for-update")
    @Secured(Role.USER)
    public Optional<ExpenseForUpdateProjection> viewOneForUpdate(Long id) {
        return viewExpenseForUpdateHandler.handle(id);
    }

    @Delete("/{id}")
    @Secured(Role.USER)
    public HttpResponse<?> delete(Long id) {
        return deletionHandler.handle(id).toHttpResponse();
    }

    @Put("/{id}")
    @Secured(Role.USER)
    public HttpResponse<?> update(Long id, @Body ExpensePersistRequest request) {
        return updateHandler.handle(id, request).toHttpResponse();
    }
}