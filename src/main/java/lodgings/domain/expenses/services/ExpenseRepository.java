package lodgings.domain.expenses.services;

import io.ebean.Database;
import io.ebean.annotation.Transactional;
import io.ebean.annotation.TxType;
import lodgings.domain.expenses.data.entities.Expense;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;

@Singleton
@RequiredArgsConstructor
@Transactional(type = TxType.MANDATORY)
public class ExpenseRepository {

    private final Database db;

    public Expense save(Expense expense) {
        db.save(expense);
        return expense;
    }

    public Optional<Expense> findById(Long id) {
        return db.find(Expense.class).where().idEq(id).findOneOrEmpty();
    }

    public Optional<Expense> findByIdSelectWhenBought(Long id) {
        return db.find(Expense.class).select(Expense.Fields.whenBought).where().idEq(id).findOneOrEmpty();
    }

    public void delete(Expense expense) {
        db.delete(expense);
    }

    public boolean noExpenses(YearMonth month) {
        LocalDateTime start = month.atDay(1).atStartOfDay();
        LocalDateTime end = month.plusMonths(1).atDay(1).atStartOfDay();
        return not(
                db.find(Expense.class)
                        .where()
                        .between(Expense.Fields.whenBought, start, end)
                        .exists()
        );
    }
}
