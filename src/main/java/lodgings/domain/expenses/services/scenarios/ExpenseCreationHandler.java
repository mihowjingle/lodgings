package lodgings.domain.expenses.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.common.services.CurrentUserService;
import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;
import lodgings.domain.expenses.services.ExpenseRepository;
import lodgings.domain.expenses.services.validators.ExpensePersistValidator;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.SettlementRepository;
import lodgings.domain.settlement.services.SettlementViewer;
import lodgings.domain.users.data.entities.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.data.Failable.error;
import static lodgings.common.data.Failable.success;
import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.ITEM_NOT_FOUND;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class ExpenseCreationHandler {

    private final ExpenseRepository expenseRepository;
    private final SettlementViewer settlementViewer;
    private final ItemRepository itemRepository;
    private final CurrentUserService currentUserService;
    private final ExpensePersistValidator expensePersistValidator;
    private final SettlementRepository settlementRepository;

    public Failable<SettlementProjection> handle(ExpensePersistRequest request) {
        log.debug("Request to create and save expense of item with name: {}", request.getItemName());
        List<Error> errors = expensePersistValidator.validate(request);
        if (not(errors.isEmpty())) {
            return error(errors);
        }
        final Optional<Item> maybeItem = itemRepository.findItemByName(request.getItemName());
        if (maybeItem.isEmpty()) {
            return error(ITEM_NOT_FOUND);
        }
        Item item = maybeItem.get();

        final User currentUser = currentUserService.get();
        Settlement settlement = settlementRepository.findSettlementFetchUsersAndExpenses(YearMonth.now());
        var expense = new Expense(
                request.getTotalPrice(),
                LocalDateTime.now(),
                item,
                currentUser,
                settlement
        );
        expenseRepository.save(expense);
        return success(settlementViewer.get(settlement));
    }
}
