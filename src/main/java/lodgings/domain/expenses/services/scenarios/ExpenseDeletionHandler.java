package lodgings.domain.expenses.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.expenses.services.ExpenseRepository;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.SettlementViewer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Optional;

import static lodgings.domain.expenses.data.errors.ExpenseDeletionErrors.ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS;
import static lodgings.domain.expenses.data.errors.ExpenseDeletionErrors.EXPENSE_NOT_FOUND_CANNOT_DETERMINE_SETTLEMENT;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class ExpenseDeletionHandler {

    private final ExpenseRepository expenseRepository;
    private final SettlementViewer settlementViewer;

    public Failable<SettlementProjection> handle(Long id) {
        log.debug("Request to delete expense with id: {}", id);
        Optional<Expense> optionalExpense = expenseRepository.findByIdSelectWhenBought(id);
        if (optionalExpense.isPresent()) {
            Expense expense = optionalExpense.get();
            if (expense.isFromBeforeCurrentMonth()) {
                return Failable.error(ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS);
            }
            expenseRepository.delete(expense);
            return Failable.success(settlementViewer.get(expense.getSettlement()));
        }
        return Failable.error(EXPENSE_NOT_FOUND_CANNOT_DETERMINE_SETTLEMENT);
    }
}
