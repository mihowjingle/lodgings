package lodgings.domain.expenses.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;
import lodgings.domain.expenses.services.ExpenseRepository;
import lodgings.domain.expenses.services.validators.ExpensePersistValidator;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.SettlementViewer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.data.Failable.success;
import static lodgings.common.data.errors.Error.ID_MISSING;
import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.ITEM_NOT_FOUND;
import static lodgings.domain.expenses.data.errors.ExpenseUpdateErrors.ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS;
import static lodgings.domain.expenses.data.errors.ExpenseUpdateErrors.EXPENSE_NOT_FOUND;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class ExpenseUpdateHandler {

    private final ExpenseRepository expenseRepository;
    private final ItemRepository itemRepository;
    private final SettlementViewer settlementViewer;
    private final ExpensePersistValidator validator;

    public Failable<SettlementProjection> handle(Long id, ExpensePersistRequest request) {
        log.debug("Request to update expense with id: {}", id);
        List<Error> errors = validator.validate(request);
        if (id == null) {
            log.warn("Attempt to update a non-identifiable expense! ID is null!");
            errors.add(ID_MISSING);
        }
        if (not(errors.isEmpty())) {
            log.debug("Failed to update expense with id: {}!", id);
            return Failable.error(errors);
        }
        Optional<Item> optionalItem = itemRepository.findItemByName(request.getItemName());
        if (optionalItem.isEmpty()) {
            return Failable.error(ITEM_NOT_FOUND);
        }
        Item item = optionalItem.get();
        Optional<Expense> optionalExpense = expenseRepository.findById(id);
        if (optionalExpense.isEmpty()) {
            return Failable.error(EXPENSE_NOT_FOUND);
        }
        Expense expense = optionalExpense.get();
        if (expense.isFromBeforeCurrentMonth()) {
            return Failable.error(ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS);
        }
        expense.setItem(item);
        expense.setTotalPrice(request.getTotalPrice());
        expenseRepository.save(expense);
        return success(settlementViewer.get(expense.getSettlement()));
    }
}
