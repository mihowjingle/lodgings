package lodgings.domain.expenses.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.expenses.data.projections.ExpenseForUpdateProjection;
import lodgings.domain.expenses.services.ExpenseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Optional;

@Slf4j
@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ViewExpenseForUpdateHandler {

    private final ExpenseRepository expenseRepository;
    private final ItemRepository itemRepository;

    public Optional<ExpenseForUpdateProjection> handle(Long id) {
        log.debug("Request to view an expense, id: {}, together with item names to choose from (for update)", id);
        return expenseRepository.findById(id).map(expense -> new ExpenseForUpdateProjection(
                expense,
                itemRepository.findAllItemNames()
        ));
    }
}
