package lodgings.domain.expenses.services.validators;

import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;
import lodgings.domain.expenses.services.validators.atomic.AtomicExpensePersistValidator;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Singleton
@RequiredArgsConstructor
public class ExpensePersistValidator {

    private final List<AtomicExpensePersistValidator> validators;

    public List<Error> validate(final ExpensePersistRequest request) {
        return validators.stream()
                .map(validator -> validator.validate(request))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }
}
