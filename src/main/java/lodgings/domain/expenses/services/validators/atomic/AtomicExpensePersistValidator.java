package lodgings.domain.expenses.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;

import java.util.Optional;

public interface AtomicExpensePersistValidator {
    Optional<Error> validate(ExpensePersistRequest request);
}
