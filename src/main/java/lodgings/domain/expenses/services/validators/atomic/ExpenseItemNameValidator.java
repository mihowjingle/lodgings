package lodgings.domain.expenses.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;

import javax.inject.Singleton;
import java.util.Optional;

import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.ITEM_NAME_MISSING;

@Singleton
public class ExpenseItemNameValidator implements AtomicExpensePersistValidator {

    @Override
    public Optional<Error> validate(ExpensePersistRequest request) {
        if (request.getItemName() == null) {
            return Optional.of(ITEM_NAME_MISSING);
        }
        return Optional.empty();
    }
}
