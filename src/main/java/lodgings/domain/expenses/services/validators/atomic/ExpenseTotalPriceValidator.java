package lodgings.domain.expenses.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.expenses.data.requests.ExpensePersistRequest;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Optional;


import static lodgings.common.data.Comparison.Operator.GREATER_THAN;
import static lodgings.common.data.Comparison.Operator.LESS_THAN;
import static lodgings.common.data.Comparison.is;
import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.*;

@Singleton
public class ExpenseTotalPriceValidator implements AtomicExpensePersistValidator {

    @Override
    public Optional<Error> validate(ExpensePersistRequest request) {
        BigDecimal totalPrice = request.getTotalPrice();
        if (totalPrice == null) {
            return Optional.of(TOTAL_PRICE_MISSING);
        }
        if (totalPrice.scale() > Expense.MAX_PRICE_DECIMAL_DIGITS) {
            return Optional.of(TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS);
        }
        if (is(totalPrice, GREATER_THAN, Expense.MAX_PRICE)) {
            return Optional.of(TOTAL_PRICE_TOO_HIGH);
        }
        if (is(totalPrice, LESS_THAN, Expense.MIN_PRICE)) {
            return Optional.of(TOTAL_PRICE_TOO_LOW);
        }
        return Optional.empty();
    }
}
