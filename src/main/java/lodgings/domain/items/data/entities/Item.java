package lodgings.domain.items.data.entities;

import lodgings.domain.users.data.entities.User;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;

@Data
@Entity
@FieldNameConstants
@Table(name = "items")
public class Item {

    public static final int MAX_NAME_LENGTH = 50;
    public static final int MIN_NAME_LENGTH = 4;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "assessable")
    private boolean assessable;

    @ManyToOne
    @JoinColumn(name = "purpose_id")
    private Purpose purpose;

    @ManyToOne
    @JoinColumn(name = "last_modified_by_id")
    private User lastModifiedBy;

    public Item(String name, boolean assessable, Purpose purpose, User lastModifiedBy) {
        this.name = name;
        this.assessable = assessable;
        this.purpose = purpose;
        this.lastModifiedBy = lastModifiedBy;
    }
}