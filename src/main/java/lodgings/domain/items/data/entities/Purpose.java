package lodgings.domain.items.data.entities;

import lodgings.domain.users.data.entities.User;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@FieldNameConstants
@Table(name = "purposes")
public class Purpose {

    public static final int MAX_NAME_LENGTH = 50;
    public static final int MIN_NAME_LENGTH = 4;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "purpose")
    private List<Item> items;

    @ManyToOne
    @JoinColumn(name = "last_modified_by_id")
    private User lastModifiedBy;

    public Purpose(String name, User lastModifiedBy) {
        this.name = name;
        this.lastModifiedBy = lastModifiedBy;
    }
}
