package lodgings.domain.items.data.errors;

import lodgings.common.data.errors.Error;

public class ItemDeletionErrors {
    public static final Error ITEM_RELATED_TO_EXPENSES = new Error("ITEM_RELATED_TO_EXPENSES");
}
