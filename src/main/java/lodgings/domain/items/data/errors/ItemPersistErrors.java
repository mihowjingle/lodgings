package lodgings.domain.items.data.errors;

import lodgings.common.data.errors.Error;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.entities.Purpose;

public class ItemPersistErrors {
    public static final Error NAME_MISSING = new Error("NAME_MISSING");
    public static final Error NAME_TOO_LONG = new Error("NAME_TOO_LONG", Item.MAX_NAME_LENGTH);
    public static final Error NAME_TOO_SHORT = new Error("NAME_TOO_SHORT", Item.MIN_NAME_LENGTH);
    public static final Error NAME_NOT_UNIQUE = new Error("NAME_NOT_UNIQUE");
    public static final Error PURPOSE_NAME_TOO_LONG = new Error("PURPOSE_NAME_TOO_LONG", Purpose.MAX_NAME_LENGTH);
    public static final Error PURPOSE_NAME_TOO_SHORT = new Error("PURPOSE_NAME_TOO_SHORT", Purpose.MIN_NAME_LENGTH);
}
