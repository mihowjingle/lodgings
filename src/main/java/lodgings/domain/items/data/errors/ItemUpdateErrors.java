package lodgings.domain.items.data.errors;

import lodgings.common.data.errors.Error;

public class ItemUpdateErrors {
    public static final Error ITEM_NOT_FOUND = new Error("ITEM_NOT_FOUND");
}
