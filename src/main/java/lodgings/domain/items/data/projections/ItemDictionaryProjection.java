package lodgings.domain.items.data.projections;

import lombok.Data;

@Data
public class ItemDictionaryProjection {
    private final String name;
    // returning an object instead of just name, because there will be other fields here in the future
    // yagni doesn't apply
}
