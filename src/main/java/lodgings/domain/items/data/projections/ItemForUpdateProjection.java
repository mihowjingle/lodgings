package lodgings.domain.items.data.projections;

import lombok.Data;

import java.util.List;

@Data
public class ItemForUpdateProjection {
    private final ItemProjection item;
    private final List<String> purposes;
}
