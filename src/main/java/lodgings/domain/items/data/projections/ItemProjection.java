package lodgings.domain.items.data.projections;

import lodgings.domain.items.data.entities.Item;
import lombok.Data;

@Data
public class ItemProjection {
    private final Long id;
    private final String name;
    private final boolean assessable;
    private final String purposeName;
    private final String lastModifiedBy;

    public ItemProjection(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.assessable = item.isAssessable();
        this.purposeName = item.getPurpose() == null ? null : item.getPurpose().getName();
        this.lastModifiedBy = item.getLastModifiedBy().getName();
    }
}
