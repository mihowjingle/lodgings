package lodgings.domain.items.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ItemPersistRequest {
    private final String name;
    private final boolean assessable;
    private final String purposeName;

    @JsonCreator
    public ItemPersistRequest(
            @JsonProperty("name") String name,
            @JsonProperty("assessable") boolean assessable,
            @JsonProperty("purposeName") String purposeName
    ) {
        this.name = name;
        this.assessable = assessable;
        this.purposeName = purposeName;
    }
}
