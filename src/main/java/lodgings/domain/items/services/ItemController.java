package lodgings.domain.items.services;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.items.data.projections.ItemDictionaryProjection;
import lodgings.domain.items.data.projections.ItemForUpdateProjection;
import lodgings.domain.items.data.requests.ItemPersistRequest;
import lodgings.domain.items.services.scenarios.*;
import lodgings.domain.users.data.entities.Role;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@Controller("/api/items")
@RequiredArgsConstructor
@Secured(SecurityRule.IS_AUTHENTICATED)
public class ItemController {

    private final ItemCreationHandler creationHandler;
    private final ItemDeletionHandler deletionHandler;
    private final ViewItemsHandler viewItemsHandler;
    private final ViewItemForUpdateHandler viewItemForUpdateHandler;
    private final ItemUpdateHandler itemUpdateHandler;
    private final GetPurposeNamesHandler getPurposeNamesHandler;
    private final GetItemsAsDictionaryHandler getItemsAsDictionaryHandler;

    @Post
    @Secured(Role.USER)
    public HttpResponse<?> save(@Body ItemPersistRequest request) {
        return creationHandler.handle(request).toHttpResponse();
    }

    @Delete("/{id}")
    @Secured(Role.USER)
    public HttpResponse<?> delete(Long id) {
        return deletionHandler.handle(id).toHttpResponse();
    }

    @Get("{?request*}")
    @Secured(Role.USER)
    public HttpResponse<?> viewItems(PaginationRequest request) {
        return viewItemsHandler.handle(request).toHttpResponse();
    }

    @Get("/{id}/for-update")
    @Secured(Role.USER)
    public Optional<ItemForUpdateProjection> viewItemForUpdate(Long id) {
        return viewItemForUpdateHandler.handle(id);
    }

    @Put("/{id}")
    @Secured(Role.USER)
    public HttpResponse<?> update(Long id, @Body ItemPersistRequest request) {
        return itemUpdateHandler.handle(id, request).toHttpResponse();
    }

    @Get("/purposes")
    @Secured(Role.USER)
    public HttpResponse<List<String>> getPurposeNames() {
        return HttpResponse.ok(getPurposeNamesHandler.handle());
    }

    @Get("/dictionary")
    @Secured(Role.USER)
    public List<ItemDictionaryProjection> getItemsForSelection() {
        return getItemsAsDictionaryHandler.handle();
    }
}
