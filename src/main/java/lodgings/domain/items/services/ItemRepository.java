package lodgings.domain.items.services;

import io.ebean.Database;
import io.ebean.PagedList;
import io.ebean.annotation.Transactional;
import io.ebean.annotation.TxType;
import io.ebean.cache.ServerCacheManager;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.entities.Purpose;
import lodgings.domain.expenses.data.entities.Expense;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static lodgings.common.Queries.join;

@Singleton
@RequiredArgsConstructor
@Transactional(type = TxType.MANDATORY)
public class ItemRepository {

    private final Database db;

    public Item save(Item item) {
        db.save(item);
        return item;
    }

    public Purpose save(Purpose purpose) {
        db.save(purpose);
        return purpose;
    }

    public Optional<Purpose> findPurposeByName(String name) {
        return db.find(Purpose.class).where().eq(Purpose.Fields.name, name).findOneOrEmpty();
    }

    public boolean itemWithNameExists(String name) {
        return db.find(Item.class).where().eq(Item.Fields.name, name).exists();
    }

    public void delete(Long id) {
        db.find(Item.class).where().eq(Item.Fields.id, id).delete();
    }

    public void deleteEmptyPurposes() {
        db.sqlUpdate("DELETE FROM purposes g " +
                "WHERE NOT EXISTS (SELECT i.id FROM items i WHERE i.purpose_id = g.id)").execute();
        ServerCacheManager serverCacheManager = db.getServerCacheManager();
        serverCacheManager.clear(Purpose.class);
    }

    public PagedList<Item> findMany(PaginationRequest request) {
        return db.find(Item.class)
                .setFirstRow(request.getFirstRow())
                .setMaxRows(request.getSize())
                .setOrderBy(request.getOrderBy())
                .findPagedList();
    }

    public List<String> findAllPurposeNames() {
        return db.find(Purpose.class).select(Purpose.Fields.name).findSingleAttributeList();
    }

    public Optional<Item> findItemById(Long id) {
        return db.find(Item.class).where().idEq(id).findOneOrEmpty();
    }

    public boolean anotherItemWithNameExists(String name, Long id) {
        return db.find(Item.class).where()
                .eq(Item.Fields.name, name)
                .ne(Item.Fields.id, id)
                .exists();
    }

    public Optional<Item> findItemByName(String itemName) {
        return db.find(Item.class).where().eq(Item.Fields.name, itemName).findOneOrEmpty();
    }

    public List<String> findAllItemNames() {
        return db.find(Item.class).select(Item.Fields.name).findSingleAttributeList();
    }

    public boolean itemIsRelatedToExpenses(Long itemId) {
        return db.find(Expense.class).where().eq(join(Expense.Fields.item, Item.Fields.id), itemId).exists();
    }

    public List<Item> findAllItemsForSelection() {
        return db.find(Item.class).select(Item.Fields.name).findList();
    }
}
