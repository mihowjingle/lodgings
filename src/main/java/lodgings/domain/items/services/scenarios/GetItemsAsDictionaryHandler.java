package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.items.data.projections.ItemDictionaryProjection;
import lodgings.domain.items.services.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class GetItemsAsDictionaryHandler {

    private final ItemRepository itemRepository;

    public List<ItemDictionaryProjection> handle() {
        log.debug("Request to get all items with only the details needed for selection");
        return itemRepository.findAllItemsForSelection().stream()
                .map(item -> new ItemDictionaryProjection(item.getName()))
                .collect(toList());
    }
}

