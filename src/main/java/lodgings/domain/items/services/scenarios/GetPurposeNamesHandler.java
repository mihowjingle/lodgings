package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.items.services.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;

@Slf4j
@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class GetPurposeNamesHandler {

    private final ItemRepository itemRepository;

    public List<String> handle() {
        log.debug("Request to get all purpose names");
        return itemRepository.findAllPurposeNames();
    }
}

