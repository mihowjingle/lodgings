package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.common.services.CurrentUserService;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.entities.Purpose;
import lodgings.domain.items.data.requests.ItemPersistRequest;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.items.services.validators.ItemPersistValidator;
import lodgings.domain.users.data.entities.User;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;

import static lodgings.domain.items.data.errors.ItemPersistErrors.NAME_NOT_UNIQUE;

@Slf4j
@Singleton
@Transactional
public class ItemCreationHandler extends ItemPersistHandler {

    private final ItemPersistValidator itemPersistValidator;
    private final CurrentUserService currentUserService;

    public ItemCreationHandler(ItemRepository itemRepository,
                               ItemPersistValidator ItemPersistValidator,
                               CurrentUserService currentUserService) {
        super(itemRepository);
        this.itemPersistValidator = ItemPersistValidator;
        this.currentUserService = currentUserService;
    }

    public Failable<Void> handle(final ItemPersistRequest request) {
        log.debug("Request to create and save item with name: {}", request.getName());
        List<Error> errors = itemPersistValidator.validate(request);
        if (errors.isEmpty()) {
            if (itemRepository.itemWithNameExists(request.getName())) {
                return Failable.error(NAME_NOT_UNIQUE);
            }
            final User currentUser = currentUserService.get();
            final Purpose purpose = findOrCreatePurposeIfRequested(request.getPurposeName(), currentUser);
            Item item = new Item(
                    request.getName(),
                    request.isAssessable(),
                    purpose,
                    currentUser
            );
            itemRepository.save(item);
            log.debug("Successfully created item with name: {}", item.getName());
            return Failable.success();
        }
        log.debug("Failed to create item with name: {}!", request.getName());
        return Failable.error(errors);
    }
}
