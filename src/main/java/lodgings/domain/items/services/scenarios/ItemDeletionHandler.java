package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.domain.items.services.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

import static lodgings.domain.items.data.errors.ItemDeletionErrors.ITEM_RELATED_TO_EXPENSES;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class ItemDeletionHandler {

    private final ItemRepository itemRepository;

    public Failable<Void> handle(Long id) {
        log.debug("Request to delete item with id: {}", id);
        if (itemRepository.itemIsRelatedToExpenses(id)) {
            return Failable.error(ITEM_RELATED_TO_EXPENSES);
        }
        itemRepository.delete(id);
        itemRepository.deleteEmptyPurposes();
        return Failable.success();
    }
}
