package lodgings.domain.items.services.scenarios;

import lodgings.domain.items.data.entities.Purpose;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.users.data.entities.User;
import lombok.RequiredArgsConstructor;

import io.micronaut.core.annotation.Nullable;

@RequiredArgsConstructor
public abstract class ItemPersistHandler {

    protected final ItemRepository itemRepository;

    // name null -> null
    // name not-null, purpose exists -> the existing purpose
    // name not-null, purpose doesn't exist -> new purpose with given name
    protected @Nullable Purpose findOrCreatePurposeIfRequested(@Nullable String purposeName, User currentUser) {
        if (purposeName == null) {
            return null;
        }
        return itemRepository.findPurposeByName(purposeName).orElseGet(
                () -> itemRepository.save(new Purpose(purposeName, currentUser))
        );
    }
}
