package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.common.services.CurrentUserService;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.requests.ItemPersistRequest;
import lodgings.domain.items.services.ItemRepository;
import lodgings.domain.items.services.validators.ItemPersistValidator;
import lodgings.domain.users.data.entities.User;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.domain.items.data.errors.ItemPersistErrors.NAME_NOT_UNIQUE;
import static lodgings.domain.items.data.errors.ItemUpdateErrors.ITEM_NOT_FOUND;

@Slf4j
@Singleton
@Transactional
public class ItemUpdateHandler extends ItemPersistHandler {

    private final ItemPersistValidator itemPersistValidator;
    private final CurrentUserService currentUserService;

    public ItemUpdateHandler(ItemRepository itemRepository,
                             ItemPersistValidator itemPersistValidator,
                             CurrentUserService currentUserService) {
        super(itemRepository);
        this.itemPersistValidator = itemPersistValidator;
        this.currentUserService = currentUserService;
    }

    public Failable<Void> handle(Long id, ItemPersistRequest request) {
        log.debug("Request to update item with id: {}", id);
        List<Error> errors = itemPersistValidator.validate(request);
        if (id == null) {
            log.warn("Attempt to update a non-identifiable item! ID is null!");
            errors.add(Error.ID_MISSING);
        }
        if (not(errors.isEmpty())) {
            log.debug("Failed to update item with id: {}!", id);
            return Failable.error(errors);
        }
        Optional<Item> maybeItem = itemRepository.findItemById(id);
        if (maybeItem.isEmpty()) {
            log.warn("Attempt to update a non-existent item! Item id: {}", id);
            return Failable.error(ITEM_NOT_FOUND);
        }
        if (itemRepository.anotherItemWithNameExists(request.getName(), id)) {
            return Failable.error(NAME_NOT_UNIQUE);
        }
        User currentUser = currentUserService.get();
        Item item = maybeItem.get();
        item.setAssessable(request.isAssessable());
        item.setPurpose(findOrCreatePurposeIfRequested(request.getPurposeName(), currentUser));
        item.setLastModifiedBy(currentUser);
        item.setName(request.getName());
        itemRepository.save(item);
        itemRepository.deleteEmptyPurposes();
        log.debug("Successfully updated item with id: {}", id);
        return Failable.success();
    }
}
