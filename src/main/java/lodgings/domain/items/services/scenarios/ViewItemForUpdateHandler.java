package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.items.data.projections.ItemForUpdateProjection;
import lodgings.domain.items.data.projections.ItemProjection;
import lodgings.domain.items.services.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Optional;

@Slf4j
@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ViewItemForUpdateHandler {

    private final ItemRepository itemRepository;

    public Optional<ItemForUpdateProjection> handle(Long id) {
        log.debug("Request to view an item, id: {}, together with purpose names to choose from (for update)", id);
        return itemRepository.findItemById(id)
                .map(item -> new ItemForUpdateProjection(
                        new ItemProjection(item),
                        itemRepository.findAllPurposeNames()
                ));
    }
}
