package lodgings.domain.items.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.pagination.Page;
import lodgings.common.data.pagination.PagedEntity;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.items.data.projections.ItemProjection;
import lodgings.domain.items.services.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ViewItemsHandler {

    private final ItemRepository itemRepository;

    @Transactional(readOnly = true)
    public Failable<Page<ItemProjection>> handle(PaginationRequest request) {
        log.debug("Request to view a page of items");
        final var errors = request.validateFor(PagedEntity.ITEM);
        if (errors.isEmpty()) {
            return Failable.success(new Page<>(itemRepository.findMany(request), ItemProjection::new));
        }
        return Failable.error(errors);
    }
}
