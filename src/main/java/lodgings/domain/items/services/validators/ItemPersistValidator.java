package lodgings.domain.items.services.validators;

import lodgings.common.data.errors.Error;
import lodgings.domain.items.data.requests.ItemPersistRequest;
import lodgings.domain.items.services.validators.atomic.AtomicItemPersistValidator;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Singleton
@RequiredArgsConstructor
public class ItemPersistValidator {

    private final List<AtomicItemPersistValidator> validators;

    public List<Error> validate(ItemPersistRequest request) {
        return validators.stream()
                .map(validator -> validator.validate(request))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }
}
