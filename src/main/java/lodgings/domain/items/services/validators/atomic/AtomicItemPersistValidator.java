package lodgings.domain.items.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.items.data.requests.ItemPersistRequest;

import java.util.Optional;

public interface AtomicItemPersistValidator {
    Optional<Error> validate(ItemPersistRequest request);
}
