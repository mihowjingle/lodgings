package lodgings.domain.items.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.items.data.entities.Item;
import lodgings.domain.items.data.errors.ItemPersistErrors;
import lodgings.domain.items.data.requests.ItemPersistRequest;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class ItemNameValidator implements AtomicItemPersistValidator {

    @Override
    public Optional<Error> validate(ItemPersistRequest request) {
        if (request.getName() == null) {
            return Optional.of(ItemPersistErrors.NAME_MISSING);
        }
        if (request.getName().length() > Item.MAX_NAME_LENGTH) {
            return Optional.of(ItemPersistErrors.NAME_TOO_LONG);
        }
        if (request.getName().length() < Item.MIN_NAME_LENGTH) {
            return Optional.of(ItemPersistErrors.NAME_TOO_SHORT);
        }
        return Optional.empty();
    }
}
