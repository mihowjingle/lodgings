package lodgings.domain.items.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.items.data.entities.Purpose;
import lodgings.domain.items.data.errors.ItemPersistErrors;
import lodgings.domain.items.data.requests.ItemPersistRequest;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class PurposeNameValidator implements AtomicItemPersistValidator {

    @Override
    public Optional<Error> validate(ItemPersistRequest request) {
        if (request.getPurposeName() != null) {
            if (request.getPurposeName().length() > Purpose.MAX_NAME_LENGTH) {
                return Optional.of(ItemPersistErrors.PURPOSE_NAME_TOO_LONG);
            }
            if (request.getPurposeName().length() < Purpose.MIN_NAME_LENGTH) {
                return Optional.of(ItemPersistErrors.PURPOSE_NAME_TOO_SHORT);
            }
        }
        return Optional.empty();
    }
}
