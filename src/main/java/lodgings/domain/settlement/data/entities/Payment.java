package lodgings.domain.settlement.data.entities;

import lodgings.domain.users.data.entities.User;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@FieldNameConstants
@Table(name = "payments")
public class Payment {

    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "payer_id")
    private User from;

    @ManyToOne
    @JoinColumn(name = "payee_id")
    private User to;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "received")
    private boolean received;

    @ManyToOne
    @JoinColumn(name = "settlement_id")
    private Settlement settlement;

    public Payment(User from, User to, BigDecimal amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Payment(" + from.getName() + " -> " + to.getName() + ", amount = " + amount + ")";
    }

    public Status getStatus() {
        if (id == null) {
            return Status.FORECAST;
        }
        if (received) {
            return Status.RECEIVED;
        }
        return Status.SETTLED;
    }

    public enum Status {
        /**
         * Current month, payment calculated live, ad-hoc, not even in the database
         */
        FORECAST,

        /**
         * Month ended, payment saved in the database, unsent / unpaid
         */
        SETTLED, // a.k.a. SETTLED_UNSENT?

        /**
         * Payer claims they paid, to be used or removed in #84
         */
        SENT, // a.k.a. SENT_AWAITING_CONFIRMATION?

        /**
         * Payee agrees with payer :P
         */
        RECEIVED // a.k.a. CONFIRMED?
    }
}
