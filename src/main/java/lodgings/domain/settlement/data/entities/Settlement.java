package lodgings.domain.settlement.data.entities;

import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.users.data.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.time.YearMonth;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
@FieldNameConstants
@Table(name = "settlements")
@ToString(exclude = {"users", "payments", "expenses"})
@EqualsAndHashCode(exclude = {"users", "payments", "expenses"})
public class Settlement {

    @Id
    private Long id;

    @Column(name = "year_month")
    private YearMonth yearMonth;

    @Column(name = "settled")
    private boolean settled;

    @ManyToMany
    @JoinTable(name = "settlements_to_users", joinColumns = {
            @JoinColumn(name = "settlement_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "user_id")
    })
    private List<User> users;

    @OneToMany(mappedBy = "settlement", cascade = CascadeType.PERSIST)
    private List<Payment> payments = new LinkedList<>();

    @Column(name = "payments_calculated")
    private boolean paymentsCalculated;

    @OneToMany(mappedBy = "settlement")
    private List<Expense> expenses = new LinkedList<>();

    public Settlement(YearMonth yearMonth, List<User> users) {
        this.yearMonth = yearMonth;
        this.users = users;
    }

    /**
     * @return whether there was a change (and an update of the db is needed)
     */
    public boolean updateSettlementConfirmation() {
        boolean settledBefore = this.settled;
        this.settled = payments.stream().allMatch(Payment::isReceived);
        return settledBefore != this.settled;
    }
}
