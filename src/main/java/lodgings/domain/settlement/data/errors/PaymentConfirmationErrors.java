package lodgings.domain.settlement.data.errors;

import lodgings.common.data.errors.Error;

public class PaymentConfirmationErrors {
    public static final Error USER_NOT_INCLUDED_IN_SETTLEMENT = new Error("USER_NOT_INCLUDED_IN_SETTLEMENT");
    public static final Error PAYMENT_DOES_NOT_EXIST = new Error("PAYMENT_DOES_NOT_EXIST");
    public static final Error USER_IS_NOT_THE_PAYEE = new Error("USER_IS_NOT_THE_PAYEE");
}
