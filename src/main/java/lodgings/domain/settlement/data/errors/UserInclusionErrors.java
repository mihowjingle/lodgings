package lodgings.domain.settlement.data.errors;

import lodgings.common.data.errors.Error;

public class UserInclusionErrors {
    public static final Error USER_NOT_FOUND = new Error("USER_NOT_FOUND");
    public static final Error SETTLEMENT_REQUIRES_USER_ROLE = new Error("SETTLEMENT_REQUIRES_USER_ROLE");
}
