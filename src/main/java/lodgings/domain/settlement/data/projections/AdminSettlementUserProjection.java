package lodgings.domain.settlement.data.projections;

import lodgings.domain.users.data.entities.User;
import lombok.Data;

@Data
public class AdminSettlementUserProjection {
    private final Long id;
    private final String login;
    private final String name;

    public AdminSettlementUserProjection(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.name = user.getName();
    }
}
