package lodgings.domain.settlement.data.projections;

import lodgings.domain.settlement.data.entities.Payment;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentProjection {
    private final Long id;
    private final SettlementUserProjection from;
    private final SettlementUserProjection to;
    private final BigDecimal amount;
    private final Payment.Status status;

    public PaymentProjection(Payment payment) {
        this.id = payment.getId();
        this.from = new SettlementUserProjection(payment.getFrom());
        this.to = new SettlementUserProjection(payment.getTo());
        this.amount = payment.getAmount();
        this.status = payment.getStatus();
    }
}
