package lodgings.domain.settlement.data.projections;

import lodgings.domain.expenses.data.projections.ExpenseProjection;
import lodgings.domain.settlement.data.entities.Payment;
import lodgings.domain.settlement.data.entities.Settlement;
import lombok.Data;

import java.time.YearMonth;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
public class SettlementProjection {

    private final List<ExpenseProjection> expenses;
    private final List<PaymentProjection> payments;
    private final List<SettlementUserProjection> users;
    private final YearMonth yearMonth;
    private final boolean settled;

    public SettlementProjection(Settlement settlement, List<Payment> payments) {
        this.expenses = settlement.getExpenses().stream().map(ExpenseProjection::new).collect(toList());
        this.payments = payments.stream().map(PaymentProjection::new).collect(toList());
        this.users = settlement.getUsers().stream().map(SettlementUserProjection::new).collect(toList());
        this.yearMonth = settlement.getYearMonth();
        this.settled = settlement.isSettled();
    }
}
