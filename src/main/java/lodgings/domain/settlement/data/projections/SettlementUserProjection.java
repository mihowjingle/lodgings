package lodgings.domain.settlement.data.projections;

import lodgings.domain.users.data.entities.User;
import lombok.Data;

@Data
public class SettlementUserProjection {
    private final Long id;
    private final String name;

    public SettlementUserProjection(User user) {
        this.id = user.getId();
        this.name = user.getName();
    }
}
