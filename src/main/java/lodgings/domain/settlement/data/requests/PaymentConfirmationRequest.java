package lodgings.domain.settlement.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PaymentConfirmationRequest {

    private final boolean received;

    @JsonCreator
    public PaymentConfirmationRequest(@JsonProperty("received") boolean received) {
        this.received = received;
    }
}
