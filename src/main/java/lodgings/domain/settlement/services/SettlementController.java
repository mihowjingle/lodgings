package lodgings.domain.settlement.services;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.data.requests.PaymentConfirmationRequest;
import lodgings.domain.settlement.services.scenarios.PaymentConfirmationHandler;
import lodgings.domain.settlement.services.scenarios.UserInclusionInSettlementHandler;
import lodgings.domain.settlement.services.scenarios.ViewSettlementHandler;
import lodgings.domain.users.data.entities.Role;
import lombok.RequiredArgsConstructor;

import java.time.YearMonth;

@RequiredArgsConstructor
@Controller("/api/settlements")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class SettlementController {

    private final PaymentConfirmationHandler paymentConfirmationHandler;
    private final ViewSettlementHandler viewSettlementHandler;
    private final UserInclusionInSettlementHandler userInclusionInSettlementHandler;

    @Post("/confirm-payment/{id}")
    @Secured(Role.USER)
    public HttpResponse<?> confirm(Long id, @Body PaymentConfirmationRequest request) {
        return paymentConfirmationHandler.handle(id, request).toHttpResponse();
    }

    @Get("/year/{year}/month/{month}")
    @Secured({Role.USER, Role.ADMIN})
    public HttpResponse<SettlementProjection> view(int year, int month) {
        return HttpResponse.ok(
                viewSettlementHandler.handle(YearMonth.of(year, month)).orElse(null) // why, oh why, Micronaut?!
        );
    }

    @Post("/user/{id}/include/{include}")
    @Secured(Role.ADMIN)
    public HttpResponse<?> addUser(Long id, boolean include) {
        return userInclusionInSettlementHandler.handle(id, include).toHttpResponse();
    }
}
