package lodgings.domain.settlement.services;

import io.ebean.annotation.Transactional;
import io.micronaut.discovery.event.ServiceReadyEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.scheduling.annotation.Async;
import io.micronaut.scheduling.annotation.Scheduled;
import lodgings.domain.expenses.services.ExpenseRepository;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.YearMonth;

import static lodgings.common.SyntacticSugar.not;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class SettlementKeeper {

    private final SettlementRepository settlementRepository;
    private final UserRepository userRepository;
    private final ExpenseRepository expenseRepository;

    @Transactional
    @Scheduled(cron = "00 23 L * ?") // last day of a month @ 23:00
    void cleanUpAndCreateNextSettlement() {
        log.info("Creating next settlement.");
        YearMonth monthAboutToStart = YearMonth.now().plusMonths(1);
        var settlement = createAndSaveSettlement(monthAboutToStart);
        log.info("Successfully created {}", settlement);
        int users = userRepository.completeTerminationPeriod();
        log.info("Automatically disallowed {} users to login (\"termination period\")", users);
    }

    @Async
    @EventListener
    @Transactional
    void ensureCurrentSettlementExists(final ServiceReadyEvent event) {
        log.info("Application startup: checking if current settlement exists");
        YearMonth now = YearMonth.now();
        if (not(settlementRepository.settlementExists(now))) {
            var current = createAndSaveSettlement(now);
            log.warn("Current settlement was missing (first app startup ever? or at least this month) Successfully created: {}", current);
        } else {
            log.info("Current ({}) settlement exists, skipping creation", now);
        }
    }

    @Transactional
    @Scheduled(cron = "1 0 1 * ?") // first day of a month @ 00:01
    void autoSettleLastMonthIfNoExpenses() {
        YearMonth lastMonth = YearMonth.now().minusMonths(1);
        if (expenseRepository.noExpenses(lastMonth)) {
            log.info("There were no expenses last month. Setting the corresponding settlement to settled.");
            settlementRepository.settle(lastMonth);
        }
    }

    private Settlement createAndSaveSettlement(YearMonth yearMonth) {
        var usersToBeIncludedInTheNextSettlement = userRepository.findToBeIncludedInTheNextSettlement();
        usersToBeIncludedInTheNextSettlement.forEach(user -> user.setAllowedToLogin(true));
        userRepository.saveAll(usersToBeIncludedInTheNextSettlement);
        return settlementRepository.save(new Settlement(yearMonth, usersToBeIncludedInTheNextSettlement));
    }
}
