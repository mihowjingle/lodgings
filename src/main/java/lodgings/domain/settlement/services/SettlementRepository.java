package lodgings.domain.settlement.services;

import io.ebean.Database;
import io.ebean.annotation.Transactional;
import io.ebean.annotation.TxType;
import lodgings.domain.settlement.data.entities.Payment;
import lodgings.domain.settlement.data.entities.Settlement;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor
@Transactional(type = TxType.MANDATORY)
public class SettlementRepository {

    private final Database db;

    public Settlement save(Settlement settlement) {
        db.save(settlement);
        return settlement;
    }

    public boolean settlementExists(YearMonth yearMonth) {
        return db.find(Settlement.class).where().eq(Settlement.Fields.yearMonth, yearMonth).exists();
    }

    public Settlement findSettlement(YearMonth yearMonth) {
        return db.find(Settlement.class)
                .where()
                .eq(Settlement.Fields.yearMonth, yearMonth)
                .findOneOrEmpty()
                .orElseThrow(() -> new IllegalStateException("No Settlement for " + yearMonth + " found!"));
    }

    public Optional<Settlement> findSettlementOptionallyFetchUsersAndExpenses(YearMonth yearMonth) {
        return db.find(Settlement.class)
                .fetch(Settlement.Fields.expenses)
                .fetch(Settlement.Fields.users)
                .where()
                .eq(Settlement.Fields.yearMonth, yearMonth)
                .findOneOrEmpty();
    }

    public Settlement findSettlementFetchUsersAndExpenses(YearMonth yearMonth) {
        return findSettlementOptionallyFetchUsersAndExpenses(yearMonth)
                .orElseThrow(() -> new IllegalStateException("No Settlement for " + yearMonth + " found!"));
    }

    public void settle(YearMonth month) {
        db.find(Settlement.class)
                .where()
                .eq(Settlement.Fields.yearMonth, month)
                .asUpdate()
                .set(Settlement.Fields.settled, true)
                .update();
    }

    public void save(List<Payment> payments) {
        db.saveAll(payments);
    }

    public Optional<Payment> findPayment(Long id) {
        return db.find(Payment.class).where().idEq(id).findOneOrEmpty();
    }

    public void save(Payment payment) {
        db.save(payment);
    }
}
