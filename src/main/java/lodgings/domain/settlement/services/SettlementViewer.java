package lodgings.domain.settlement.services;

import io.ebean.annotation.Transactional;
import io.ebean.annotation.TxType;
import io.micronaut.core.annotation.NonNull;
import lodgings.domain.settlement.data.entities.Payment;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.payments.PaymentCalculator;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
@Transactional(type = TxType.MANDATORY)
@RequiredArgsConstructor
public class SettlementViewer {

    private final SettlementRepository settlementRepository;
    private final PaymentCalculator paymentCalculator;

    public SettlementProjection get(Settlement settlement) {
        return new SettlementProjection(settlement, calculateOrLazyLoadPayments(settlement));
    }

    public Optional<SettlementProjection> get(@NonNull YearMonth month) {
        return settlementRepository.findSettlementOptionallyFetchUsersAndExpenses(month).map(this::get);
    }

    private List<Payment> calculateOrLazyLoadPayments(Settlement settlement) {
        if (settlement.getYearMonth().equals(YearMonth.now())) { // viewing current month
            return paymentCalculator.calculate(settlement);
        } else if (settlement.isPaymentsCalculated()) { // viewing past month
            return sortedById(settlement.getPayments());
        } else { // viewing past month... for the first time since it ended
            calculateAndSavePayments(settlement);
            return sortedById(settlement.getPayments());
        }
    }

    // just to get the same order every time
    private List<Payment> sortedById(List<Payment> payments) {
        return payments.stream()
                .sorted(Comparator.comparing(Payment::getId))
                .collect(Collectors.toList());
    }

    private void calculateAndSavePayments(Settlement settlement) {
        List<Payment> payments = paymentCalculator.calculate(settlement);
        settlement.setPayments(payments);
        settlement.setPaymentsCalculated(true);
        settlementRepository.save(settlement);
    }
}
