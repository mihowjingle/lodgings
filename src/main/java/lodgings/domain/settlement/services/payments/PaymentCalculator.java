package lodgings.domain.settlement.services.payments;

import lodgings.domain.expenses.data.entities.Expense;
import lodgings.domain.settlement.data.entities.Payment;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.users.data.entities.User;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.data.Comparison.Operator.*;
import static lodgings.common.data.Comparison.is;

@Slf4j
@Singleton
public class PaymentCalculator {

    private static final BigDecimal MIN_AMOUNT_OF_MONEY = new BigDecimal("0.01");

    public List<Payment> calculate(Settlement settlement) {
        List<Expense> expenses = settlement.getExpenses();
        List<User> users = settlement.getUsers();
        if (expenses.isEmpty() || users.isEmpty()) {
            return List.of();
        }
        Map<User, List<Expense>> expensesByUser = groupExpensesByUser(expenses, users);
        List<UserAmount> totalExpenseByUser = calculateTotalExpenseByUser(expensesByUser);
        BigDecimal averageExpense = calculateAverageExpensePerUser(totalExpenseByUser);
        UserDiscriminator userDiscriminator = partitionPayingUsersPaidUsers(totalExpenseByUser, averageExpense);
        List<Payment> payments = new LinkedList<>();
        compensateEqualOpposites(userDiscriminator, payments);
        compensateUnequalOpposites(userDiscriminator, payments);
        payments.forEach(payment -> payment.setAmount(payment.getAmount().setScale(2, RoundingMode.HALF_DOWN)));
        return payments.stream()
                .filter(payment -> is(payment.getAmount(), GREATER_THAN, BigDecimal.ZERO))
                .collect(toList());
    }

    private Map<User, List<Expense>> groupExpensesByUser(List<Expense> expenses, List<User> users) {
        final var userExpenses = new HashMap<User, List<Expense>>();
        for (var user : users) {
            userExpenses.put(user, getExpensesOfUser(expenses, user));
        }
        return userExpenses;
    }

    private List<Expense> getExpensesOfUser(List<Expense> expenses, User user) {
        return expenses.stream()
                .filter(expense -> expense.getBuyer().equals(user))
                .collect(toList());
    }

    private List<UserAmount> calculateTotalExpenseByUser(Map<User, List<Expense>> expensesByUser) {
        return expensesByUser.entrySet().stream() // here it's how much the user HAS PAID for the goods they bought:
                .map(userExpenses -> new UserAmount(userExpenses.getKey(), getTotalAmount(userExpenses)))
                .collect(toList());
    }

    private BigDecimal getTotalAmount(Map.Entry<User, List<Expense>> userExpenses) {
        return userExpenses.getValue().stream()
                .map(Expense::getTotalPrice)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    private BigDecimal calculateAverageExpensePerUser(List<UserAmount> totalExpenseByUser) {
        return totalExpenseByUser.stream()
                .map(userAmount -> userAmount.amount)
                .reduce(BigDecimal::add)
                .orElseThrow(() -> new IllegalStateException("No users?"))
                .divide(BigDecimal.valueOf(totalExpenseByUser.size()), 3, RoundingMode.HALF_UP);
    }

    private UserDiscriminator partitionPayingUsersPaidUsers(List<UserAmount> totalExpenseByUser, BigDecimal averageExpense) {
        List<UserAmount> howMuchGivenUserNeedsToPay = totalExpenseByUser.stream()
                .map(userAmount -> new UserAmount( // here it's how much the user NEEDS TO PAY (in total) to compensate others
                        userAmount.user,
                        averageExpense.subtract(userAmount.amount) // negative = user needs to be compensated
                ))
                .filter(userAmount -> is(userAmount.amount, NOT_EQUAL_TO, BigDecimal.ZERO))
                .sorted(reverseOrder())
                .collect(toList());
        Map<Boolean, List<UserAmount>> split = howMuchGivenUserNeedsToPay.stream().collect(
                partitioningBy(userAmount -> is(userAmount.amount, GREATER_THAN, BigDecimal.ZERO))
        );
        UserDiscriminator userDiscriminator = new UserDiscriminator(split.get(true), split.get(false));
        userDiscriminator.paidUsers.forEach(userAmount -> userAmount.amount = userAmount.amount.negate());
        return userDiscriminator;
    }

    private void compensateEqualOpposites(UserDiscriminator userDiscriminator, List<Payment> payments) {
        for (var paying : userDiscriminator.payingUsers) {
            for (var paid : userDiscriminator.paidUsers) {
                if (is(paying.amount, EQUAL_TO, paid.amount)) {
                    payments.add(new Payment(paying.user, paid.user, paying.amount));
                    paid.amount = BigDecimal.ZERO;
                    paying.amount = BigDecimal.ZERO;
                }
            }
        }
    }

    private void compensateUnequalOpposites(UserDiscriminator userDiscriminator, List<Payment> payments) {
        for (var payingUser : userDiscriminator.payingUsers) {
            while (is(payingUser.amount.setScale(2, RoundingMode.HALF_UP), GREATER_THAN, BigDecimal.ZERO)) {
                boolean userToBePaidFound = false;
                for (var paidUser : userDiscriminator.paidUsers) {
                    if (is(paidUser.amount, GREATER_THAN, BigDecimal.ZERO)) {
                        userToBePaidFound = true;
                        BigDecimal smallerAmount = smallerAmount(payingUser.amount, paidUser.amount);
                        payments.add(new Payment(payingUser.user, paidUser.user, smallerAmount));
                        paidUser.amount = paidUser.amount.subtract(smallerAmount);
                        payingUser.amount = payingUser.amount.subtract(smallerAmount);
                        break;
                    }
                }
                if (not(userToBePaidFound)) {
                    if (is(payingUser.amount, GREATER_THAN, MIN_AMOUNT_OF_MONEY)) {
                        log.warn("Payment calculation: noticeable rounding inaccuracy! amount: {} (still as close to fair as possible, so ok)", payingUser.amount);
                    }
                    break;
                }
            }
        }
    }

    private BigDecimal smallerAmount(BigDecimal one, BigDecimal other) {
        return is(one, LESS_THAN, other) ? one : other;
    }

    @AllArgsConstructor
    private static class UserAmount implements Comparable<UserAmount> {
        private final User user;
        private BigDecimal amount;

        @Override
        public int compareTo(@Nonnull UserAmount that) {
            return this.amount.compareTo(that.amount);
        }

        @Override
        public String toString() {
            return "UserAmount(" +
                    "user=" + user.getName() +
                    ", amount=" + amount +
                    ')';
        }
    }

    @RequiredArgsConstructor
    private static class UserDiscriminator {
        private final List<UserAmount> payingUsers;
        private final List<UserAmount> paidUsers;
    }
}