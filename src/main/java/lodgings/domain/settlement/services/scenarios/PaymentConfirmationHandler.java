package lodgings.domain.settlement.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.services.CurrentUserService;
import lodgings.domain.settlement.data.entities.Payment;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.data.requests.PaymentConfirmationRequest;
import lodgings.domain.settlement.services.SettlementRepository;
import lodgings.domain.settlement.services.SettlementViewer;
import lodgings.domain.users.data.entities.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.data.Failable.error;
import static lodgings.common.data.Failable.success;
import static lodgings.domain.settlement.data.errors.PaymentConfirmationErrors.*;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class PaymentConfirmationHandler {

    private final CurrentUserService currentUserService;
    private final SettlementRepository settlementRepository;
    private final SettlementViewer settlementViewer;

    public Failable<SettlementProjection> handle(Long id, PaymentConfirmationRequest request) {

        User currentUser = currentUserService.get();
        log.debug(
                "Request to set payment receipt confirmation to: {}, id: {}, by user: {}",
                request.isReceived(),
                id,
                currentUser.getLogin()
        );
        Optional<Payment> optionalPayment = settlementRepository.findPayment(id);
        if (optionalPayment.isEmpty()) {
            return error(PAYMENT_DOES_NOT_EXIST);
        }
        Payment payment = optionalPayment.get();
        Settlement settlement = payment.getSettlement();
        if (not(settlement.getUsers().contains(currentUser))) {
            return error(USER_NOT_INCLUDED_IN_SETTLEMENT);
        }
        if (payment.getTo().equals(currentUser)) {
            payment.setReceived(request.isReceived());
            settlementRepository.save(payment);
        } else {
            return error(USER_IS_NOT_THE_PAYEE);
        }
        boolean updateNeeded = settlement.updateSettlementConfirmation();
        if (updateNeeded) {
            settlementRepository.save(settlement);
        }

        return success(settlementViewer.get(settlement));
    }
}
