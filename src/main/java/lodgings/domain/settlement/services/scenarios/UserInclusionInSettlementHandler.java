package lodgings.domain.settlement.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.data.errors.UserInclusionErrors;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.SettlementRepository;
import lodgings.domain.settlement.services.SettlementViewer;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.domain.settlement.data.errors.UserInclusionErrors.USER_NOT_FOUND;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class UserInclusionInSettlementHandler {

    private final SettlementRepository settlementRepository;
    private final UserRepository userRepository;
    private final SettlementViewer settlementViewer;

    public Failable<SettlementProjection> handle(Long id, boolean include) {
        var action = include ? "include" : "exclude";
        log.debug("Request to {} user with id = {} in/from the current settlement", action, id);
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty()) {
            return Failable.error(USER_NOT_FOUND);
        }
        User user = optionalUser.get();
        if (not(user.hasRole(Role.USER))) {
            return Failable.error(UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE);
        }
        Settlement settlement = settlementRepository.findSettlementFetchUsersAndExpenses(YearMonth.now());
        if (include && not(settlement.getUsers().contains(user))) {
            user.setAllowedToLogin(true);
            userRepository.save(user);
            settlement.getUsers().add(user);
        } else if (not(include)) {
            settlement.getUsers().remove(user);
        }
        settlementRepository.save(settlement);
        return Failable.success(settlementViewer.get(settlement));
    }
}
