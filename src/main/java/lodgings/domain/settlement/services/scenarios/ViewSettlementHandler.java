package lodgings.domain.settlement.services.scenarios;

import io.ebean.annotation.Transactional;
import io.micronaut.core.annotation.NonNull;
import lodgings.domain.settlement.data.projections.SettlementProjection;
import lodgings.domain.settlement.services.SettlementViewer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.Optional;

@Slf4j
@Singleton
@Transactional
@RequiredArgsConstructor
public class ViewSettlementHandler {

    private final SettlementViewer settlementViewer;

    public Optional<SettlementProjection> handle(@NonNull YearMonth month) {
        log.debug("Request to view settlement for month: {}", month);
        return settlementViewer.get(month);
    }
}
