package lodgings.domain.users.data.entities;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@FieldNameConstants
@Table(name = "roles")
public class Role {

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String RESET_PASSWORD = "RESET_PASSWORD"; // special "role", only for what it says

    @Id
    private final Long id;

    @Column(name = "name")
    private final String name;
}
