package lodgings.domain.users.data.entities;

import at.favre.lib.crypto.bcrypt.BCrypt;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@FieldNameConstants
@Table(name = "users")
@NoArgsConstructor
@EqualsAndHashCode(exclude = "roles")
public class User {

    public static final int MIN_LOGIN_LENGTH = 4;
    public static final int MAX_LOGIN_LENGTH = 30;
    public static final int MIN_NAME_LENGTH = 2;
    public static final int MAX_NAME_LENGTH = 40;
    public static final int MIN_PASSWORD_LENGTH = 5;
    public static final int MAX_PASSWORD_LENGTH = 50;

    public static final String ROLES = String.join(", ", List.of(Role.ADMIN, Role.USER));

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "name")
    private String name;

    @Column(name = "password_reset_required")
    private boolean passwordResetRequired;

    @Column(name = "allowed_to_login")
    private boolean allowedToLogin;

     // or even current, in the case of first app startup ever:
     // see SettlementKeeper#ensureCurrentSettlementExists
    @Column(name = "auto_include_in_next_settlement")
    private boolean autoIncludeInNextSettlement;

    @ManyToMany
    @JoinTable(name = "users_to_roles", joinColumns = {
            @JoinColumn(name = "user_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "role_id")
    })
    private List<Role> roles;

    public boolean passwordMatches(String password) {
        return password != null && BCrypt.verifyer().verify(password.toCharArray(), passwordHash).verified;
    }

    public boolean hasRole(String name) {
        return roles.stream().anyMatch(role -> role.getName().equals(name));
    }

    public User(String login, String name, String passwordHash, List<Role> roles, boolean allowedToLogin, boolean autoIncludeInNextSettlement) {
        this.login = login;
        this.name = name;
        this.passwordHash = passwordHash;
        this.roles = roles;
        this.passwordResetRequired = true;
        this.allowedToLogin = allowedToLogin;
        this.autoIncludeInNextSettlement = autoIncludeInNextSettlement;
    }
}