package lodgings.domain.users.data.errors;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;

public class PasswordChangeErrors {
    public static final Error AUTHENTICATION_FAILED = new Error("AUTHENTICATION_FAILED");
    public static final Error NEW_PASSWORD_MISSING = new Error("NEW_PASSWORD_MISSING");
    public static final Error NEW_PASSWORD_TOO_SHORT = new Error("NEW_PASSWORD_TOO_SHORT", User.MIN_PASSWORD_LENGTH);
    public static final Error NEW_PASSWORD_TOO_LONG = new Error("NEW_PASSWORD_TOO_LONG", User.MAX_PASSWORD_LENGTH);
    public static final Error NEW_PASSWORD_EQUALS_OLD = new Error("NEW_PASSWORD_EQUALS_OLD");
}
