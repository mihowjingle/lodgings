package lodgings.domain.users.data.errors;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;

public class UserCreationErrors {
    public static final Error LOGIN_MISSING = new Error("LOGIN_MISSING");
    public static final Error LOGIN_TOO_SHORT = new Error("LOGIN_TOO_SHORT", User.MIN_LOGIN_LENGTH);
    public static final Error LOGIN_TOO_LONG = new Error("LOGIN_TOO_LONG", User.MAX_PASSWORD_LENGTH);
    public static final Error LOGIN_NOT_UNIQUE = new Error("LOGIN_NOT_UNIQUE");
}
