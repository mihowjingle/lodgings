package lodgings.domain.users.data.errors;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;

/**
 * Persist = Create or Update, these errors are shared between the two actions
 */
public class UserPersistErrors {
    public static final Error NAME_MISSING = new Error("NAME_MISSING");
    public static final Error NAME_TOO_SHORT = new Error("NAME_TOO_SHORT", User.MIN_NAME_LENGTH);
    public static final Error NAME_TOO_LONG = new Error("NAME_TOO_LONG", User.MAX_NAME_LENGTH);
    public static final Error NAME_NOT_UNIQUE = new Error("USER_NAME_NOT_UNIQUE"); // todo #27
    public static final Error ROLES_MISSING = new Error("ROLES_MISSING");
    public static final Error ROLES_UNRECOGNIZED = new Error("ROLES_UNRECOGNIZED", User.ROLES);
}
