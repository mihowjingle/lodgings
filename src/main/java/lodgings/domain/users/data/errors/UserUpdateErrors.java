package lodgings.domain.users.data.errors;

import lodgings.common.data.errors.Error;

public class UserUpdateErrors {
    public static final Error USER_NOT_FOUND = new Error("USER_NOT_FOUND");
    public static final Error ADMIN_REVOKED_FOR_ADMIN = new Error("ADMIN_REVOKED_FOR_ADMIN");
    public static final Error LOGIN_DISALLOWED_FOR_ADMIN = new Error("LOGIN_DISALLOWED_FOR_ADMIN");
}
