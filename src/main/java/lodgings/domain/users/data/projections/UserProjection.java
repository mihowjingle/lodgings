package lodgings.domain.users.data.projections;

import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.entities.User;
import lombok.Data;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
public class UserProjection {
    private final Long id;
    private final String login;
    private final String name;
    private final boolean passwordResetRequired;
    private final boolean allowedToLogin;
    private final boolean autoIncludeInNextSettlement;
    private final List<String> roles;

    public UserProjection(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.name = user.getName();
        this.passwordResetRequired = user.isPasswordResetRequired();
        this.allowedToLogin = user.isAllowedToLogin();
        this.autoIncludeInNextSettlement = user.isAutoIncludeInNextSettlement();
        this.roles = user.getRoles().stream().map(Role::getName).collect(toList());
    }
}
