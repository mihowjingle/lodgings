package lodgings.domain.users.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PasswordChangeRequest {
    private final String oldPassword;
    private final String newPassword;

    @JsonCreator
    public PasswordChangeRequest(@JsonProperty("oldPassword") String oldPassword,
                                 @JsonProperty("newPassword") String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}
