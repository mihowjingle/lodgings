package lodgings.domain.users.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserCreationRequest implements UserPersistRequest {
    private final String name;
    private final List<String> roles;
    private final String login;
    private final boolean allowedToLogin;
    private final boolean autoIncludeInNextSettlement;
    private final boolean addToCurrentSettlement;

    @JsonCreator
    public UserCreationRequest(@JsonProperty("login") String login,
                               @JsonProperty("name") String name,
                               @JsonProperty("roles") List<String> roles,
                               @JsonProperty("allowedToLogin") boolean allowedToLogin,
                               @JsonProperty("autoIncludeInNextSettlement") boolean autoIncludeInNextSettlement,
                               @JsonProperty("addToCurrentSettlement") boolean addToCurrentSettlement) {
        this.name = name;
        this.roles = roles;
        this.login = login;
        this.allowedToLogin = allowedToLogin;
        this.autoIncludeInNextSettlement = autoIncludeInNextSettlement;
        this.addToCurrentSettlement = addToCurrentSettlement;
    }
}
