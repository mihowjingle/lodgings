package lodgings.domain.users.data.requests;

import java.util.List;

public interface UserPersistRequest {
    String getName();
    List<String> getRoles();
    boolean isAutoIncludeInNextSettlement();
}
