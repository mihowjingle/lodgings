package lodgings.domain.users.data.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserUpdateRequest implements UserPersistRequest {
    private final String name;
    private final List<String> roles;
    private final boolean passwordResetRequired;
    private final boolean allowedToLogin;
    private final boolean autoIncludeInNextSettlement;

    @JsonCreator
    public UserUpdateRequest(@JsonProperty("name") String name,
                             @JsonProperty("passwordResetRequired") boolean passwordResetRequired,
                             @JsonProperty("allowedToLogin") boolean allowedToLogin,
                             @JsonProperty("autoIncludeInNextSettlement") boolean autoIncludeInNextSettlement,
                             @JsonProperty("roles") List<String> roles) {
        this.name = name;
        this.roles = roles;
        this.passwordResetRequired = passwordResetRequired;
        this.allowedToLogin = allowedToLogin;
        this.autoIncludeInNextSettlement = autoIncludeInNextSettlement;
    }
}
