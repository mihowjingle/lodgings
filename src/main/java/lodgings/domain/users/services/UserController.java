package lodgings.domain.users.services;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.settlement.data.projections.AdminSettlementUserProjection;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.projections.UserProjection;
import lodgings.domain.users.data.requests.PasswordChangeRequest;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.data.requests.UserUpdateRequest;
import lodgings.domain.users.services.scenarios.*;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@Controller("/api/users")
@RequiredArgsConstructor
@Secured(SecurityRule.IS_AUTHENTICATED)
public class UserController {

    private final UserCreationHandler userCreationHandler;
    private final UserUpdateHandler userUpdateHandler;
    private final UserPasswordChangeHandler userPasswordChangeHandler;
    private final ViewSpecificUserHandler viewSpecificUserHandler;
    private final ViewUsersHandler viewUsersHandler;
    private final NotInCurrentSettlementUserDictionaryHandler notInCurrentSettlementUserDictionaryHandler;

    @Post
    @Secured(Role.ADMIN)
    public HttpResponse<?> save(@Body UserCreationRequest request) {
        return userCreationHandler.handle(request).toHttpResponse();
    }

    @Put("/{id}")
    @Secured(Role.ADMIN)
    public HttpResponse<?> update(Long id, @Body UserUpdateRequest request) {
        return userUpdateHandler.handle(id, request).toHttpResponse();
    }

    @Post("/change-password")
    @Secured({Role.USER, Role.RESET_PASSWORD})
    public HttpResponse<?> changePassword(@Body PasswordChangeRequest request) {
        return userPasswordChangeHandler.handle(request).toHttpResponse();
    }

    @Get("/{id}")
    @Secured(Role.ADMIN)
    public Optional<UserProjection> viewUser(Long id) {
        return viewSpecificUserHandler.handle(id);
    }

    @Get("{?request*}")
    @Secured(Role.ADMIN)
    public HttpResponse<?> viewUsers(PaginationRequest request) {
        return viewUsersHandler.handle(request).toHttpResponse();
    }

    @Get("/not-in-current-settlement")
    @Secured(Role.ADMIN)
    public List<AdminSettlementUserProjection> getUsersNotInTheCurrentSettlementAsDictionary() {
        return notInCurrentSettlementUserDictionaryHandler.get();
    }
}