package lodgings.domain.users.services;

import io.ebean.Database;
import io.ebean.PagedList;
import io.ebean.annotation.Transactional;
import io.ebean.annotation.TxType;
import io.ebean.cache.ServerCacheManager;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.entities.User;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static lodgings.common.Queries.properties;

@Singleton
@RequiredArgsConstructor
@Transactional(type = TxType.MANDATORY)
public class UserRepository {

    private final Database db;

    public User save(User user) {
        db.save(user);
        return user;
    }

    public void saveAll(List<User> users) {
        db.saveAll(users);
    }

    public boolean userWithLoginExists(String login) {
        return db.find(User.class).where().eq(User.Fields.login, login).exists();
    }

    public boolean userWithNameExists(String name) {
        return db.find(User.class).where().eq(User.Fields.name, name).exists();
    }

    public boolean otherUserWithNameExists(Long id, String name) {
        return db.find(User.class).where()
                .eq(User.Fields.name, name)
                .ne(User.Fields.id, id)
                .exists();
    }

    public List<Role> findRoles(List<String> names) {
        return db.find(Role.class).where().in(Role.Fields.name, names).findList();
    }

    public Optional<User> findByLogin(String login) {
        return db.find(User.class).where().eq(User.Fields.login, login).findOneOrEmpty();
    }

    public Optional<User> findForAuthentication(String login) {
        return db.find(User.class).select(properties(
                User.Fields.name,
                User.Fields.allowedToLogin,
                User.Fields.passwordHash,
                User.Fields.passwordResetRequired
        )).fetch(
                User.Fields.roles,
                Role.Fields.name
        ).where()
                .eq(User.Fields.login, login)
                .findOneOrEmpty();
    }

    public Optional<User> findById(Long id) {
        return db.find(User.class).where().idEq(id).findOneOrEmpty();
    }

    public PagedList<User> findMany(PaginationRequest request) {
        return db.find(User.class)
                .setFirstRow(request.getFirstRow())
                .setMaxRows(request.getSize())
                .setOrderBy(request.getOrderBy())
                .findPagedList();
    }

    public List<User> findToBeIncludedInTheNextSettlement() {
        return db.find(User.class).where().eq(User.Fields.autoIncludeInNextSettlement, true).findList();
    }

    // we expect few rows in the subquery, that's why this is acceptable
    public List<User> findOnlyUsersNotInCurrentSettlementSelectIdNameAndLogin() {
        String sql = "SELECT exu.id as id, exu.login as login, exu.name as name " +
                "FROM users exu " +
                "         JOIN users_to_roles utr on exu.id = utr.user_id " +
                "         JOIN roles r on r.id = utr.role_id " +
                "WHERE exu.id NOT IN ( " +
                "    SELECT inu.id " +
                "    FROM users inu " +
                "             JOIN settlements_to_users stu on inu.id = stu.user_id " +
                "             JOIN settlements s on stu.settlement_id = s.id " +
                "    WHERE s.year_month = :yearMonth " +
                ") " +
                "  AND r.name = 'USER' " +
                "ORDER BY exu.name, exu.login;";
        return db.findNative(User.class, sql)
                .setParameter("yearMonth", YearMonth.now())
                .findList();
    }

    /**
     * "termination period" is just for quick, intuitive explanation, it's not an "official" term:
     * it's when a user is not in the current settlement and not in the next,
     * but they were left allowed to login for now, so that they can "finalize"
     * their business, pay or accept payments, mostly, etc.
     *
     * 1. allowed to login and
     * 2. not in current settlement and
     * 3. not in next settlement
     *
     * then
     *
     * allowedToLogin = false
     */
    public int completeTerminationPeriod() {
        String sql = "UPDATE users " +
                "SET allowed_to_login = false " +
                "WHERE id NOT IN ( " +
                "    SELECT u.id " +
                "    FROM users u " +
                "             JOIN settlements_to_users stu on u.id = stu.user_id " +
                "             JOIN settlements s on stu.settlement_id = s.id " +
                "    WHERE s.year_month = :yearMonth " +
                ") AND allowed_to_login = true AND auto_include_in_next_settlement = false;";
        int usersAffected = db.sqlUpdate(sql)
                .setParameter("yearMonth", YearMonth.now())
                .execute();
        ServerCacheManager serverCacheManager = db.getServerCacheManager();
        serverCacheManager.clear(User.class);
        return usersAffected;
    }
}
