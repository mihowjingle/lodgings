package lodgings.domain.users.services.passwords;

import javax.inject.Singleton;
import java.util.Random;

@Singleton
public class PasswordGenerator {

    private final String source = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
    private final Random random = new Random();

    public String generate() {
        return randomStringOfLength(randomBetweenInclusive(5, 7));
    }

    private String randomStringOfLength(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(source.charAt(random.nextInt(source.length())));
        }
        return sb.toString();
    }

    private int randomBetweenInclusive(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }
}
