package lodgings.domain.users.services.passwords;

import at.favre.lib.crypto.bcrypt.BCrypt;
import lodgings.config.LodgingsConfiguration;

import javax.inject.Singleton;

@Singleton
public class PasswordHasher {

    private final int passwordHashingStrength;

    public PasswordHasher(LodgingsConfiguration configuration) {
        passwordHashingStrength = configuration.getPasswordHashingStrength();
    }

    @SuppressWarnings("unused") // lombok delegate: PasswordManager
    public String hash(String password) {
        return BCrypt.withDefaults().hashToString(passwordHashingStrength, password.toCharArray());
    }
}
