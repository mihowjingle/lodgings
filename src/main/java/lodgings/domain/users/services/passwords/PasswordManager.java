package lodgings.domain.users.services.passwords;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class PasswordManager {

    @Delegate
    private final PasswordGenerator generator;

    @Delegate
    private final PasswordHasher hasher;
}
