package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.settlement.data.projections.AdminSettlementUserProjection;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Singleton
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class NotInCurrentSettlementUserDictionaryHandler {

    private final UserRepository repository;

    public List<AdminSettlementUserProjection> get() {
        log.debug("Request to get users not in current settlement (id, login, name) as dictionary");
        return repository.findOnlyUsersNotInCurrentSettlementSelectIdNameAndLogin().stream()
                .map(AdminSettlementUserProjection::new)
                .collect(toList());
    }
}
