package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.services.SettlementRepository;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.errors.UserPersistErrors;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.services.UserRepository;
import lodgings.domain.users.services.passwords.PasswordManager;
import lodgings.domain.users.services.validators.UserCreationValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.List;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserCreationHandler {

    private final UserRepository userRepository;
    private final PasswordManager passwordManager;
    private final UserCreationValidator userCreationValidator;
    private final SettlementRepository settlementRepository;

    @Transactional
    public Failable<String> handle(UserCreationRequest request) {
        log.debug("Request to create and save user with login: {}", request.getLogin());
        List<Error> errors = userCreationValidator.validate(request);
        if (errors.isEmpty()) {
            if (userRepository.userWithNameExists(request.getName())) {
                log.debug("Failed to create user with login: {}! Duplicate name!", request.getLogin());
                return Failable.error(UserPersistErrors.NAME_NOT_UNIQUE);
            }
            String generatedPassword = passwordManager.generate();
            User user = new User(
                    request.getLogin(),
                    request.getName(),
                    passwordManager.hash(generatedPassword),
                    userRepository.findRoles(request.getRoles()),
                    request.isAllowedToLogin(),
                    request.isAutoIncludeInNextSettlement()
            );
            userRepository.save(user);
            if (request.isAddToCurrentSettlement()) {
                Settlement currentSettlement = settlementRepository.findSettlement(YearMonth.now());
                currentSettlement.getUsers().add(user);
                settlementRepository.save(currentSettlement);
            }
            log.debug("Successfully created user with login: {}", user.getLogin());
            return Failable.success(generatedPassword);
        }
        log.debug("Failed to create user with login: {}!", request.getLogin());
        return Failable.error(errors);
    }
}