package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.common.services.CurrentUserService;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.requests.PasswordChangeRequest;
import lodgings.domain.users.services.UserRepository;
import lodgings.domain.users.services.passwords.PasswordManager;
import lodgings.domain.users.services.validators.UserPasswordChangeValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.domain.users.data.errors.PasswordChangeErrors.AUTHENTICATION_FAILED;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserPasswordChangeHandler {

    private final UserRepository userRepository;
    private final PasswordManager passwordManager;
    private final UserPasswordChangeValidator userPasswordChangeValidator;
    private final CurrentUserService currentUserService;

    @Transactional
    public Failable<Void> handle(PasswordChangeRequest request) {
        User currentUser = currentUserService.get();
        log.debug("Request to change password by user with login: {}", currentUser.getLogin());
        List<Error> errors = new ArrayList<>();
        Optional<Error> passwordError = userPasswordChangeValidator.validate(request);
        passwordError.ifPresent(errors::add);
        if (not(currentUser.isAllowedToLogin() && currentUser.passwordMatches(request.getOldPassword()))) {
            errors.add(AUTHENTICATION_FAILED);
            return Failable.error(errors);
        }
        if (not(errors.isEmpty())) {
            log.debug("Failed to change password for user with login: {}!", currentUser.getLogin());
            return Failable.error(errors);
        }
        currentUser.setPasswordHash(passwordManager.hash(request.getNewPassword()));
        currentUser.setPasswordResetRequired(false);
        userRepository.save(currentUser);
        log.debug("Successfully changed password for user with login: {}", currentUser.getLogin());
        return Failable.success();
    }
}