package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.errors.Error;
import lodgings.domain.settlement.data.entities.Settlement;
import lodgings.domain.settlement.services.SettlementRepository;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.errors.UserPersistErrors;
import lodgings.domain.users.data.requests.UserUpdateRequest;
import lodgings.domain.users.services.UserRepository;
import lodgings.domain.users.services.validators.UserUpdateValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.data.errors.Error.ID_MISSING;
import static lodgings.domain.users.data.errors.UserUpdateErrors.*;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserUpdateHandler {

    private final UserRepository userRepository;
    private final UserUpdateValidator userUpdateValidator;
    private final SettlementRepository settlementRepository;

    @Transactional
    public Failable<Void> handle(Long id, UserUpdateRequest request) {
        log.debug("Request to update user with id: {}", id);
        List<Error> errors = userUpdateValidator.validate(request);
        if (id == null) {
            log.warn("Attempt to update a non-identifiable user! ID is null!");
            errors.add(ID_MISSING);
            return Failable.error(errors);
        }
        if (userRepository.otherUserWithNameExists(id, request.getName())) {
            log.debug("Failed to update user with id: {}! Duplicate name!", id);
            return Failable.error(UserPersistErrors.NAME_NOT_UNIQUE);
        }
        Optional<User> maybeUser = userRepository.findById(id);
        if (maybeUser.isEmpty()) {
            log.warn("Attempt to update a non-existent user! User id: {}", id);
            errors.add(USER_NOT_FOUND);
            return Failable.error(errors);
        }
        User user = maybeUser.get();
        if ("admin".equals(user.getLogin())) {
            if (not(request.getRoles().contains(Role.ADMIN))) {
                errors.add(ADMIN_REVOKED_FOR_ADMIN);
                log.warn("Attempt to revoke admin rights for (the default) admin!");
            }
            if (not(request.isAllowedToLogin())) {
                errors.add(LOGIN_DISALLOWED_FOR_ADMIN);
                log.warn("Attempt to disallow (the default) admin from logging in!");
            }
        }
        if (not(errors.isEmpty())) {
            log.debug("Failed to update user with id: {}!", id);
            return Failable.error(errors);
        }
        user.setName(request.getName());
        user.setPasswordResetRequired(request.isPasswordResetRequired());
        synchronizeRoles(user, userRepository.findRoles(request.getRoles()));
        user.setAllowedToLogin(request.isAllowedToLogin());
        user.setAutoIncludeInNextSettlement(request.isAutoIncludeInNextSettlement());
        userRepository.save(user);
        if (not(user.isAllowedToLogin()) || not(user.hasRole(Role.USER))) {
            log.debug("User '{}' is not allowed to login, or does not have USER role. Excluding the user from the current settlement (if needed).", user.getLogin());
            Settlement currentSettlement = settlementRepository.findSettlement(YearMonth.now());
            if (currentSettlement.getUsers().contains(user)) {
                currentSettlement.getUsers().remove(user);
                settlementRepository.save(currentSettlement);
            }
        }
        log.debug("Successfully updated user with id: {}", id);
        return Failable.success();
    }

    private void synchronizeRoles(User user, List<Role> intendedRoles) {
        user.getRoles().removeIf(role -> not(intendedRoles.contains(role)));
        intendedRoles.stream()
                .filter(role -> not(user.getRoles().contains(role)))
                .forEach(role -> user.getRoles().add(role));
    }
}