package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.domain.users.data.projections.UserProjection;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Optional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ViewSpecificUserHandler {

    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public Optional<UserProjection> handle(Long id) {
        log.debug("Request to get user with id: {}", id);
        return userRepository.findById(id).map(UserProjection::new);
    }
}
