package lodgings.domain.users.services.scenarios;

import io.ebean.annotation.Transactional;
import lodgings.common.data.Failable;
import lodgings.common.data.pagination.Page;
import lodgings.common.data.pagination.PagedEntity;
import lodgings.common.data.pagination.PaginationRequest;
import lodgings.domain.users.data.projections.UserProjection;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ViewUsersHandler {

    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public Failable<Page<UserProjection>> handle(PaginationRequest request) {
        log.debug("Request to view a page of users");
        final var errors = request.validateFor(PagedEntity.USER);
        if (errors.isEmpty()) {
            return Failable.success(new Page<>(userRepository.findMany(request), UserProjection::new));
        }
        return Failable.error(errors);
    }
}
