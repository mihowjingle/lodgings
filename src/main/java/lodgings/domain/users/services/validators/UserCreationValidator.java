package lodgings.domain.users.services.validators;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.services.validators.atomic.AtomicUserCreationValidator;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Singleton
@RequiredArgsConstructor
public class UserCreationValidator {

    private final List<AtomicUserCreationValidator> validators;

    public List<Error> validate(final UserCreationRequest request) {
        return validators.stream()
                .map(validator -> validator.validate(request))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }
}
