package lodgings.domain.users.services.validators;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.errors.PasswordChangeErrors;
import lodgings.domain.users.data.requests.PasswordChangeRequest;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class UserPasswordChangeValidator {

    public Optional<Error> validate(PasswordChangeRequest request) {
        if (request.getNewPassword() == null) {
            return Optional.of(PasswordChangeErrors.NEW_PASSWORD_MISSING);
        }
        if (request.getNewPassword().length() > User.MAX_PASSWORD_LENGTH) {
            return Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_LONG);
        }
        if (request.getNewPassword().length() < User.MIN_PASSWORD_LENGTH) {
            return Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_SHORT);
        }
        if (request.getNewPassword().equals(request.getOldPassword())) {
            return Optional.of(PasswordChangeErrors.NEW_PASSWORD_EQUALS_OLD);
        }
        return Optional.empty();
    }
}
