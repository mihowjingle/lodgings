package lodgings.domain.users.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.requests.UserCreationRequest;

import java.util.Optional;

public interface AtomicUserCreationValidator {
    Optional<Error> validate(UserCreationRequest request);
}
