package lodgings.domain.users.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.requests.UserUpdateRequest;

import java.util.Optional;

public interface AtomicUserUpdateValidator {
    Optional<Error> validate(UserUpdateRequest request);
}
