package lodgings.domain.users.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.errors.UserCreationErrors;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.services.UserRepository;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor
public class UserLoginValidator implements AtomicUserCreationValidator {

    private final UserRepository userRepository;

    @Override
    public Optional<Error> validate(UserCreationRequest request) {
        if (request.getLogin() == null) {
            return Optional.of(UserCreationErrors.LOGIN_MISSING);
        }
        if (request.getLogin().length() > User.MAX_LOGIN_LENGTH) {
            return Optional.of(UserCreationErrors.LOGIN_TOO_LONG);
        }
        if (request.getLogin().length() < User.MIN_LOGIN_LENGTH) {
            return Optional.of(UserCreationErrors.LOGIN_TOO_SHORT);
        }
        if (userRepository.userWithLoginExists(request.getLogin())) {
            return Optional.of(UserCreationErrors.LOGIN_NOT_UNIQUE);
        }
        return Optional.empty();
    }
}
