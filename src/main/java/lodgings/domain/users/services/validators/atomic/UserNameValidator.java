package lodgings.domain.users.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.users.data.entities.User;
import lodgings.domain.users.data.errors.UserPersistErrors;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.data.requests.UserPersistRequest;
import lodgings.domain.users.data.requests.UserUpdateRequest;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class UserNameValidator implements AtomicUserCreationValidator, AtomicUserUpdateValidator {

    @Override
    public Optional<Error> validate(UserCreationRequest request) {
        return validateInternally(request);
    }

    @Override
    public Optional<Error> validate(UserUpdateRequest request) {
        return validateInternally(request);
    }

    private Optional<Error> validateInternally(UserPersistRequest request) {
        if (request.getName() == null) {
            return Optional.of(UserPersistErrors.NAME_MISSING);
        }
        if (request.getName().length() > User.MAX_NAME_LENGTH) {
            return Optional.of(UserPersistErrors.NAME_TOO_LONG);
        }
        if (request.getName().length() < User.MIN_NAME_LENGTH) {
            return Optional.of(UserPersistErrors.NAME_TOO_SHORT);
        }
        return Optional.empty();
    }
}
