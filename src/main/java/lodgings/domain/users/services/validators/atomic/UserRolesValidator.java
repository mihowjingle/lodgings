package lodgings.domain.users.services.validators.atomic;

import lodgings.common.data.errors.Error;
import lodgings.domain.settlement.data.errors.UserInclusionErrors;
import lodgings.domain.users.data.entities.Role;
import lodgings.domain.users.data.errors.UserPersistErrors;
import lodgings.domain.users.data.requests.UserCreationRequest;
import lodgings.domain.users.data.requests.UserPersistRequest;
import lodgings.domain.users.data.requests.UserUpdateRequest;

import javax.inject.Singleton;
import java.util.Optional;

import static lodgings.common.SyntacticSugar.not;
import static lodgings.common.SyntacticSugar.nullOrEmpty;

@Singleton
public class UserRolesValidator implements AtomicUserCreationValidator, AtomicUserUpdateValidator {

    @Override
    public Optional<Error> validate(UserCreationRequest request) {
        Optional<Error> optionalError = validateInternally(request);
        if (optionalError.isPresent()) {
            return optionalError;
        }
        if (request.getRoles().stream().noneMatch(Role.USER::equals) && request.isAddToCurrentSettlement()) {
            return Optional.of(UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Error> validate(UserUpdateRequest request) {
        return validateInternally(request);
    }

    private Optional<Error> validateInternally(UserPersistRequest request) {
        if (nullOrEmpty(request.getRoles())) {
            return Optional.of(UserPersistErrors.ROLES_MISSING);
        }
        if (request.getRoles().stream().anyMatch(r -> not(Role.USER.equals(r)) && not(Role.ADMIN.equals(r)))) {
            return Optional.of(UserPersistErrors.ROLES_UNRECOGNIZED);
        }
        if (request.getRoles().stream().noneMatch(Role.USER::equals) && request.isAutoIncludeInNextSettlement()) {
            return Optional.of(UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE);
        }
        return Optional.empty();
    }
}
