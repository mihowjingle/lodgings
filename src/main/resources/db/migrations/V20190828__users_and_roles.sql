CREATE TABLE roles (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO roles(id, name) VALUES (1, 'ADMIN'), (2, 'USER');

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    login VARCHAR(30) NOT NULL UNIQUE,
    password_hash CHAR(60) NOT NULL,
    name VARCHAR(40) NOT NULL UNIQUE,
    password_reset_required BOOLEAN NOT NULL,
    allowed_to_login BOOLEAN NOT NULL,
    auto_include_in_next_settlement BOOLEAN NOT NULL
);

CREATE INDEX search_users_login_idx ON users(login);

CREATE TABLE users_to_roles (
    user_id BIGINT NOT NULL REFERENCES users,
    role_id BIGINT NOT NULL REFERENCES roles
);

CREATE INDEX fk_users_to_roles_user_id_idx ON users_to_roles(user_id);
CREATE INDEX fk_users_to_roles_role_id_idx ON users_to_roles(role_id);

INSERT INTO users(login, password_hash, name, password_reset_required, allowed_to_login, auto_include_in_next_settlement) VALUES (
    'admin',
    '$2a$12$xwkEExNdJECM7FZ/0bd3MONjJPbrOwqFEQPEqvyT1wn2G7aK2kxOe',
    'Michał',
    true,
    true,
    true
);

INSERT INTO users_to_roles(user_id, role_id) VALUES (1, 1), (1, 2);