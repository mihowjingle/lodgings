CREATE TABLE purposes (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    last_modified_by_id BIGINT NOT NULL REFERENCES users
);

CREATE INDEX fk_purposes_users_idx ON purposes(last_modified_by_id);

CREATE TABLE items (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    assessable BOOLEAN NOT NULL,
    purpose_id BIGINT REFERENCES purposes,
    last_modified_by_id BIGINT NOT NULL REFERENCES users
);

CREATE INDEX fk_items_purposes_idx ON items(purpose_id);
CREATE INDEX fk_items_users_idx ON items(last_modified_by_id);