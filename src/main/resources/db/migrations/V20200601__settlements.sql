CREATE TABLE settlements (
    id BIGSERIAL PRIMARY KEY,
    year_month DATE NOT NULL UNIQUE,
    settled BOOLEAN NOT NULL,
    payments_calculated BOOLEAN NOT NULL
);

CREATE TABLE settlements_to_users (
    user_id BIGINT NOT NULL REFERENCES users,
    settlement_id BIGINT NOT NULL REFERENCES settlements
);

CREATE INDEX fk_settlements_to_users_user_id_idx ON settlements_to_users(user_id);
CREATE INDEX fk_settlements_to_users_settlement_id_idx ON settlements_to_users(settlement_id);