CREATE TABLE expenses (
    id BIGSERIAL PRIMARY KEY,
    total_price DECIMAL(6,2) NOT NULL,
    when_bought TIMESTAMP NOT NULL,
    buyer_id BIGINT NOT NULL REFERENCES users,
    item_id BIGINT NOT NULL REFERENCES items,
    settlement_id BIGINT NOT NULL REFERENCES settlements
);

CREATE INDEX fk_expenses_users_idx ON expenses(buyer_id);
CREATE INDEX fk_expenses_items_idx ON expenses(item_id);
CREATE INDEX fk_expenses_settlements_idx ON expenses(settlement_id);