CREATE TABLE payments (
    id BIGSERIAL PRIMARY KEY,
    payer_id BIGINT NOT NULL REFERENCES users,
    payee_id BIGINT NOT NULL REFERENCES users,
    settlement_id BIGINT NOT NULL REFERENCES settlements,
    amount DECIMAL(8,2) NOT NULL,
    received BOOLEAN NOT NULL
);

CREATE INDEX fk_payments_payers_idx ON payments(payer_id);
CREATE INDEX fk_payments_payees_idx ON payments(payee_id);
CREATE INDEX fk_payments_settlements_idx ON payments(settlement_id);