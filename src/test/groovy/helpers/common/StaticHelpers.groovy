package helpers.common

import io.ebean.OrderBy
import lodgings.domain.users.data.projections.UserProjection

import static lodgings.common.SyntacticSugar.not

class StaticHelpers {

    static def stringOfLength(int length) {
        def builder = new StringBuilder()
        for (def i = 0; i < length; i++) {
            builder.append('a')
        }
        builder.toString()
    }

    static def ascending(String property) {
        def order = new OrderBy<Object>()
        order.asc(property)
        order
    }

    static def descending(String property) {
        def order = new OrderBy<Object>()
        order.desc(property)
        order
    }

    static final def isSortedAscendingByLogin = { List<UserProjection> users ->
        users.size() < 2 || (1..<users.size()).every { users[it - 1].login <= users[it].login }
    }

    static final def isSortedDescendingByLogin = { List<UserProjection> users ->
        users.size() < 2 || (1..<users.size()).every { users[it - 1].login >= users[it].login }
    }

    static final def isNotSortedByLogin = { List<UserProjection> users ->
        not(isSortedAscendingByLogin.call(users)) && not(isSortedDescendingByLogin.call(users))
    }
}
