package helpers.common

import io.micronaut.runtime.server.EmbeddedServer
import kong.unirest.Unirest
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper

import javax.inject.Singleton

@Singleton
class UnirestWrapper {

    private final UnirestInstance unirest
    private final String baseUri

    UnirestWrapper(EmbeddedServer embeddedServer) {
        Unirest.config().setObjectMapper(new JacksonObjectMapper())
        Unirest.config().setDefaultHeader("Content-Type", "application/json")
        unirest = Unirest.primaryInstance()
        baseUri = embeddedServer.getURI()
    }

    def post(String url) {
        unirest.post(baseUri + url)
    }

    def put(String url) {
        unirest.put(baseUri + url)
    }

    def get(String url) {
        unirest.get(baseUri + url)
    }

    def delete(String url) {
        unirest.delete(baseUri + url)
    }
}
