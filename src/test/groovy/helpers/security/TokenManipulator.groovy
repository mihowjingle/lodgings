package helpers.security

import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.micronaut.context.annotation.Property
import lodgings.domain.users.data.entities.Role

import javax.crypto.spec.SecretKeySpec
import javax.inject.Singleton

@Singleton
class TokenManipulator {

    private final String jwtSigningKey
    private final JwtParser parser

    TokenManipulator(@Property(name = "micronaut.security.token.jwt.signatures.secret.generator.secret") String jwtSigningKey) {
        this.jwtSigningKey = jwtSigningKey
        parser = Jwts.parser().setSigningKey(jwtSigningKey.getBytes())
    }

    def parse(String token) {
        parser.parseClaimsJws(token)
    }

    def fakeToken(String login, List<String> roles) {
        Date now = new Date()
        "Bearer " + Jwts.builder()
                .setIssuedAt(now)
                .setExpiration(tomorrow())
                .setSubject(login)
                .claim("roles", roles)
                .signWith(new SecretKeySpec(jwtSigningKey.getBytes(), SignatureAlgorithm.HS256.getJcaName()))
                .compact()
    }

    def fakeTamperedToken() {
        def token = fakeToken("someone", [Role.USER])

        def bearerHeader = token.split("\\.")[0]
        def payload = token.split("\\.")[1]
        def signature = token.split("\\.")[2]

        def decodedPayload = new String(payload.decodeBase64())
        def modifiedPayload = Base64.getEncoder().encodeToString(decodedPayload.replace(Role.USER, Role.ADMIN).bytes)

        String.join(".", bearerHeader, modifiedPayload, signature)
    }

    private static def tomorrow() {
        Date now = new Date()
        Calendar c = Calendar.getInstance()
        c.setTime(now)
        c.add(Calendar.DATE, 1)
        return c.getTime()
    }
}
