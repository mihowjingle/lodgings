package helpers.settlements

import io.ebean.Database
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.users.data.entities.User

import javax.inject.Singleton
import java.time.LocalDateTime
import java.time.YearMonth

@Singleton
class TestSettlementHelper {

    private final Database db

    TestSettlementHelper(Database db) {
        this.db = db
    }

    Settlement findCurrent() {
        db.find(Settlement).where().eq(Settlement.Fields.yearMonth, YearMonth.now()).findOne()
    }

    Settlement saveAndGetOldSettlement(LocalDateTime time, List<User> users) {
        def settlement = new Settlement(YearMonth.from(time), users)
        db.save(settlement)
        return settlement
    }

    def delete(Settlement settlement) {
        db.deleteAll(settlement.payments)
        db.deleteAll(settlement.expenses)
        settlement.users.clear()
        db.save(settlement)
        db.delete(settlement)
    }
}
