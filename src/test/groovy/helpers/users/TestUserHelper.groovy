package helpers.users

import at.favre.lib.crypto.bcrypt.BCrypt
import io.ebean.Database
import lodgings.config.LodgingsConfiguration
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User

import javax.inject.Singleton

@Singleton
class TestUserHelper {

    private final int passwordHashingStrength
    private final Database db

    TestUserHelper(LodgingsConfiguration configuration, Database db) {
        this.passwordHashingStrength = configuration.passwordHashingStrength
        this.db = db
    }

    User createAndSaveUniqueUser() {
        def passwordHash = BCrypt.withDefaults().hashToString(
                passwordHashingStrength,
                "unique".toCharArray()
        )
        def roles = db.find(Role.class).where().in(Role.Fields.name, List.of(Role.USER)).findList()
        def user = new User("unique", "Ms. Unique", passwordHash, roles, true, true)
        user.passwordResetRequired = false
        db.save(user)
        user
    }

    User createAndSaveExistingUser() {
        def passwordHash = BCrypt.withDefaults().hashToString(
                passwordHashingStrength,
                "existing".toCharArray()
        )
        def roles = db.find(Role.class).where().in(Role.Fields.name, List.of(Role.USER)).findList()
        def user = new User("existing", "Mr. Existing", passwordHash, roles, true, true)
        user.passwordResetRequired = false
        db.save(user)
        user
    }

    User createAndSaveUserRequiredToChangePassword() {
        def passwordHash = BCrypt.withDefaults().hashToString(
                passwordHashingStrength,
                "mr_required".toCharArray()
        )
        def roles = db.find(Role.class).where().in(Role.Fields.name, List.of(Role.USER)).findList()
        def user = new User("mr_required", "Mr. Required", passwordHash, roles, true, true)
        user.passwordResetRequired = true
        db.save(user)
        user
    }

    User createAndSaveInactiveUser() {
        def passwordHash = BCrypt.withDefaults().hashToString(
                passwordHashingStrength,
                "mr_inactive".toCharArray()
        )
        def roles = db.find(Role.class).where().in(Role.Fields.name, List.of(Role.USER)).findList()
        def user = new User("mr_inactive", "Mr. Inactive", passwordHash, roles, true, true)
        user.passwordResetRequired = false
        user.allowedToLogin = false
        db.save(user)
        user
    }

    void delete(User user) {
        user.roles.clear()
        db.save(user)
        db.sqlUpdate("DELETE FROM settlements_to_users WHERE user_id = :id")
                .setParameter("id", user.id)
                .execute() // roles - ok, I need them anyway, but I don't want to add a relation mapping
                           // to an entity only for tests to work
        db.delete(user)
    }

    User findAdmin() {
        db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
    }
}
