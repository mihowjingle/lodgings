package lodgings.common.data


import spock.lang.Specification

import static lodgings.common.SyntacticSugar.not
import static lodgings.common.data.Comparison.Operator.EQUAL_TO
import static lodgings.common.data.Comparison.Operator.GREATER_THAN
import static lodgings.common.data.Comparison.Operator.GREATER_THAN_OR_EQUAL_TO
import static lodgings.common.data.Comparison.Operator.LESS_THAN
import static lodgings.common.data.Comparison.Operator.LESS_THAN_OR_EQUAL_TO
import static lodgings.common.data.Comparison.Operator.NOT_EQUAL_TO

class ComparisonSpec extends Specification {

    def "it should compare things correctly"() {

        expect:
        Comparison.is(left, EQUAL_TO, right) == equal
        Comparison.is(left, NOT_EQUAL_TO, right) == not(equal)
        Comparison.is(left, GREATER_THAN, right) == leftGreater
        Comparison.is(left, GREATER_THAN_OR_EQUAL_TO, right) == leftGE
        Comparison.is(left, LESS_THAN, right) == leftLess
        Comparison.is(left, LESS_THAN_OR_EQUAL_TO, right) == leftLE

        where:
        left    | right     || equal    | leftGreater   | leftGE    | leftLess  | leftLE
        1       | 0         || false    | true          | true      | false     | false
        1       | 1         || true     | false         | true      | false     | true
        1       | 2         || false    | false         | false     | true      | true
        2       | 3         || false    | false         | false     | true      | true
        3       | 3         || true     | false         | true      | false     | true
        4       | 3         || false    | true          | true      | false     | false
    }
}
