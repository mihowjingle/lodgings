package lodgings.common.data.pagination

import spock.lang.Specification

import static helpers.common.StaticHelpers.ascending
import static helpers.common.StaticHelpers.descending

class PaginationRequestSpec extends Specification {

    def "it should return the correct first row and page size"() {

        when: "there is a request to load some data"
        def pagination = new PaginationRequest(index, size, null, null)

        then: "the pagination is translated, so that the repository returns only and all the requested data"
        pagination.firstRow == expectedFirstRow
        pagination.size == size ?: 10

        where:
        index   | size  || expectedFirstRow
        0       | 20    || 0
        1       | 10    || 10
        7       | 23    || 161
        3       | 15    || 45
        1       | 1     || 1
        null    | 1     || 0
        1       | null  || 10
    }

    def "it should return the correct order"() {

        when: "there is a request to load some data"
        def pagination = new PaginationRequest(null, null, property, order)

        then: "the pagination is translated, so that the repository returns only and all the requested data"
        pagination.orderBy == expectedOrderBy

        where:
        property    | order         || expectedOrderBy
        "anything"  | "ASCENDING"   || ascending("anything")
        "aaa"       | "ASCENDING"   || ascending("aaa")
        "anything"  | "DESCENDING"  || descending("anything")
        "aaa"       | "DESCENDING"  || descending("aaa")
        null        | "ASCENDING"   || null
        "anything"  | null          || null
    }
}
