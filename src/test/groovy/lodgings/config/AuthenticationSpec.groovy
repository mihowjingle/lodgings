package lodgings.config

import helpers.common.UnirestWrapper
import helpers.security.TokenManipulator
import helpers.users.TestUserHelper
import io.ebean.Database
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.security.token.jwt.render.AccessRefreshToken
import io.micronaut.test.annotation.MicronautTest
import kong.unirest.HttpResponse
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import lodgings.domain.users.data.requests.UserCreationRequest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class AuthenticationSpec extends Specification {

    @Inject
    Database db

    @Inject
    UnirestWrapper unirest

    @Inject
    TokenManipulator tokenManipulator

    @Inject
    TestUserHelper userHelper

    def "on a new app instance, admin should be able to log in with correct password"() {

        given: "a new login request by admin"
        def authenticationRequest = new UsernamePasswordCredentials("admin", "admin")

        when: "the request is sent by POST"
        HttpResponse response = unirest.post("/api/login")
                .body(authenticationRequest)
                .asObject(AccessRefreshToken.class)

        then: "it's all good"
        response.status == 200

        and: "response contains the token"
        response.body.accessToken != null
        response.body.tokenType == "Bearer"

        and: "the token expires in an hour"
        response.body.expiresIn == 3600

        and: "the token is indeed for 'admin' (subject)"
        Jws<Claims> claims = tokenManipulator.parse(response.body.accessToken)
        claims.body.getSubject() == "admin"

        and: "admin can only reset password (because it's a fresh app instance)"
        claims.body.get("roles", List.class) == [Role.RESET_PASSWORD]
    }

    def "on a new app instance, admin should be required to reset their password on first login"() {

        expect:
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOneOrEmpty()
        admin.isPresent()
        admin.get().passwordResetRequired
    }

    def "it should log user in, with correct roles, when the user exists and password is correct"() {

        given: "user with login and password 'existing' exists"
        def existing = userHelper.createAndSaveExistingUser()

        and: "user with login and password 'mr_required' exists"
        def required = userHelper.createAndSaveUserRequiredToChangePassword()

        and: "that there is a new login request"
        def authenticationRequest = new UsernamePasswordCredentials(username, password)

        when: "the request is sent by POST"
        HttpResponse response = unirest.post("/api/login")
                .body(authenticationRequest)
                .asObject(AccessRefreshToken.class)

        then: "it's all good"
        response.status == 200

        and: "response contains the token"
        response.body.accessToken != null
        response.body.tokenType == "Bearer"

        and: "the token expires in an hour"
        response.body.expiresIn == 3600

        and: "the token is indeed for the user (subject)"
        Jws<Claims> claims = tokenManipulator.parse(response.body.accessToken)
        claims.body.getSubject() == username

        and: "user has expected roles"
        claims.body.get("roles", List.class) == expectedRoles

        cleanup:
        userHelper.delete(existing)
        userHelper.delete(required)

        where:
        username        | password      || expectedRoles
        "admin"         | "admin"       || [Role.RESET_PASSWORD]
        "existing"      | "existing"    || [Role.USER]
        "mr_required"   | "mr_required" || [Role.RESET_PASSWORD]
    }

    def "it should not log user in when the user doesn't exist or password is incorrect"() {

        given: "that there is a new login request"
        def authenticationRequest = new UsernamePasswordCredentials(username, password)

        when: "the request is sent by POST"
        HttpResponse response = unirest.post("/api/login")
                .body(authenticationRequest)
                .asObject(AccessRefreshToken.class)

        then: "it's not all good - the attempt is unauthorized"
        response.status == 401 || password == null && response.status == 400 // micronaut treats null password differently ¯\_(ツ)_/¯

        and: "there is no token"
        response.body.accessToken == null

        where:
        username    | password
        "admin"     | "not_admins_password"
        "admin"     | "nope"
        "existing"  | "nah"
        "existing"  | "maybe"
        "existing"  | null
        "aaaaa"     | "aaaaa"
        "bbbbb"     | "bbbbb"
    }

    def "it should not log an inactive user in, even with correct roles and password"() {

        given: "that there is a new login request"
        def authenticationRequest = new UsernamePasswordCredentials(username, password)

        when: "the request is sent by POST"
        HttpResponse response = unirest.post("/api/login")
                .body(authenticationRequest)
                .asObject(AccessRefreshToken.class)

        then: "it's not all good - the attempt is unauthorized"
        response.status == 401

        and: "there is no token"
        response.body.accessToken == null

        where:
        username            | password
        "mr_inactive"       | "existing"
        "ms_nonexistent"    | "existing"
    }

    def "it should detect and reject tokens meddled with"() {

        given: "that we are an attacker with an intercepted token with added ADMIN role"
        def token = tokenManipulator.fakeTamperedToken()

        and: "some users may or may not be in the database"
        def countBefore = db.find(User.class).findCount()

        and: "we want to save a new user"
        UserCreationRequest payload = new UserCreationRequest("whatever", "Mrs. Whoever", [Role.USER], false, false, false)

        when: "we send the new user by POST"
        HttpResponse response = unirest.post("/api/users")
                .header("Authorization", token)
                .body(payload)
                .asEmpty()

        then: "it's not all good - unauthorized"
        response.status == 401

        and: "there are no new users in the database"
        db.find(User.class).findCount() == countBefore
    }
}
