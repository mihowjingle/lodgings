package lodgings.domain.expenses.data.entities

import lodgings.domain.settlement.data.entities.Settlement
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.YearMonth

class ExpenseSpec extends Specification {

    def "it should correctly determine if a expense is from before current month"() {

        given: "an expense"
        def expense = new Expense(
                BigDecimal.ONE,
                whenBought,
                null,
                null,
                new Settlement(YearMonth.from(whenBought), [])
        )

        expect: "it is (not) from before current month"
        isOld == expense.isFromBeforeCurrentMonth()

        where:
        whenBought                                  | isOld
        LocalDateTime.now().minusMonths(1)          | true
        LocalDateTime.now().minusMonths(2)          | true
        LocalDateTime.now()                         | false
        YearMonth.now().atDay(1).atStartOfDay()     | false
    }
}
