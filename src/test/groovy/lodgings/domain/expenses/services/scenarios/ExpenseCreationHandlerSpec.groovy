package lodgings.domain.expenses.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.items.data.entities.Item
import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.expenses.data.requests.ExpensePersistRequest
import spock.lang.Specification

import javax.inject.Inject

import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.*

@MicronautTest
class ExpenseCreationHandlerSpec extends Specification {

    @Inject
    Database db

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    TestUserHelper userHelper

    @Inject
    ExpenseCreationHandler handler

    def "it should create a expense after a valid request"() {

        given: "there is a request to create a expense"
        def request = new ExpensePersistRequest(totalPrice, itemName)

        and: "we are logged in"
        def admin = userHelper.findAdmin()
        currentUserService.get() >> admin

        and: "an item already exists"
        def item = new Item("tp", false, null, admin)
        db.save(item)

        when:
        def response = handler.handle(request)

        then: "it's ok"
        response.isSuccess()

        and: "there is exactly one expense (none before)"
        def expense = db.find(Expense).findOne()
        expense != null

        and: "the response contains it"
        response.body.expenses[0].buyer.id == admin.id

        and: "it has the price and item name that we want"
        expense.totalPrice == totalPrice
        expense.item.name == itemName

        cleanup:
        db.delete(expense)
        db.delete(item)

        where:
        totalPrice                  | itemName
        new BigDecimal("0.01")      | "tp"
        new BigDecimal("1007.17")   | "tp"
        new BigDecimal("9999.99")   | "tp"
    }

    def "it should reject an invalid expense creation request"() {

        given: "there is a request to create a expense"
        def request = new ExpensePersistRequest(totalPrice, itemName)

        and: "we are logged in"
        def admin = userHelper.findAdmin()
        currentUserService.get() >> admin

        and: "an item already exists"
        def item = new Item("tp", false, null, admin)
        db.save(item)

        when:
        def response = handler.handle(request)

        then: "it's not ok"
        response.isError()

        and: "specifically..."
        response.errors.containsAll(expectedErrors)
        expectedErrors.containsAll(response.errors)

        and: "there is (still) no expense in the db"
        db.find(Expense).findCount() == 0

        cleanup:
        db.delete(item)

        where:
        totalPrice                  | itemName  || expectedErrors
        new BigDecimal("0.001")     | "tp"      || [TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS]
        new BigDecimal("9999.99")   | "tp2"     || [ITEM_NOT_FOUND]
        null                        | null      || [TOTAL_PRICE_MISSING, ITEM_NAME_MISSING]
        new BigDecimal("33")        | null      || [ITEM_NAME_MISSING]
    }
}