package lodgings.domain.expenses.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Item
import lodgings.domain.expenses.data.entities.Expense
import spock.lang.Specification

import javax.inject.Inject
import java.time.LocalDateTime

import static lodgings.domain.expenses.data.errors.ExpenseDeletionErrors.ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS

@MicronautTest
class ExpenseDeletionHandlerSpec extends Specification {
    
    @Inject
    Database db
    
    @Inject
    ExpenseDeletionHandler handler

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper
    
    def "it should correctly delete a expense"() {

        given: "there are some expenses (and items) in the database"
        def admin = userHelper.findAdmin()
        def item = new Item("item", true, null, admin)
        db.save(item)
        def expense = new Expense(
                BigDecimal.ONE,
                LocalDateTime.now(),
                item,
                admin,
                settlementHelper.findCurrent()
        )
        db.save(expense)
        def expensesBefore = db.find(Expense).findCount()
        def itemsBefore = db.find(Item).findCount()

        when: "we request the expense to be deleted"
        def response = handler.handle(expense.id)

        then: "it's all good"
        response.isSuccess()

        and: "we have one fewer expenses in the database"
        db.find(Expense).findCount() == expensesBefore - 1

        and: "the number of items doesn't change (why would it)"
        db.find(Item).findCount() == itemsBefore

        cleanup:
        db.delete(expense)
        db.delete(item)
    }

    def "it should reject deletion of a expense from before the current settlement"() {

        given: "there are some expenses (from before current month), (and items) in the database"
        def admin = userHelper.findAdmin()
        def item = new Item("item", true, null, admin)
        db.save(item)
        def whenBought = LocalDateTime.now().minusMonths(2)
        def settlement = settlementHelper.saveAndGetOldSettlement(whenBought, [admin])
        def expense = new Expense(
                BigDecimal.ONE,
                whenBought,
                item,
                admin,
                settlement
        )
        db.save(expense)
        def expensesBefore = db.find(Expense).findCount()

        when: "we request the expense to be deleted"
        def response = handler.handle(expense.id)

        then: "it's an error"
        response.isError()

        and: "specifically..."
        response.errors.size() == 1
        response.errors[0] == ATTEMPT_TO_DELETE_EXPENSE_FROM_PREVIOUS_MONTHS

        and: "the number of expenses doesn't change"
        db.find(Expense).findCount() == expensesBefore

        cleanup:
        settlementHelper.delete(settlement)
        db.delete(item)
    }
}
