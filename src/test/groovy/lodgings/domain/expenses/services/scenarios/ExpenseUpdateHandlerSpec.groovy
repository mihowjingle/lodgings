package lodgings.domain.expenses.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Item
import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.expenses.data.errors.ExpenseUpdateErrors
import lodgings.domain.expenses.data.requests.ExpensePersistRequest
import spock.lang.Specification

import javax.inject.Inject
import java.time.LocalDateTime

import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.*
import static lodgings.domain.expenses.data.errors.ExpenseUpdateErrors.ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS

@MicronautTest
class ExpenseUpdateHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper

    @Inject
    ExpenseUpdateHandler handler

    def "it should update a expense after a valid request"() {

        given: "an item already exists"
        def admin = userHelper.findAdmin()
        def item = new Item("tp", false, null, admin)
        db.save(item)

        and: "a expense already exists"
        def expense = new Expense(
                BigDecimal.TEN,
                LocalDateTime.now(),
                item,
                admin,
                settlementHelper.findCurrent()
        )
        db.save(expense)

        and: "some number of expenses exists in general"
        def countBefore = db.find(Expense).findCount()

        and: "there is a request to update the expense"
        def request = new ExpensePersistRequest(totalPrice, itemName)

        when:
        def response = handler.handle(expense.id, request)

        then: "it's ok"
        response.isSuccess()

        and: "the expense has the price and item name that we want"
        db.refresh(expense)
        expense.totalPrice == totalPrice
        expense.item.name == itemName

        and: "the number of expenses doesn't change"
        countBefore == db.find(Expense).findCount()

        cleanup:
        db.delete(expense)
        db.delete(item)

        where:
        totalPrice                  | itemName
        new BigDecimal("0.01")      | "tp"
        new BigDecimal("1007.17")   | "tp"
        new BigDecimal("9999.99")   | "tp"
    }

    def "it should reject an invalid expense update request"() {

        given: "an item already exists"
        def admin = userHelper.findAdmin()
        def item = new Item("tp", false, null, admin)
        db.save(item)

        and: "a expense already exists"
        def expense = new Expense(
                BigDecimal.TEN,
                LocalDateTime.now(),
                item,
                admin,
                settlementHelper.findCurrent()
        )
        db.save(expense)

        and: "there is a request to update the expense"
        def request = new ExpensePersistRequest(totalPrice, itemName)

        when:
        def response = handler.handle(expense.id, request)

        then: "it's not ok"
        response.isError()

        and: "specifically..."
        response.errors.containsAll(expectedErrors)
        expectedErrors.containsAll(response.errors)

        and: "the expense remains unchanged"
        db.refresh(expense)
        expense.totalPrice == BigDecimal.TEN
        expense.item.name == item.getName()

        when: "there is a request to update a non-existent expense"
        request = new ExpensePersistRequest(BigDecimal.TEN, item.getName())
        response = handler.handle(23498232399L, request)

        then: "it's not ok"
        response.isError()
        response.errors == [ExpenseUpdateErrors.EXPENSE_NOT_FOUND]

        cleanup:
        db.delete(expense)
        db.delete(item)

        where:
        totalPrice                  | itemName  || expectedErrors
        new BigDecimal("0.001")     | "tp"      || [TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS]
        new BigDecimal("9999.99")   | "tp2"     || [ITEM_NOT_FOUND]
        null                        | null      || [TOTAL_PRICE_MISSING, ITEM_NAME_MISSING]
        new BigDecimal("33")        | null      || [ITEM_NAME_MISSING]
    }

    def "it should reject a request to update a expense in the past (before current settlement)"() {

        given: "an item already exists"
        def admin = userHelper.findAdmin()
        def item = new Item("tp", false, null, admin)
        db.save(item)

        and: "a expense associated with a past settlement already exists"
        def whenBought = LocalDateTime.now().minusMonths(2)
        def settlement = settlementHelper.saveAndGetOldSettlement(whenBought, [admin])
        def expense = new Expense(
                BigDecimal.TEN,
                whenBought,
                item,
                admin,
                settlement
        )
        db.save(expense)

        and: "there is a request to update the expense"
        def request = new ExpensePersistRequest(BigDecimal.TEN, item.getName())

        when:
        def response = handler.handle(expense.id, request)

        then: "it's not ok"
        response.isError()

        and: "specifically..."
        response.errors == [ATTEMPT_TO_UPDATE_EXPENSE_FROM_PREVIOUS_MONTHS]

        and: "the expense remains unchanged"
        db.refresh(expense)
        expense.totalPrice == BigDecimal.TEN
        expense.item.name == item.getName()

        cleanup:
        settlementHelper.delete(settlement)
        db.delete(item)
    }
}
