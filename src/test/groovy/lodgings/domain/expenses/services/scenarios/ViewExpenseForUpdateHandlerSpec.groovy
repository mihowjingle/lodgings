package lodgings.domain.expenses.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Item
import lodgings.domain.expenses.data.entities.Expense
import spock.lang.Specification

import javax.inject.Inject
import java.time.LocalDateTime

import static java.util.stream.Collectors.toList

@MicronautTest
class ViewExpenseForUpdateHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper

    @Inject
    ViewExpenseForUpdateHandler handler

    def "it should return expected expense for update and all items to choose from"() {

        given: "that there exists a user with USER role"
        def admin = userHelper.findAdmin()

        and: "there are some items in the database"
        def item1 = new Item(
                "item1",
                true,
                null,
                admin
        )
        def item2 = new Item(
                "item2",
                true,
                null,
                admin
        )
        def items = [item1, item2]
        db.saveAll(items)

        and: "there is a expense in the db"
        def expense = new Expense(
                new BigDecimal("1.50"),
                LocalDateTime.now().minusMinutes(2),
                item1,
                admin,
                settlementHelper.findCurrent()
        )
        db.save(expense)

        when: "we ask for the expense with the intention of updating it"
        def response = handler.handle(expense.id)

        then: "it's all good"
        noExceptionThrown()
        response.isPresent()

        and: "the returned expense is the one we expected"
        response.get().id == expense.id
        response.get().totalPrice == expense.totalPrice

        and: "all items get returned (and nothing else, for whatever reason)"
        def itemNames = items.stream().map { it.name }.collect(toList())
        response.get().availableItems.containsAll(itemNames)
        itemNames.containsAll(response.get().availableItems)

        when: "we ask for some non-existent expense with the intention of updating it"
        response = handler.handle(999898989876775L)

        then: "it's not found"
        response.isEmpty()

        cleanup:
        db.delete(expense)
        db.deleteAll(items)
    }
}
