package lodgings.domain.expenses.services.validators.atomic


import lodgings.domain.expenses.data.requests.ExpensePersistRequest
import spock.lang.Specification

import static lodgings.domain.expenses.data.errors.ExpensePersistErrors.*

class ExpenseTotalPriceValidatorSpec extends Specification {

    def validator = new ExpenseTotalPriceValidator()

    def "it should validate total price correctly"() {

        given:
        def request = new ExpensePersistRequest(totalPrice, "whatever")

        expect:
        validator.validate(request) == expectedError

        where:
        totalPrice                  || expectedError
        new BigDecimal("0.01")      || Optional.empty()
        new BigDecimal("7.73")      || Optional.empty()
        new BigDecimal("9999.99")   || Optional.empty()
        new BigDecimal("166.00")    || Optional.empty()
        new BigDecimal("10000.00")  || Optional.of(TOTAL_PRICE_TOO_HIGH)
        new BigDecimal("10000.0")   || Optional.of(TOTAL_PRICE_TOO_HIGH)
        new BigDecimal("999.999")   || Optional.of(TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS)
        new BigDecimal("999.9999")  || Optional.of(TOTAL_PRICE_TOO_MANY_DECIMAL_DIGITS)
        null                        || Optional.of(TOTAL_PRICE_MISSING)
        new BigDecimal("-0.01")     || Optional.of(TOTAL_PRICE_TOO_LOW)
        new BigDecimal("0.00")      || Optional.of(TOTAL_PRICE_TOO_LOW)
    }
}
