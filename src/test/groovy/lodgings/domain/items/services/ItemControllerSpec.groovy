package lodgings.domain.items.services

import helpers.common.UnirestWrapper
import helpers.security.TokenManipulator
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import kong.unirest.HttpResponse
import lodgings.common.data.Failable
import lodgings.common.data.pagination.PaginationRequest
import lodgings.domain.items.data.projections.ItemForUpdateProjection
import lodgings.domain.items.data.requests.ItemPersistRequest
import lodgings.domain.items.services.scenarios.*
import lodgings.domain.users.data.entities.Role
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class ItemControllerSpec extends Specification {

    @Inject
    UnirestWrapper unirest

    @Inject
    TokenManipulator tokenManipulator

    @MockBean(ItemCreationHandler)
    ItemCreationHandler itemCreationHandler() {
        Mock(ItemCreationHandler)
    }

    @Inject
    ItemCreationHandler itemCreationHandler

    @MockBean(ItemDeletionHandler)
    ItemDeletionHandler itemDeletionHandler() {
        Mock(ItemDeletionHandler)
    }

    @Inject
    ItemDeletionHandler itemDeletionHandler

    @MockBean(ViewItemForUpdateHandler)
    ViewItemForUpdateHandler viewItemForUpdateHandler() {
        Mock(ViewItemForUpdateHandler)
    }

    @Inject
    ViewItemForUpdateHandler viewItemForUpdateHandler

    @MockBean(ViewItemsHandler)
    ViewItemsHandler viewItemsHandler() {
        Mock(ViewItemsHandler)
    }

    @Inject
    ViewItemsHandler viewItemsHandler

    @MockBean(ItemUpdateHandler)
    ItemUpdateHandler itemUpdateHandler() {
        Mock(ItemUpdateHandler)
    }

    @Inject
    ItemUpdateHandler itemUpdateHandler

    def "only a USER should be able to save an item"() {

        given: "that we are logged in as a user with some roles"
        def token = tokenManipulator.fakeToken("unique", roles)

        and: "we want to save a new item"
        def request = new ItemPersistRequest(
                "whatever",
                true,
                null
        )

        when: "we send the new item by POST"
        HttpResponse response = unirest.post("/api/items")
                .header("Authorization", token)
                .body(request)
                .asEmpty()

        then: "it's (not) all good"
        response.status == status

        and: "execution proceeds accordingly"
        invocations * itemCreationHandler.handle(request) >> Failable.success()

        where:
        roles                               || invocations  | status
        ["NOPE"]                            || 0            | 403
        null                                || 0            | 403
        []                                  || 0            | 403
        [Role.ADMIN, Role.RESET_PASSWORD]   || 0            | 403
        [Role.USER]                         || 1            | 200
    }

    def "only a USER should be allowed to delete an item"() {

        given: "that we are logged in as a user with some roles"
        def token = tokenManipulator.fakeToken("whoever", roles)

        when: "we send some item id by DELETE"
        HttpResponse response = unirest.delete("/api/items/1")
                .header("Authorization", token)
                .asEmpty()

        then: "it's (not) all good"
        response.status == expectedStatus

        and: "execution proceeds accordingly"
        invocations * itemDeletionHandler.handle(1L) >> Failable.success()

        where:
        roles                   || expectedStatus   | invocations
        [Role.RESET_PASSWORD]   || 403              | 0
        [Role.ADMIN]            || 403              | 0
        ["BLAH", "Nope!"]       || 403              | 0
        []                      || 403              | 0
        null                    || 403              | 0
        [Role.USER]             || 200              | 1
    }

    def "only a USER should be able to view an item"() {

        given: "that we are logged in as a user with some roles"
        def token = tokenManipulator.fakeToken("whoever", roles)

        when: "we ask for an item by GET"
        HttpResponse response = unirest.get("/api/items/1/for-update")
                .header("Authorization", token)
                .asEmpty()

        then: "it's (not) all good"
        response.status == status

        and: "execution proceeds accordingly"
        invocations * viewItemForUpdateHandler.handle(1L) >> Optional.of(
                new ItemForUpdateProjection(null, List.of())
        )

        where:
        roles                               || invocations  | status
        ["NOPE"]                            || 0            | 403
        null                                || 0            | 403
        []                                  || 0            | 403
        [Role.ADMIN, Role.RESET_PASSWORD]   || 0            | 403
        [Role.USER]                         || 1            | 200
    }

    def "only a USER should be able to view a page of items"() {

        given: "that we are logged in as a user with some roles"
        def token = tokenManipulator.fakeToken("whoever", roles)

        when: "we ask for a page of items by GET"
        HttpResponse response = unirest.get("/api/items?index=0")
                .header("Authorization", token)
                .asEmpty()

        then: "it's (not) all good"
        response.status == status

        and: "execution proceeds accordingly"
        invocations * viewItemsHandler.handle(_ as PaginationRequest) >> Failable.success(null)

        where:
        roles                               || invocations  | status
        ["NOPE"]                            || 0            | 403
        null                                || 0            | 403
        []                                  || 0            | 403
        [Role.ADMIN, Role.RESET_PASSWORD]   || 0            | 403
        [Role.USER]                         || 1            | 200
    }

    def "only a USER should be able to update an item"() {

        given: "that we are logged in as a user with some roles"
        def token = tokenManipulator.fakeToken("whoever", roles)

        and: "we want to update an item"
        def request = new ItemPersistRequest(
                "whatever",
                true,
                null
        )

        when: "we send the item by PUT"
        HttpResponse response = unirest.put("/api/items/1")
                .header("Authorization", token)
                .body(request)
                .asEmpty()

        then: "it's (not) all good"
        response.status == status

        and: "execution proceeds accordingly"
        invocations * itemUpdateHandler.handle(1L, request) >> Failable.success()

        where:
        roles                               || invocations  | status
        ["NOPE"]                            || 0            | 403
        null                                || 0            | 403
        []                                  || 0            | 403
        [Role.ADMIN, Role.RESET_PASSWORD]   || 0            | 403
        [Role.USER]                         || 1            | 200
    }
}
