package lodgings.domain.items.services.scenarios

import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Purpose
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject

import static java.util.stream.Collectors.toList

@MicronautTest
class GetPurposeNamesHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    GetPurposeNamesHandler handler

    def "it should return all purpose names"() {

        given: "that there exists a user with USER role"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()

        and: "there are some purposes in the database"
        def purposeNames = ["one purpose", "another purpose", "food stuff", "cleaning stuff"]
        def purposes = purposeNames.stream().map { new Purpose(it, admin) }.collect(toList())
        db.saveAll(purposes)

        when: "we ask for all purpose names"
        def response = handler.handle()

        then: "it's all good"
        noExceptionThrown()

        and: "result contains purpose names as expected"
        response.containsAll(purposeNames)
        purposeNames.containsAll(response)

        cleanup:
        db.deleteAll(purposes)
    }
}
