package lodgings.domain.items.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.items.data.entities.Item
import lodgings.domain.items.data.entities.Purpose
import lodgings.domain.items.data.requests.ItemPersistRequest
import spock.lang.Specification

import javax.inject.Inject

import static helpers.common.StaticHelpers.stringOfLength
import static lodgings.domain.items.data.errors.ItemPersistErrors.*

@MicronautTest
class ItemCreationHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    ItemCreationHandler handler

    def "it should save a valid item"() {

        given: "that there exists a user with login 'unique' and USER role"
        def user = userHelper.createAndSaveUniqueUser()

        and: "we are logged in as that user"
        currentUserService.get() >> user

        and: "purpose 'laundry' already exists"
        def existingPurpose = new Purpose("laundry", user)
        db.save(existingPurpose)

        and: "some items may, or may not be in the database already"
        def itemsBefore = db.find(Item.class).findCount()

        and: "some purposes may, or may not be in the database already"
        def purposesBefore = db.find(Purpose.class).findCount()

        and: "we want to save a new item"
        ItemPersistRequest request = new ItemPersistRequest(
                name,
                true,
                purposeName
        )

        when: "we send the new item by POST"
        def response = handler.handle(request)

        then: "it's all good"
        response.isSuccess()

        and: "we have one more item in the database"
        db.find(Item.class).findCount() == itemsBefore + 1

        and: "the number of purposes changes, or doesn't, accordingly"
        db.find(Purpose.class).findCount() == purposesBefore + howManyMorePurposesExpected

        and: "that is indeed the item we just saved"
        def justSavedItem = db.find(Item.class).where().eq(Item.Fields.name, name).findOne()
        justSavedItem != null

        and: "the item is 'lastModifiedBy' the correct user"
        justSavedItem.lastModifiedBy.login == "unique"

        cleanup:
        db.delete(justSavedItem)
        db.delete(existingPurpose)
        db.find(Purpose.class).where().eq(Purpose.Fields.name, purposeName).delete()
        userHelper.delete(user)

        where:
        name                | purposeName   || howManyMorePurposesExpected
        "washing liquid"    | "laundry"     || 0
        "washing liquid"    | "not laundry" || 1
        "washing liquid"    | null          || 0
        stringOfLength(4)   | null          || 0
        stringOfLength(50)  | null          || 0
    }

    def "it should not save an invalid item"() {

        given: "that there exists a user with login 'unique' and USER role"
        def user = userHelper.createAndSaveUniqueUser()

        and: "we are logged in as that user"
        currentUserService.get() >> user

        and: "purpose 'laundry' already exists"
        def laundry = new Purpose("laundry", user)
        db.save(laundry)

        and: "item 'washing powder' already exists"
        def washingPowder = new Item("washing powder", false, laundry, user)
        db.save(washingPowder)

        and: "some items may, or may not be in the database already"
        def countBefore = db.find(Item.class).findCount()

        and: "we want to save a new item"
        ItemPersistRequest request = new ItemPersistRequest(
                name,
                true,
                purposeName
        )

        when: "we send the new item by POST"
        def response = handler.handle(request)

        then: "it's not all good"
        response.isError()

        and: "there are all the expected errors and only the expected errors"
        response.errors.containsAll(expectedErrors)
        expectedErrors.containsAll(response.errors)

        and: "we have no new items in the database"
        db.find(Item.class).findCount() == countBefore

        cleanup:
        db.delete(washingPowder)
        db.delete(laundry)
        userHelper.delete(user)

        where:
        name                | purposeName           || expectedErrors
        "washing powder"    | "laundry"             || [NAME_NOT_UNIQUE]
        null                | null                  || [NAME_MISSING]
        stringOfLength(3)   | stringOfLength(3)     || [NAME_TOO_SHORT, PURPOSE_NAME_TOO_SHORT]
        stringOfLength(51)  | stringOfLength(51)    || [NAME_TOO_LONG, PURPOSE_NAME_TOO_LONG]
    }
}
