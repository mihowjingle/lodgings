package lodgings.domain.items.services.scenarios

import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.items.data.entities.Item
import lodgings.domain.items.data.entities.Purpose
import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.YearMonth

import static java.util.stream.Collectors.toList

@MicronautTest
class ItemDeletionHandlerSpec extends Specification {

    @Inject
    Database db

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    ItemDeletionHandler handler

    def "it should delete a purposeless item"() {

        given: "there are some items in the database"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
        def item = new Item("dish soap", false, null, admin)
        db.save(item)
        def itemsBefore = db.find(Item.class).findCount()

        and: "some purposes may, or may not be in the database already"
        def purposesBefore = db.find(Purpose.class).findCount()

        and: "we are logged in as Role.ADMIN"
        currentUserService.get() >> admin

        when: "we request the item to be deleted"
        handler.handle(item.id)

        then: "it's all good"
        noExceptionThrown()

        and: "we have one fewer items in the database"
        db.find(Item.class).findCount() == itemsBefore - 1

        and: "the number of purposes doesn't change"
        db.find(Purpose.class).findCount() == purposesBefore
    }

    def "it should delete an item assigned to a purpose"() {

        given: "there are some items in the database"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
        def existingPurpose = new Purpose("purpose", admin)
        db.save(existingPurpose)
        def item1 = new Item("item 1", false, existingPurpose, admin)
        db.save(item1)
        def item2 = new Item("item 2", true, existingPurpose, admin)
        db.save(item2)
        def itemsBefore = db.find(Item.class).findCount()

        and: "some purposes may, or may not be in the database already"
        def purposesBefore = db.find(Purpose.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> admin

        when: "we send the item's id by DELETE"
        handler.handle(item1.id)

        then: "it's all good"
        noExceptionThrown()

        and: "we have one fewer items in the database"
        def itemsAfterFirstDelete = db.find(Item.class).findCount()
        itemsAfterFirstDelete == itemsBefore - 1

        and: "the number of purposes doesn't change"
        db.find(Purpose.class).findCount() == purposesBefore

        when: "we delete the other item"
        handler.handle(item2.id)

        then: "it's all good"
        noExceptionThrown()

        and: "we have one fewer items in the database, again"
        db.find(Item.class).findCount() == itemsAfterFirstDelete - 1

        and: "this time, the purpose also gets deleted, because that was the last item"
        db.find(Purpose.class).findCount() == purposesBefore - 1
    }

    def "it should do nothing if there is a request to delete a non-existing item"() {

        given: "some items may, or may not be in the database already"
        def itemsBefore = db.find(Item.class).findCount()

        and: "some purposes may, or may not be in the database already"
        def purposesBefore = db.find(Purpose.class).findCount()

        and: "we are logged in as admin"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
        currentUserService.get() >> admin

        when: "we send some random item id by DELETE"
        handler.handle(9129090192192L)

        then: "it's all good"
        noExceptionThrown() // maybe i'll make it so that such a situation is reported as an error,
        // but for now, it's more "make sure there is no item x", than "delete item x" - we don't care if it was there
        // in the first place

        and: "the number of items doesn't change"
        db.find(Item.class).findCount() == itemsBefore

        and: "the number of purposes doesn't change"
        db.find(Purpose.class).findCount() == purposesBefore
    }

    def "it should reject a request to delete an item which 'has' some expenses"() {

        given: "there is an item in the database"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
        def item = new Item("dish soap", false, null, admin)
        db.save(item)
        def itemsBefore = db.find(Item.class).findCount()

        and: "there are expenses based on that item"
        def settlement = db.find(Settlement).where().eq(Settlement.Fields.yearMonth, YearMonth.now()).findOne()
        def expense = new Expense(BigDecimal.ONE, LocalDateTime.now(), item, admin, settlement)
        db.save(expense)

        and: "some purposes may, or may not be in the database already"
        def purposesBefore = db.find(Purpose.class).findCount()

        and: "we are logged in as Role.ADMIN"
        currentUserService.get() >> admin

        when: "we request the item to be deleted"
        def response = handler.handle(item.id)

        then: "the request is processed without an exception"
        noExceptionThrown()

        and: "there are errors"
        response.errors.stream().map({it.key }).collect(toList()) == [ "ITEM_RELATED_TO_EXPENSES" ]

        and: "no items are deleted from (or added to) the database"
        db.find(Item.class).findCount() == itemsBefore

        and: "the number of purposes doesn't change either"
        db.find(Purpose.class).findCount() == purposesBefore

        cleanup:
        db.delete(expense)
        db.delete(item)
    }
}
