package lodgings.domain.items.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.data.errors.Error
import lodgings.common.services.CurrentUserService
import lodgings.domain.items.data.entities.Item
import lodgings.domain.items.data.entities.Purpose
import lodgings.domain.items.data.errors.ItemPersistErrors
import lodgings.domain.items.data.errors.ItemUpdateErrors
import lodgings.domain.items.data.requests.ItemPersistRequest
import spock.lang.Specification

import javax.inject.Inject

import static helpers.common.StaticHelpers.stringOfLength

@MicronautTest
class ItemUpdateHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    private ItemUpdateHandler handler

    def "it should handle a request to update an item accordingly"() {

        given: "(there needs to be a user who saved the item to be updated in the first place)"
        def user = userHelper.createAndSaveUniqueUser()

        and: "this is the user that wants to update the item (doesn't have to be, just reusing)"
        currentUserService.get() >> user

        and: "the item is in the database"
        def updatedItem = new Item("item", false, null, user)
        db.save(updatedItem)

        and: "another item (with name 'unique') is in the database already, needed for name uniqueness test"
        def uniqueItem = new Item("unique", false, null, user)
        db.save(uniqueItem)

        and: "there is a request to update an item"
        def request = new ItemPersistRequest(name, true, purposeName)

        when:
        def result = handler.handle(updatedItem.id, request)

        then: "the result is what we expect, based on absence/presence of errors"
        result.isSuccess() == expectedErrors.isEmpty()

        and: "if result is not successful, there are exactly the errors we expect, in any order"
        result.isSuccess() || result.errors.containsAll(expectedErrors) && expectedErrors.containsAll(result.errors)

        cleanup:
        db.delete(updatedItem)
        db.delete(uniqueItem)
        db.find(Purpose).delete()
        userHelper.delete(user)

        where:
        name                | purposeName           || expectedErrors
        "item"              | null                  || []
        "aaaa"              | null                  || []
        "aaaa"              | "purpose"             || []
        "unique"            | "purpose"             || [ItemPersistErrors.NAME_NOT_UNIQUE]
        "aaa"               | "aaa"                 || [ItemPersistErrors.PURPOSE_NAME_TOO_SHORT, ItemPersistErrors.NAME_TOO_SHORT]
        null                | null                  || [ItemPersistErrors.NAME_MISSING]
        stringOfLength(51)  | stringOfLength(51)    || [ItemPersistErrors.PURPOSE_NAME_TOO_LONG, ItemPersistErrors.NAME_TOO_LONG]
    }

    def "it should not update a non-existent item"() {

        given: "there is a request to update an item with a 'wrong' id"
        def request = new ItemPersistRequest("name", true, null)

        when:
        def result = handler.handle(id, request)

        then: "the result is an error"
        result.isError()

        and: "there are exactly the errors we expect, in any order"
        result.errors.containsAll(expectedErrors) && expectedErrors.containsAll(result.errors)

        where:
        id      || expectedErrors
        null    || [Error.ID_MISSING]
        99999L  || [ItemUpdateErrors.ITEM_NOT_FOUND]
    }

    def "it should delete empty purposes after item update - or not, accordingly"() {

        given: "(there needs to be a user who saved the item to be updated in the first place)"
        def user = userHelper.createAndSaveUniqueUser()

        and: "this is the user that wants to update the item (doesn't have to be, just reusing)"
        currentUserService.get() >> user

        and: "there is an purpose"
        def purpose = new Purpose("category", user)
        db.save(purpose)

        and: "there are two items"
        def item1 = new Item("item 1", false, purpose, user)
        db.save(item1)

        def item2 = new Item("item 2", false, purpose, user)
        db.save(item2)

        and: "there is a request to update the first item (not to have a purpose)"
        def request = new ItemPersistRequest(item1.name, true, null)

        when:
        def result = handler.handle(item1.id, request)

        then: "it's ok"
        result.isSuccess()
        noExceptionThrown()

        and: "the purpose is not deleted (because there are still items in it)"
        db.find(Purpose).where().eq(Purpose.Fields.name, purpose.name).findOneOrEmpty().isPresent()

        when: "we update the other item to have a different purpose"
        request = new ItemPersistRequest(item2.name, true, "different category")

        result = handler.handle(item2.id, request)

        then: "it's ok"
        result.isSuccess()
        noExceptionThrown()

        and: "the initial purpose is deleted now"
        db.find(Purpose).where().eq(Purpose.Fields.name, purpose.name).findOneOrEmpty().isEmpty()

        cleanup:
        db.delete(item1)
        db.delete(item2)
        db.find(Purpose).delete()
        userHelper.delete(user)
    }
}