package lodgings.domain.items.services.scenarios


import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Item
import lodgings.domain.items.data.entities.Purpose
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject

import static java.util.stream.Collectors.toList

@MicronautTest
class ViewItemForUpdateHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    ViewItemForUpdateHandler handler

    def "it should return expected item and all purposes for update"() {

        given: "that there exists a user with USER role"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()

        and: "there is an item in the database"
        def item = new Item(
                "item",
                true,
                null,
                admin
        )
        db.save(item)

        and: "there are some purposes in the database"
        def purposeNames = ["one purpose", "another purpose", "food stuff", "cleaning stuff"]
        def purposes = purposeNames.stream().map { new Purpose(it, admin) }.collect(toList())
        db.saveAll(purposes)

        when: "we ask for the item with the intention of updating it"
        def response = handler.handle(item.id)

        then: "it's all good"
        noExceptionThrown()
        response.isPresent()

        and: "the returned item is the one we expected"
        response.get().item.id == item.id

        and: "all purposes get returned (and nothing else, for whatever reason)"
        response.get().purposes.containsAll(purposeNames)
        purposeNames.containsAll(response.get().purposes)

        when: "we ask for some non-existent item with the intention of updating it"
        response = handler.handle(999898989876775L)

        then: "it's not found"
        response.isEmpty()

        cleanup:
        db.delete(item)
        db.deleteAll(purposes)
    }
}
