package lodgings.domain.items.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.common.data.pagination.PaginationRequest
import lodgings.domain.items.data.entities.Item
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class ViewItemsHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    ViewItemsHandler handler

    def "it should find multiple items according to given pagination parameters"() {

        given: "some items exist"
        def items = [
                new Item("aaa", true, null, userHelper.findAdmin()),
                new Item("zzz", true, null, userHelper.findAdmin()),
                new Item("jjj", true, null, userHelper.findAdmin()),
                new Item("fff", true, null, userHelper.findAdmin()),
                new Item("bbb", true, null, userHelper.findAdmin()),
                new Item("qqq", true, null, userHelper.findAdmin())
        ]
        db.saveAll(items)

        when: "we query for a page of items"
        def response = handler.handle(new PaginationRequest(index, size, property, order))

        then: "we get expected results"
        response.isSuccess()
        response.body.data[0].name == expectedFirstItemName
        response.body.data.size() == expectedSize

        cleanup:
        db.deleteAll(items)

        where:
        index   | size  | property  | order         || expectedFirstItemName    | expectedSize
        0       | 3     | "name"    | "ASCENDING"   || "aaa"                    | 3
        1       | 5     | "name"    | "ASCENDING"   || "zzz"                    | 1
    }
}
