package lodgings.domain.settlement.services

import helpers.common.UnirestWrapper
import helpers.security.TokenManipulator
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import kong.unirest.HttpResponse
import lodgings.common.data.Failable
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.settlement.data.projections.SettlementProjection
import lodgings.domain.settlement.data.requests.PaymentConfirmationRequest
import lodgings.domain.settlement.services.scenarios.PaymentConfirmationHandler
import lodgings.domain.users.data.entities.Role
import spock.lang.Specification

import javax.inject.Inject
import java.time.YearMonth

@MicronautTest
class SettlementControllerSpec extends Specification {

    @Inject
    UnirestWrapper unirest

    @Inject
    TokenManipulator tokenManipulator

    @MockBean(PaymentConfirmationHandler)
    PaymentConfirmationHandler paymentConfirmationHandler() {
        Mock(PaymentConfirmationHandler)
    }

    @MockBean(SettlementViewer)
    SettlementViewer settlementViewer() {
        Mock(SettlementViewer)
    }

    @Inject
    PaymentConfirmationHandler handler

    @Inject
    SettlementViewer settlementViewer

    def "it should correctly accept or reject a payment confirmation request"() {

        given: "that we are logged in as a user"
        def token = tokenManipulator.fakeToken("unique", [role])

        and: "we want to confirm a payment"
        def request = new PaymentConfirmationRequest(true)

        when: "we send the request by POST"
        HttpResponse response = unirest.post("/api/settlements/confirm-payment/1")
                .header("Authorization", token)
                .body(request)
                .asEmpty()

        then: "the request is accepted/rejected accordingly, and handler is invoked, or not"
        invocations * handler.handle(1L, request) >> Failable.success(
                new SettlementProjection(new Settlement(), [])
        )
        response.status == status

        where:
        role        || invocations  | status
        Role.USER   || 1            | 200
        Role.ADMIN  || 0            | 403
        "HEHEHE"    || 0            | 403
    }

    def "it should correctly parse url for viewing a settlement and check roles"() {

        given: "that we are logged in as a user"
        def token = tokenManipulator.fakeToken("unique", [role])

        and: "we want some settlement"
        def year = 2003
        def month = 2

        when: "we request to GET it"
        def response = unirest.get("/api/settlements/year/$year/month/$month")
                .header("Authorization", token)
                .asEmpty()

        then: "it's ok and the settlement viewer gets called"
        response.status == status
        invocations * settlementViewer.get(YearMonth.of(year, month)) >> Optional.empty()

        where:
        role        || invocations  | status
        Role.USER   || 1            | 200
        Role.ADMIN  || 1            | 200
        "HEHEHE"    || 0            | 403
    }
}
