package lodgings.domain.settlement.services

import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject
import java.time.YearMonth

/**
 * @see SettlementKeeper#ensureCurrentSettlementExists(io.micronaut.discovery.event.ServiceReadyEvent)
 */
@MicronautTest
class SettlementKeeperSpec extends Specification {

    @Inject
    private Database db

    @Inject
    private SettlementKeeper keeper

    def "it should make sure current settlement month exists on startup, and correctly save an upcoming one"() {

        given: "there may or may not be some settlement months in the database"
        def countBefore = db.find(Settlement.class).findCount()

        expect: "there should be exactly one - the current one, just created (clean db @ test startup)"
        countBefore == 1

        and: "the only existing settlement month is for the current month, therefore (@see GroovyDoc) works"
        def currentSettlement = db.find(Settlement.class).findOne()
        currentSettlement.yearMonth == YearMonth.now()

        and: "it contains all the users supposed to be included in the next settlement"
        currentSettlement.users.size() == db.find(User).where().eq(User.Fields.autoIncludeInNextSettlement, true).findCount()

        when:
        keeper.cleanUpAndCreateNextSettlement()

        then: "there is a new settlement month in the database"
        db.find(Settlement.class).findCount() == countBefore + 1

        and: "it is for the next month"
        def nextSettlement = db.find(Settlement.class).where()
                .eq(Settlement.Fields.yearMonth, YearMonth.now().plusMonths(1))
                .findOne()

        nextSettlement != null
        nextSettlement.yearMonth.minusMonths(1) == YearMonth.now()

        and: "it contains all the users supposed to be included in the next settlement"
        nextSettlement.users.size() == db.find(User).where().eq(User.Fields.autoIncludeInNextSettlement, true).findCount()
    }
}
