package lodgings.domain.settlement.services.payments

import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.settlement.data.entities.Payment
import lodgings.domain.settlement.data.entities.Settlement
import spock.lang.Specification

import java.time.LocalTime
import java.time.YearMonth

import static lodgings.domain.settlement.services.payments.samples.Samples.*

class PaymentCalculatorSpec extends Specification {
    
    def calculator = new PaymentCalculator()
    
    static def expenses1 = [
            new Expense(new BigDecimal("0.8"), null, null, MICHAL, new Settlement(null, null)),
            new Expense(new BigDecimal("0.2"), null, null, KACPER, new Settlement(null, null)),
            new Expense(new BigDecimal("2.3"), null, null, MICHAL, new Settlement(null, null)),
            new Expense(new BigDecimal("5.7"), null, null, PRZEMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("3.3"), null, null, TOMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("1.8"), null, null, KACPER, new Settlement(null, null)),
            new Expense(new BigDecimal("7.1"), null, null, MICHAL, new Settlement(null, null))

            // Michał   10.2    gets paid   4.9
            // Kacper   2.0     pays others 3.3
            // Przemek  5.7     gets paid   0.4
            // Tomek    3.3     pays others 2.0
            // --------------------------------
            //          21.2 / 4 = 5.3
    ]

    static def users1 = [MICHAL, KACPER, PRZEMEK, TOMEK]

    static def expectedPayments1 = [
            new Payment(KACPER, MICHAL, new BigDecimal("2.90")),
            new Payment(TOMEK, MICHAL, new BigDecimal("2.00")),
            new Payment(KACPER, PRZEMEK, new BigDecimal("0.40"))
    ]

    static def expenses2 = [
            new Expense(new BigDecimal("2"), null, null, MICHAL, new Settlement(null, null)),
            new Expense(new BigDecimal("2"), null, null, MICHAL, new Settlement(null, null)),
            new Expense(new BigDecimal("2"), null, null, PRZEMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("3"), null, null, PRZEMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("2"), null, null, KACPER, new Settlement(null, null)),
            new Expense(new BigDecimal("2"), null, null, PRZEMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("1"), null, null, KACPER, new Settlement(null, null)),
            new Expense(new BigDecimal("5"), null, null, TOMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("1"), null, null, TOMEK, new Settlement(null, null)),
            new Expense(new BigDecimal("6"), null, null, MARTA, new Settlement(null, null)),
            new Expense(new BigDecimal("4"), null, null, MARTA, new Settlement(null, null))

            // Michał   4       pays others 2
            // Przemek  7       gets paid   1
            // Kacper   3       pays others 3
            // Tomek    6                   0
            // Marta    10      gets paid   4
            // ------------------------------
            //          30 / 5 = 6
    ]

    static def users2 = [MICHAL, KACPER, PRZEMEK, TOMEK, MARTA]

    static def expectedPayments2 = [
            new Payment(MICHAL, MARTA, new BigDecimal("2.00")),
            new Payment(KACPER, PRZEMEK, new BigDecimal("1.00")),
            new Payment(KACPER, MARTA, new BigDecimal("2.00"))
    ]

    static def expenses3 = [
            expense("3", KACPER)
    ]

    static def users3 = [KACPER, MICHAL, MARTA]

    static def expectedPayments3 = [
            payment(MICHAL, KACPER, "1.00"),
            payment(MARTA, KACPER, "1.00")
    ]

    static def expenses4 = [
            expense("2", TOMEK),
            expense("4", MARTA)
    ]

    static def users4 = [TOMEK, PRZEMEK, MARTA]

    static def expectedPayments4 = [
            payment(PRZEMEK, MARTA, "2.00")
    ]

    static def expenses5 = [
            expense("1.99", MICHAL)
    ]

    static def users5 = [KACPER, MICHAL, TOMEK]

    static def expectedPayments5 = [
            payment(KACPER, MICHAL, "0.66"),
            payment(TOMEK, MICHAL, "0.66")
    ]

    static def expenses6 = [
            expense("1.99", MICHAL),
            expense("0.99", MICHAL)
    ]

    static def users6 = [KACPER, MICHAL, TOMEK]

    static def expectedPayments6 = [
            payment(KACPER, MICHAL, "0.99"),
            payment(TOMEK, MICHAL, "0.99")
    ]

    static def expenses7 = [
            expense("2.01", MICHAL),
            expense("1.01", MICHAL)
    ]

    static def users7 = [KACPER, MICHAL, TOMEK]

    static def expectedPayments7 = [
            payment(KACPER, MICHAL, "1.01"),
            payment(TOMEK, MICHAL, "1.01")
    ]

    static def expenses8 = [
            expense("1.00", KACPER),
            expense("7.00", MICHAL),
            expense("7.00", TOMEK),
            expense("7.00", PRZEMEK)
    ]

    static def users8 = [KACPER, MICHAL, TOMEK, PRZEMEK, MARTA]

    static def expectedPayments8 = [
            payment(MARTA, PRZEMEK, "2.60"),
            payment(MARTA, TOMEK, "1.80"),
            payment(KACPER, TOMEK, "0.80"),
            payment(KACPER, MICHAL, "2.60")
    ]

    static def expenses9 = [
            expense("1.00", KACPER),
            expense("0.99", MICHAL)
    ]

    static def users9 = [KACPER, MICHAL]

    static def expectedPayments9 = []

    static def expenses10 = [
            expense("3.00", KACPER),
            expense("6.99", MICHAL)
    ]

    static def users10 = [
            KACPER,
            MICHAL,
            TOMEK,
            PRZEMEK,
            MARTA,
            AAA,
            BBB,
            CCC,
            DDD,
            EEE,
    ]

    static def expectedPayments10 = [
            payment(PRZEMEK, KACPER, "1.00"),
            payment(AAA, KACPER, "1.00"),
            payment(TOMEK, MICHAL, "1.00"),
            payment(BBB, MICHAL, "1.00"),
            payment(CCC, MICHAL, "1.00"),
            payment(DDD, MICHAL, "1.00"),
            payment(MARTA, MICHAL, "1.00"),
            payment(EEE, MICHAL, "1.00")
    ]

    static def expenses11 = [
            expense("9.99", KACPER),
            expense("16.00", MICHAL)
    ]

    static def users11 = users10

    static def expectedPayments11 = [
            payment(PRZEMEK, KACPER, "2.60"),
            payment(BBB, KACPER, "2.60"),
            payment(TOMEK, KACPER, "2.20"), // or 2.19, fine too
            payment(TOMEK, MICHAL, "0.40"),
            payment(AAA, MICHAL, "2.60"),
            payment(CCC, MICHAL, "2.60"),
            payment(DDD, MICHAL, "2.60"),
            payment(MARTA, MICHAL, "2.60"),
            payment(EEE, MICHAL, "2.60")
    ]

    static def expenses12 = [
            expense("9.99", KACPER),
            expense("16.00", MICHAL),
            expense("10.00", MARTA)
    ]

    static def users12 = users11

    static def expectedPayments12 = [
            payment(PRZEMEK, KACPER, "3.60"),
            payment(BBB, KACPER, "2.80"), // or 2.79, fine too
            payment(BBB, MARTA, "0.80"),
            payment(TOMEK, MARTA, "3.60"),
            payment(AAA, MARTA, "2.00"),
            payment(AAA, MICHAL, "1.60"),
            payment(CCC, MICHAL, "3.60"),
            payment(DDD, MICHAL, "3.60"),
            payment(EEE, MICHAL, "3.60")
    ]

    static def noExpenses = []
    static def someUsers = [MICHAL, MARTA, PRZEMEK]
    static def noUsers = []
    static def noPayments = []

    def "it should calculate payments correctly"() {

        given: "a settlement"
        def settlement = new Settlement(YearMonth.now(), users)
        settlement.setExpenses(expenses)

        when:
        println "testing payment calculation: $i"
        println "GO!   ${LocalTime.now()}"
        def payments = calculator.calculate(settlement)
        println "DONE! ${LocalTime.now()}\n"

        then:
        payments.containsAll(expectedPayments)
        expectedPayments.containsAll(payments)

        where:
        i   | expenses   | users     | expectedPayments
        1   | expenses1  | users1    | expectedPayments1
        2   | expenses2  | users2    | expectedPayments2
        3   | expenses3  | users3    | expectedPayments3
        4   | expenses4  | users4    | expectedPayments4
        5   | expenses5  | users5    | expectedPayments5
        6   | expenses6  | users6    | expectedPayments6
        7   | expenses7  | users7    | expectedPayments7
        8   | expenses8  | users8    | expectedPayments8
        9   | expenses9  | users9    | expectedPayments9
        10  | expenses10 | users10   | expectedPayments10
//        expenses11 | users11   | expectedPayments11 // hmmmm todo
//        expenses12 | users12   | expectedPayments12
        11  | noExpenses | someUsers | noPayments
        12  | noExpenses | noUsers   | noPayments
    }
}
