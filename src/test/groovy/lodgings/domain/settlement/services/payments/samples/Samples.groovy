package lodgings.domain.settlement.services.payments.samples

import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.settlement.data.entities.Payment
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.users.data.entities.User

class Samples {
    static def MICHAL = new User("michal", "Michał", null, null, true, true)
    static def KACPER = new User("kacper", "Kacper", null, null, true, true)
    static def PRZEMEK = new User("przemek", "Przemek", null, null, true, true)
    static def TOMEK = new User("tomek", "Tomek", null, null, true, true)
    static def MARTA = new User("marta", "Marta", null, null, true, true)
    static def AAA = new User("aaa", "Aaa", "", [], true, true)
    static def BBB = new User("bbb", "Bbb", "", [], true, true)
    static def CCC = new User("ccc", "Ccc", "", [], true, true)
    static def DDD = new User("ddd", "Ddd", "", [], true, true)
    static def EEE = new User("eee", "Eee", "", [], true, true)

    static Expense expense(String amount, User user) {
        new Expense(new BigDecimal(amount), null, null, user, new Settlement(null, null))
    }

    static Payment payment(User from, User to, String amount) {
        new Payment(from, to, new BigDecimal(amount))
    }
}
