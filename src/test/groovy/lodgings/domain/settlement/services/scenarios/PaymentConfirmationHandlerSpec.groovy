package lodgings.domain.settlement.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.settlement.data.entities.Payment
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.settlement.data.requests.PaymentConfirmationRequest
import spock.lang.Specification

import javax.inject.Inject
import java.time.YearMonth

import static lodgings.common.SyntacticSugar.not
import static lodgings.domain.settlement.data.errors.PaymentConfirmationErrors.*

@MicronautTest
class PaymentConfirmationHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Mock(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    PaymentConfirmationHandler handler

    def "it should (un)confirm payment correctly and if (not) all paid users confirm, (un)settle the settlement automatically"() {

        given: "there are some users besides admin (otherwise this app would be pretty boring)"
        def existing = userHelper.createAndSaveExistingUser()
        def unique = userHelper.createAndSaveUniqueUser()
        def admin = userHelper.findAdmin()

        and: "there are some payments"
        def payment1 = new Payment(existing, admin, BigDecimal.ONE)
        def payment2 = new Payment(unique, admin, BigDecimal.ONE)
        def payments = [payment1, payment2]

        and: "there is a past settlement"
        def yearMonth = YearMonth.now().minusMonths(1)
        def settlement = new Settlement(yearMonth, [existing, unique, admin])
        settlement.setPayments(payments)
        db.save(settlement)

        and: "the admin (payee in those payments) sends a request to confirm a payment"
        def request = new PaymentConfirmationRequest(true)

        when:
        def response = handler.handle(payment1.id, request)

        then: "it's ok"
        1 * currentUserService.get() >> admin
        response.isSuccess()

        and: "the payment is confirmed"
        db.refresh(payment1)
        payment1.received

        and: "the month is not settled"
        db.refresh(settlement)
        not(settlement.settled)

        when: "now admin confirms the other payment"
        response = handler.handle(payment2.id, request)

        then: "it's also ok"
        1 * currentUserService.get() >> admin
        response.isSuccess()

        and: "the other payment is confirmed"
        db.refresh(payment2)
        payment2.received

        and: "the settlement for that month is now settled"
        db.refresh(settlement)
        settlement.settled

        when: "now the admin changed their mind, one payment isn't actually received"
        response = handler.handle(payment1.id, new PaymentConfirmationRequest(false))

        then: "it's ok"
        1 * currentUserService.get() >> admin
        response.isSuccess()

        and: "the payment is un-confirmed"
        db.refresh(payment1)
        not(payment1.received)

        and: "the month is not settled, again"
        db.refresh(settlement)
        not(settlement.settled)

        cleanup:
        settlementHelper.delete(settlement)
        userHelper.delete(existing)
        userHelper.delete(unique)
    }

    def "it should reject payment confirmation correctly"() {

        given: "there are some users besides admin (otherwise this app would be pretty boring)"
        def existing = userHelper.createAndSaveExistingUser()
        def unique = userHelper.createAndSaveUniqueUser()
        def admin = userHelper.findAdmin()

        and: "there are some payments"
        def payment = new Payment(existing, admin, BigDecimal.ONE)
        def payments = [payment]

        and: "there is a past settlement"
        def yearMonth = YearMonth.now().minusMonths(1)
        def settlement = new Settlement(yearMonth, [existing, admin])
        settlement.setPayments(payments)
        db.save(settlement)

        when: "we try to confirm a non-existent payment"
        def response = handler.handle(983409223888L, new PaymentConfirmationRequest(true))
        
        then: "it's an error"
        1 * currentUserService.get() >> admin
        response.isError()
        
        and: "the error is..."
        response.errors == [PAYMENT_DOES_NOT_EXIST]

        when: "another user (not the payee in a payment) tries to confirm a payment"
        response = handler.handle(payment.id, new PaymentConfirmationRequest(true))

        then: "it's an error"
        1 * currentUserService.get() >> existing
        response.isError()

        and: "the error is..."
        response.errors == [USER_IS_NOT_THE_PAYEE]

        when: "yet another user (not even participating in the settlement) tries to confirm a payment"
        response = handler.handle(payment.id, new PaymentConfirmationRequest(true))

        then: "it's an error"
        1 * currentUserService.get() >> unique
        response.isError()

        and: "the error is..."
        response.errors == [USER_NOT_INCLUDED_IN_SETTLEMENT]

        cleanup:
        settlementHelper.delete(settlement)
        userHelper.delete(existing)
        userHelper.delete(unique)
    }
}
