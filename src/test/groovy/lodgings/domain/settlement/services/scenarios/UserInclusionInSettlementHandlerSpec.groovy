package lodgings.domain.settlement.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.settlement.data.errors.UserInclusionErrors
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject

import static lodgings.common.SyntacticSugar.not
import static lodgings.domain.settlement.data.errors.UserInclusionErrors.USER_NOT_FOUND

@MicronautTest
class UserInclusionInSettlementHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper

    @Inject
    UserInclusionInSettlementHandler handler

    def "it should correctly include or exclude users from the current settlement"() {

        given: "another user, initially not included in the current settlement ..."
        User user = userHelper.createAndSaveExistingUser()

        and: "... who is currently not allowed to login and does not have USER role"
        user.allowedToLogin = false
        user.roles.clear()
        db.save(user)
        user.roles = db.find(Role.class).where().eq(Role.Fields.name, Role.ADMIN).findList()
        db.save(user)

        and: "the current settlement"
        def settlement = settlementHelper.findCurrent()

        when: "we ask that the user be included in the settlement"
        def response = handler.handle(user.id, true)

        then: "it's not ok"
        response.isError()

        and: "there is information about what kind of error we encountered"
        response.errors == [UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE]

        when: "we make the user have the correct role and retry"
        user.roles.clear()
        db.save(user)
        user.roles = db.find(Role.class).where().eq(Role.Fields.name, Role.USER).findList()
        db.save(user)

        response = handler.handle(user.id, true)

        then: "it's ok"
        response.isSuccess()

        and: "the user is included in the settlement"
        response.body.users.stream().anyMatch { it.name == user.name }
        db.refresh(settlement)
        db.refresh(user)
        settlement.users.contains(user)

        and: "the user is now allowed to login"
        user.allowedToLogin

        when: "we ask that the user be excluded from the settlement"
        response = handler.handle(user.id, false)

        then: "it's ok"
        response.isSuccess()

        and: "the user is not included in the settlement"
        response.body.users.stream().noneMatch { it.name == user.name }
        db.refresh(settlement)
        not(settlement.users.contains(user))

        when: "we ask for some random, non-existent user to be excluded (or included, doesn't matter) from the settlement"
        response = handler.handle(4599038382900L, false)

        then: "it's an error ..."
        response.isError()

        and: "... more specifically"
        response.errors == [USER_NOT_FOUND]

        cleanup:
        userHelper.delete(user)
    }
}
