package lodgings.domain.settlement.services.scenarios

import helpers.settlements.TestSettlementHelper
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.domain.items.data.entities.Item
import lodgings.domain.expenses.data.entities.Expense
import lodgings.domain.settlement.data.entities.Settlement
import spock.lang.Specification

import javax.inject.Inject
import java.time.LocalDateTime
import java.time.YearMonth

import static lodgings.common.SyntacticSugar.not

@MicronautTest
class ViewSettlementHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @Inject
    TestSettlementHelper settlementHelper

    @Inject
    ViewSettlementHandler viewSettlementHandler

    def "it should view a settlement correctly"() {

        given: "an existing, old settlement"
        def existing = userHelper.createAndSaveExistingUser()
        def admin = userHelper.findAdmin()
        def settlement = new Settlement(YearMonth.of(2020, 4), [admin, existing])
        db.save(settlement)

        and: "there are some expenses"
        def item = new Item("item", false, null, admin)
        db.save(item)
        def expense = new Expense(
                new BigDecimal("1.00"),
                LocalDateTime.of(2020, 4, 11, 11, 11),
                item,
                admin,
                settlement
        )
        db.save(expense)

        when: "we check if the payments for the settlement are calculated"
        db.refresh(settlement)

        then: "they are not"
        not(settlement.paymentsCalculated)

        when: "we want to view it"
        def response = viewSettlementHandler.handle(YearMonth.of(2020, 4))

        then: "it is found"
        response.isPresent()

        and: "it contains the expected payments"
        response.get().payments.size() == 1
        response.get().payments[0].from.name == existing.name
        response.get().payments[0].from.id == existing.id
        response.get().payments[0].to.name == admin.name
        response.get().payments[0].to.id == admin.id
        response.get().payments[0].amount == new BigDecimal("0.50")

        and: "the payments have been calculated"
        db.refresh(settlement)
        settlement.paymentsCalculated

        when: "we want to view some non-existent settlement"
        response = viewSettlementHandler.handle(YearMonth.of(1920, 4))

        then: "it is not found"
        response.isEmpty()

        cleanup:
        settlementHelper.delete(settlement)
        db.delete(item)
        userHelper.delete(existing)
    }
}
