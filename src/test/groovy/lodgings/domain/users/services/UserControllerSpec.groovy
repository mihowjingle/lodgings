package lodgings.domain.users.services

import helpers.common.UnirestWrapper
import helpers.security.TokenManipulator
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import kong.unirest.GenericType
import kong.unirest.HttpResponse
import lodgings.common.data.Failable
import lodgings.common.data.errors.Error
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import lodgings.domain.users.data.requests.PasswordChangeRequest
import lodgings.domain.users.data.requests.UserCreationRequest
import lodgings.domain.users.data.requests.UserUpdateRequest
import lodgings.domain.users.services.scenarios.UserPasswordChangeHandler
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class UserControllerSpec extends Specification {

    @Inject
    Database db

    @Inject
    UnirestWrapper unirest

    @Inject
    TokenManipulator tokenManipulator

    @Inject
    TestUserHelper userHelper

    @MockBean(UserPasswordChangeHandler)
    UserPasswordChangeHandler userPasswordChangeHandler() {
        Mock(UserPasswordChangeHandler)
    }

    @Inject
    UserPasswordChangeHandler userPasswordChangeHandler

    def "only admin should be able to save a user"() {

        given: "that we are not logged in as admin"
        def token = tokenManipulator.fakeToken("admin", roles) // it's the roles that matters

        and: "some users may or may not be in the database"
        def countBefore = db.find(User.class).findCount()

        and: "we want to save a new user"
        UserCreationRequest payload = new UserCreationRequest("whatever", "Mr. Whoever", [Role.USER], false, false, false)

        when: "we send the new user by POST"
        HttpResponse response = unirest.post("/api/users")
                .header("Authorization", token)
                .body(payload)
                .asObject(new GenericType<List<Error>>() {})

        then: "it's not all good - forbidden"
        response.status == 403

        and: "there are no new users in the database"
        db.find(User.class).findCount() == countBefore

        where:
        roles                   | _
        [Role.USER]             | _
        [Role.RESET_PASSWORD]   | _
        ["WHATEVER"]            | _
        ["HUE HUE"]             | _
        []                      | _
    }

    def "only admin should be able to update a user"() {

        given: "that we are not logged in as admin"
        def token = tokenManipulator.fakeToken("admin", roles) // it's the roles that matters

        and: "we want to update a user (admin in this case, but whatever)"
        UserUpdateRequest payload = new UserUpdateRequest(
                "Mr. Whoever",
                false,
                false,
                true,
                [Role.USER]
        )

        when: "we send the request by PUT"
        HttpResponse response = unirest.put("/api/users/1")
                .header("Authorization", token)
                .body(payload)
                .asObject(new GenericType<List<String>>() {})

        then: "it's not all good - forbidden"
        response.status == 403

        where:
        roles                   | _
        [Role.USER]             | _
        [Role.RESET_PASSWORD]   | _
        ["WHATEVER"]            | _
        ["HUE HUE"]             | _
        []                      | _
    }

    def "only a user with the role USER or RESET_PASSWORD can change their password"() {

        given: "that a user with login 'unique' and password 'unique' exists"
        def user = userHelper.createAndSaveUniqueUser()

        and: "the user is logged in (only the roles really matter)"
        def token = tokenManipulator.fakeToken(login, roles)

        and: "there is a request to change the user's password"
        def request = new PasswordChangeRequest("oldPassword", "newPassword")

        when:
        HttpResponse response = unirest.post("/api/users/change-password")
                .header("Authorization", token)
                .body(request)
                .asEmpty()

        then: "we get the expected result"
        response.status == expectedStatus

        and: "the password change occurs (or doesn't even get as far, when the roles are wrong)"
        expectedInvocations * userPasswordChangeHandler.handle(request) >> Failable.success()

        cleanup:
        userHelper.delete(user)

        where:
        login         | roles                     || expectedStatus | expectedInvocations
        "unique"      | [Role.RESET_PASSWORD]     || 200            | 1
        "unique"      | [Role.USER]               || 200            | 1
        "unique"      | ["HUE HUE"]               || 403            | 0
        "unique"      | [Role.ADMIN]              || 403            | 0
    }
}
