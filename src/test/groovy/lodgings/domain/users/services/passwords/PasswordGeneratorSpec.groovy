package lodgings.domain.users.services.passwords

import spock.lang.Specification

class PasswordGeneratorSpec extends Specification {

    def generator = new PasswordGenerator()

    def "generated passwords should be between 5 and 7 characters long, inclusive"() {

        when:
        def password = generator.generate()

        then:
        println password
        password.length() >= 5
        password.length() <= 7

        where: "the test is repeated 20 times"
        i << (1..20)
    }
}
