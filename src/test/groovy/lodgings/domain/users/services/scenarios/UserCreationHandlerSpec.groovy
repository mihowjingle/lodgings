package lodgings.domain.users.services.scenarios

import at.favre.lib.crypto.bcrypt.BCrypt
import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.settlement.data.errors.UserInclusionErrors
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import lodgings.domain.users.data.errors.UserCreationErrors
import lodgings.domain.users.data.errors.UserPersistErrors
import lodgings.domain.users.data.requests.UserCreationRequest
import lodgings.domain.users.services.passwords.PasswordGenerator
import spock.lang.Specification

import javax.inject.Inject
import java.time.YearMonth

import static helpers.common.StaticHelpers.stringOfLength
import static lodgings.common.SyntacticSugar.not

@MicronautTest
class UserCreationHandlerSpec extends Specification {

    @Inject
    Database db

    @MockBean(PasswordGenerator)
    PasswordGenerator passwordGenerator() {
        Mock(PasswordGenerator)
    }

    @Inject
    PasswordGenerator passwordGenerator

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    TestUserHelper userHelper

    @Inject
    UserCreationHandler handler

    def "it should save a valid user"() {

        given: "that some users already exist"
        def countBefore = db.find(User.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to save a new user"
        UserCreationRequest request = new UserCreationRequest("unique", "Ms. Unique", List.of(Role.USER), false, false, false)

        when:
        def response = handler.handle(request)

        then: "it's all good"
        response.isSuccess()

        and: "we have one more user in the database"
        db.find(User.class).findCount() == countBefore + 1

        and: "that is indeed the user we just saved"
        def newUser = db.find(User.class).where().eq(User.Fields.login, "unique").findOne()
        newUser != null

        and: "they have a password"
        newUser.passwordHash != null

        and: "they are required to reset their password"
        newUser.passwordResetRequired

        and: "the password is generated as expected"
        1 * passwordGenerator.generate() >> "totallyRandomPassword"
        BCrypt.verifyer().verify("totallyRandomPassword".toCharArray(), newUser.getPasswordHash()).verified

        and: "the password is returned as response body"
        response.body == "totallyRandomPassword"

        cleanup:
        userHelper.delete(newUser)
    }

    def "it should not save an invalid user"() {

        given: "that some users already exist"
        def user = userHelper.createAndSaveExistingUser()
        def countBefore = db.find(User.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to save a new user"
        UserCreationRequest request = new UserCreationRequest(login, name, roles, false, true, true)

        when:
        def response = handler.handle(request)

        then: "it's not all good"
        response.isError()

        and: "there are no new users in the database"
        db.find(User.class).findCount() == countBefore

        and: "there are errors"
        response.errors == expectedErrors

        cleanup:
        userHelper.delete(user)

        where:
        login               | name                  | roles         || expectedErrors
        "existing"          | "Whatever"            | [Role.USER]   || [UserCreationErrors.LOGIN_NOT_UNIQUE]
        "aaa"               | "Whatever"            | [Role.USER]   || [UserCreationErrors.LOGIN_TOO_SHORT]
        stringOfLength(31)  | "Whatever"            | [Role.USER]   || [UserCreationErrors.LOGIN_TOO_LONG]
        null                | "Whatever"            | [Role.USER]   || [UserCreationErrors.LOGIN_MISSING]
        "aaaaaaaa"          | "W"                   | [Role.USER]   || [UserPersistErrors.NAME_TOO_SHORT]
        "aaaaaaaa"          | stringOfLength(41)    | [Role.USER]   || [UserPersistErrors.NAME_TOO_LONG]
        "aaaaaaaa"          | null                  | [Role.USER]   || [UserPersistErrors.NAME_MISSING]
        "aaaaaaaa"          | "Mr. Existing"        | [Role.USER]   || [UserPersistErrors.NAME_NOT_UNIQUE]
        "aaaaaaaa"          | "Whatever"            | []            || [UserPersistErrors.ROLES_MISSING]
        "aaaaaaaa"          | "Whatever"            | null          || [UserPersistErrors.ROLES_MISSING]
        "aaaaaaaa"          | "Whatever"            | ["HUE HUE"]   || [UserPersistErrors.ROLES_UNRECOGNIZED]
        "aaaaaaaa"          | "Whatever"            | [Role.ADMIN]  || [UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE]
    }

    def "it should add the user being created to current settlement, as requested"() {

        given: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "the may or may not be some users participating in the settlement for the current month already"
        def countBefore = db.find(Settlement).where()
                .eq(Settlement.Fields.yearMonth, YearMonth.now())
                .findOne()
                .users
                .size()

        and: "we want to save a new user"
        UserCreationRequest request = new UserCreationRequest("unique", "Ms. Unique", List.of(Role.USER), true, false, addToCurrentSettlement)

        when:
        def response = handler.handle(request)

        then: "it's all good"
        response.isSuccess()

        and: "that is indeed the user we just saved"
        def newUser = db.find(User.class).where().eq(User.Fields.login, "unique").findOne()
        newUser != null

        and: "a password is generated (doesn't even matter, but we have to return something - it's a mock)"
        1 * passwordGenerator.generate() >> "ble"

        and: "the new user is participating in the current settlement only when we asked for it"
        def users = db.find(Settlement).where()
                .eq(Settlement.Fields.yearMonth, YearMonth.now())
                .findOne()
                .users
        def countAfter = users.size()

        (countAfter == countBefore + 1) == addToCurrentSettlement
        users.contains(newUser) == addToCurrentSettlement

        cleanup:
        userHelper.delete(newUser)

        where:
        addToCurrentSettlement  | _
        true                    | _
        false                   | _
    }

    def "it should not add the user being created to current settlement, as requested"() {

        given: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "the may or may not be some users participating in the settlement for the current month already"
        def countBefore = db.find(Settlement).where()
                .eq(Settlement.Fields.yearMonth, YearMonth.now())
                .findOne()
                .users
                .size()

        and: "we want to save a new user"
        UserCreationRequest request = new UserCreationRequest("unique", "Ms. Unique", List.of(Role.USER), false, false, false)

        when:
        def response = handler.handle(request)

        then: "it's all good"
        response.isSuccess()

        and: "that is indeed the user we just saved"
        def newUser = db.find(User.class).where().eq(User.Fields.login, "unique").findOne()
        newUser != null

        and: "a password is generated (doesn't even matter, but we have to return something - it's a mock)"
        1 * passwordGenerator.generate() >> "ble"

        and: "the new user isn't participating in the current settlement"
        def users = db.find(Settlement).where()
                .eq(Settlement.Fields.yearMonth, YearMonth.now())
                .findOne()
                .users
        def countAfter = users.size()

        countAfter == countBefore
        not(users.contains(newUser))

        cleanup:
        userHelper.delete(newUser)
    }
}
