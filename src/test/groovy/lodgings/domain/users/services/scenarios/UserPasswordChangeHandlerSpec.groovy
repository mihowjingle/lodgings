package lodgings.domain.users.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import lodgings.domain.users.data.requests.PasswordChangeRequest
import spock.lang.Specification

import javax.inject.Inject

import static helpers.common.StaticHelpers.stringOfLength
import static lodgings.domain.users.data.errors.PasswordChangeErrors.*

@MicronautTest
class UserPasswordChangeHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    UserPasswordChangeHandler handler

    def "it should change a user's password (or not, accordingly)"() {

        given: "that a user with login 'mr_required' and password 'mr_required' exists"
        def user = userHelper.createAndSaveUserRequiredToChangePassword()

        and: "the user who wants to change their password, has some password already"
        def oldHash = db.find(User.class).where().eq(User.Fields.login, login).findOne().passwordHash

        and: "the user is logged in"
        currentUserService.get() >> user

        and: "there is a request to change the user's password"
        def request = new PasswordChangeRequest(oldPassword, newPassword)

        when: "the request is sent by POST"
        def response = handler.handle(request)

        then: "we get the expected result"
        response.isSuccess() == shouldBeSuccess
        response.errors == expectedErrors

        and: "the password is changed only when it was supposed to be"
        def newHash = db.find(User.class).where().eq(User.Fields.login, login).findOne().passwordHash
        (newHash != oldHash) == shouldBeSuccess

        and: "the password should no longer be required to be changed (or not, accordingly)"
        db.refresh(user)
        user.isPasswordResetRequired() != shouldBeSuccess

        cleanup:
        userHelper.delete(user)

        where:
        oldPassword     | newPassword           | login              | roles                     || shouldBeSuccess  | expectedErrors
        "mr_required"   | "mr_required1"        | "mr_required"      | [Role.RESET_PASSWORD]     || true             | null
        "mr_required"   | "mr_required1"        | "mr_required"      | [Role.USER]               || true             | null
        "mr_required"   | stringOfLength(5)     | "mr_required"      | [Role.USER]               || true             | null
        "mr_required"   | stringOfLength(50)    | "mr_required"      | [Role.USER]               || true             | null
        "mr_required"   | "mr_required"         | "mr_required"      | [Role.USER]               || false            | [NEW_PASSWORD_EQUALS_OLD]
        "wrong"         | "mr_required1"        | "mr_required"      | [Role.USER]               || false            | [AUTHENTICATION_FAILED]
        null            | "mr_required1"        | "mr_required"      | [Role.USER]               || false            | [AUTHENTICATION_FAILED]
        "mr_required"   | null                  | "mr_required"      | [Role.USER]               || false            | [NEW_PASSWORD_MISSING]
        "mr_required"   | stringOfLength(4)     | "mr_required"      | [Role.USER]               || false            | [NEW_PASSWORD_TOO_SHORT]
        "mr_required"   | stringOfLength(51)    | "mr_required"      | [Role.USER]               || false            | [NEW_PASSWORD_TOO_LONG]
    }

    def "it should reject password change request for inactive user"() {

        given: "that a user is logged in, but has been meanwhile deactivated"
        def user = userHelper.createAndSaveInactiveUser()
        currentUserService.get() >> user

        and: "there is a request to change the user's password"
        def request = new PasswordChangeRequest("mr_inactive", "newPassword")

        when:
        def response = handler.handle(request)

        then: "the request is rejected"
        response.isError()
        response.errors == [AUTHENTICATION_FAILED]

        cleanup:
        userHelper.delete(user)
    }
}
