package lodgings.domain.users.services.scenarios

import helpers.users.TestUserHelper
import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.data.errors.Error
import lodgings.common.services.CurrentUserService
import lodgings.domain.settlement.data.entities.Settlement
import lodgings.domain.settlement.data.errors.UserInclusionErrors
import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.entities.User
import lodgings.domain.users.data.errors.UserPersistErrors
import lodgings.domain.users.data.errors.UserUpdateErrors
import lodgings.domain.users.data.requests.UserUpdateRequest
import spock.lang.Specification

import javax.inject.Inject
import java.time.YearMonth

import static helpers.common.StaticHelpers.stringOfLength
import static java.util.stream.Collectors.toList

@MicronautTest
class UserUpdateHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    TestUserHelper userHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    UserUpdateHandler handler

    def "it should update user after a valid request"() {

        given: "that some users already exist"
        def existingUser = userHelper.createAndSaveExistingUser()
        def countBefore = db.find(User.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to update the existing user (with a new name)"
        UserUpdateRequest request = new UserUpdateRequest(
                "Mr. Updated",
                false,
                false,
                true,
                [Role.USER]
        )

        when:
        def response = handler.handle(existingUser.id, request)

        then: "it's all good"
        response.isSuccess()

        and: "the user's name is updated"
        db.find(User).where().idEq(existingUser.id).findOne().name == "Mr. Updated"

        and: "there are no new users in the database"
        db.find(User).findCount() == countBefore

        when: "we want to 'update' (but not really) the user with exactly the same data (specifically, name)"
        response = handler.handle(existingUser.id, request)

        then: "it should be ok (specifically, no false-positive name uniqueness error)"
        response.isSuccess()

        cleanup:
        userHelper.delete(existingUser)
    }

    def "it should remove a user from the current settlement if needed"() {

        given: "that some user already exists"
        def existingUser = userHelper.createAndSaveExistingUser()
        def countBefore = db.find(User.class).findCount()

        and: "the user is included in the current settlement"
        def currentSettlement = db.find(Settlement).where().eq(Settlement.Fields.yearMonth, YearMonth.now()).findOne()
        currentSettlement.users.add(existingUser)
        db.save(currentSettlement)

        and: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to update the existing user (with a new name)"
        UserUpdateRequest request = new UserUpdateRequest(
                "Mr. Updated",
                false,
                false,
                false,
                roles
        )

        when:
        def response = handler.handle(existingUser.id, request)

        then: "it's all good"
        response.isSuccess()

        and: "the user's name is updated"
        db.find(User).where().idEq(existingUser.id).findOne().name == "Mr. Updated"

        and: "there are no new users in the database"
        db.find(User).findCount() == countBefore

        and: "the user is no longer in the settlement"
        db.refresh(currentSettlement)
        currentSettlement.users.stream().noneMatch {it.id == existingUser.id }

        cleanup:
        userHelper.delete(existingUser)

        where:
        roles         | _
        [Role.ADMIN]  | _
        [Role.USER]   | _
    }

    def "it should update user to have more or fewer roles"() {

        given: "that some users already exist"
        def existingUser = userHelper.createAndSaveExistingUser()
        def countBefore = db.find(User.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to update the existing user (with a new role)"
        UserUpdateRequest request = new UserUpdateRequest(
                "Mr. Existing",
                false,
                false,
                false,
                [Role.USER, Role.ADMIN]
        )

        when:
        def response = handler.handle(existingUser.id, request)

        then: "it's all good"
        response.isSuccess()

        and: "the user's roles are updated"
        def actualRoles = db.find(User).where().idEq(existingUser.id).findOne().roles.stream()
                .map(role -> role.name)
                .collect(toList())
        actualRoles.containsAll([Role.ADMIN, Role.USER])
        [Role.ADMIN, Role.USER].containsAll(actualRoles)

        and: "there are no new users in the database"
        db.find(User).findCount() == countBefore

        when: "we want to update the existing user (revoking one role)"
        request = new UserUpdateRequest(
                "Mr. Existing",
                false,
                false,
                false,
                [Role.USER]
        )

        response = handler.handle(existingUser.id, request)

        then: "it's all good"
        response.isSuccess()

        and: "the user's roles are updated"
        def actualRolesAgain = db.find(User).where().idEq(existingUser.id).findOne().roles.stream()
                .map(role -> role.name)
                .collect(toList())
        actualRolesAgain.containsAll([Role.USER])
        [Role.USER].containsAll(actualRolesAgain)

        cleanup:
        userHelper.delete(existingUser)
    }

    def "it should not update user after an invalid request"() {

        given: "that the user to be updated already exists"
        def existingUser = userHelper.createAndSaveExistingUser()

        and: "another user whose name will be needed for unique name test exists"
        def otherUser = userHelper.createAndSaveUniqueUser()

        and: "there is 'some' number of users"
        def countBefore = db.find(User.class).findCount()

        and: "we are logged in as admin"
        currentUserService.get() >> existingUser

        and: "we want to update the existing user (with a new name)"
        UserUpdateRequest request = new UserUpdateRequest(name, false, false, true, roles)

        when: "we send the updated user by PUT"
        def response = handler.handle(existingUser.id, request)

        then: "it's not all good"
        response.isError()

        and: "the user is unchanged"
        def foundUser = db.find(User.class).where().idEq(existingUser.id).findOne()
        foundUser.name == existingUser.name
        foundUser.roles == existingUser.roles

        and: "there are no new users in the database"
        db.find(User.class).findCount() == countBefore

        and: "there are errors"
        response.errors == expectedErrors

        cleanup:
        userHelper.delete(existingUser)
        userHelper.delete(otherUser)

        where:
        name                  | roles         || expectedErrors
        "W"                   | [Role.USER]   || [UserPersistErrors.NAME_TOO_SHORT]
        stringOfLength(41)    | [Role.USER]   || [UserPersistErrors.NAME_TOO_LONG]
        null                  | [Role.USER]   || [UserPersistErrors.NAME_MISSING]
        "Ms. Unique"          | [Role.USER]   || [UserPersistErrors.NAME_NOT_UNIQUE]
        "Whatever"            | []            || [UserPersistErrors.ROLES_MISSING]
        "Whatever"            | null          || [UserPersistErrors.ROLES_MISSING]
        "Whatever"            | ["HUE HUE"]   || [UserPersistErrors.ROLES_UNRECOGNIZED]
        "Whatever"            | [Role.ADMIN]  || [UserInclusionErrors.SETTLEMENT_REQUIRES_USER_ROLE] // todo sign there is refactor needed
    }

    def "it should not update a non-existent user"() {

        given: "that we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to update a non-existent user"
        UserUpdateRequest request = new UserUpdateRequest(
                "Whatever",
                false,
                false,
                true,
                [Role.USER]
        )

        when: "we send the updated user by PUT"
        def response = handler.handle(id, request)

        then: "it's not all good"
        response.isError()

        and: "there are errors"
        response.errors == [expectedError]

        where:
        id      || expectedError
        123L    || UserUpdateErrors.USER_NOT_FOUND
        null    || Error.ID_MISSING
    }

    def "it should not update 'admin' not to have ADMIN role or to not be able to login"() {

        given: "that we are logged in as admin"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we want to revoke ADMIN role for admin"
        UserUpdateRequest request = new UserUpdateRequest("Whatever", false, false, true, [Role.USER])

        when:
        def response = handler.handle(1L, request)

        then: "it's not all good"
        response.isError()

        and: "admin is unchanged"
        def admin = db.find(User.class).where().eq(User.Fields.login, "admin").findOne()
        admin.roles.stream().map { it.name }.collect(toList()).containsAll([Role.USER, Role.ADMIN])

        and: "there are errors"
        def expectedErrors = [UserUpdateErrors.ADMIN_REVOKED_FOR_ADMIN, UserUpdateErrors.LOGIN_DISALLOWED_FOR_ADMIN]

        response.errors.containsAll(expectedErrors)
        expectedErrors.containsAll(response.errors)
    }
}
