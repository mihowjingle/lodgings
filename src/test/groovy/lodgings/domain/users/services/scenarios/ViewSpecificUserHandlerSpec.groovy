package lodgings.domain.users.services.scenarios

import helpers.users.TestUserHelper
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import lodgings.common.services.CurrentUserService
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class ViewSpecificUserHandlerSpec extends Specification {

    @Inject
    TestUserHelper userHelper

    @MockBean(CurrentUserService)
    CurrentUserService currentUserService() {
        Stub(CurrentUserService)
    }

    @Inject
    CurrentUserService currentUserService

    @Inject
    ViewSpecificUserHandler handler

    def "it should find a specific user by id"() {

        given: "that we are logged in as someone with the role to view users"
        currentUserService.get() >> userHelper.findAdmin()

        and: "we know the admin's id to be 1"
        def adminId = 1

        when: "we ask for the user with admin's id"
        def response = handler.handle(adminId)

        then: "we get admin"
        response.isPresent()
        response.get().login == "admin"
    }

    def "it should not find a non-existent user by id"() {

        given: "that we are logged in as someone with the role to view users"
        currentUserService.get() >> userHelper.findAdmin()

        when:
        def response = handler.handle(id)

        then: "there is no user with given id"
        response.isEmpty()

        where:
        id  | _
        777 | _
        333 | _
    }
}
