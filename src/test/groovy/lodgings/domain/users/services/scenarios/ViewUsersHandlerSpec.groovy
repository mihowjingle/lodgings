package lodgings.domain.users.services.scenarios

import io.ebean.Database
import io.micronaut.test.annotation.MicronautTest
import lodgings.common.data.pagination.PaginationErrors
import lodgings.common.data.pagination.PaginationRequest
import lodgings.domain.users.data.entities.User
import spock.lang.Specification

import javax.inject.Inject

import static helpers.common.StaticHelpers.*

@MicronautTest
class ViewUsersHandlerSpec extends Specification {

    @Inject
    Database db

    @Inject
    ViewUsersHandler handler

    def "it should find multiple users according to given pagination parameters"() {

        given: "exactly 10 additional (besides admin) users exist"
        def users = [
                new User("aaaaa", "Mr. Aaaaa", _ as String, [], false, false),
                new User("fffff", "Ms. Fffff", _ as String, [], false, false),
                new User("bbbbb", "Mr. Bbbbb", _ as String, [], false, false),
                new User("zzzzz", "Ms. Zzzzz", _ as String, [], false, false),
                new User("ccccc", "Mr. Ccccc", _ as String, [], false, false),
                new User("ppppp", "Ms. Ppppp", _ as String, [], false, false),
                new User("ttttt", "Mr. Ttttt", _ as String, [], false, false),
                new User("ddddd", "Ms. Ddddd", _ as String, [], false, false),
                new User("hhhhh", "Mr. Hhhhh", _ as String, [], false, false),
                new User("rrrrr", "Ms. Rrrrr", _ as String, [], false, false)
        ]
        db.saveAll(users)

        when: "we query for a page of users"
        def response = handler.handle(new PaginationRequest(index, size, property, order))

        then: "we get expected results"
        response.isSuccess()
        predicate(response.body.data)
        response.body.data.size() == expectedSize

        cleanup:
        db.deleteAll(users)

        where:
        index   | size  | property  | order         || predicate                    | expectedSize
        0       | 3     | "login"   | "ASCENDING"   || isSortedAscendingByLogin     | size
        0       | 3     | "login"   | "DESCENDING"  || isSortedDescendingByLogin    | size
        1       | 5     | "login"   | "ASCENDING"   || isSortedAscendingByLogin     | size
        1       | 5     | "login"   | "DESCENDING"  || isSortedDescendingByLogin    | size
        null    | 3     | "login"   | "ASCENDING"   || isSortedAscendingByLogin     | size
        0       | null  | "login"   | "ASCENDING"   || isSortedAscendingByLogin     | 10 // because that's the default size
        2       | 3     | null      | "ASCENDING"   || isNotSortedByLogin           | size
        0       | 50    | "login"   | null          || isNotSortedByLogin           | 11 // because that's how many there are
    }

    def "it should properly validate pagination parameters and reject request accordingly"() {

        when: "we query for a page of users"
        def response = handler.handle(new PaginationRequest(index, size, property, order))

        then: "we get expected results"
        response.isError()
        response.errors == [ expectedError ]

        where:
        index   | size  | property          | order         || expectedError
        -1      | 3     | "login"           | "ASCENDING"   || PaginationErrors.NEGATIVE_PAGE_INDEX
        0       | 0     | "allowedToLogin"  | "ASCENDING"   || PaginationErrors.NON_POSITIVE_PAGE_SIZE
        0       | -1    | "login"           | "DESCENDING"  || PaginationErrors.NON_POSITIVE_PAGE_SIZE
        7       | 30    | "hohoho"          | "ASCENDING"   || PaginationErrors.UNRECOGNIZED_SORT_PROPERTY
        7       | 30    | "passwordHash"    | "DESCENDING"  || PaginationErrors.UNRECOGNIZED_SORT_PROPERTY
        3       | 50    | "id"              | "MEH"         || PaginationErrors.UNRECOGNIZED_SORT_ORDER
    }
}
