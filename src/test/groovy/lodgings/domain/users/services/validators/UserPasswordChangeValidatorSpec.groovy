package lodgings.domain.users.services.validators

import lodgings.domain.users.data.errors.PasswordChangeErrors
import lodgings.domain.users.data.requests.PasswordChangeRequest
import spock.lang.Specification

import static helpers.common.StaticHelpers.stringOfLength

class UserPasswordChangeValidatorSpec extends Specification {

    def validator = new UserPasswordChangeValidator()

    def "it should correctly validate a password change request"() {

        given: "that we want to validate the request to change a user's password"
        def request = new PasswordChangeRequest(oldPassword, newPassword)

        expect: "the request gets validated correctly"
        validator.validate(request) == expectedError

        where:
        oldPassword     | newPassword           || expectedError
        _ as String     | stringOfLength(1)     || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_SHORT)
        _ as String     | stringOfLength(2)     || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_SHORT)
        _ as String     | stringOfLength(3)     || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_SHORT)
        _ as String     | stringOfLength(4)     || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_SHORT)
        _ as String     | stringOfLength(5)     || Optional.empty()
        _ as String     | stringOfLength(6)     || Optional.empty()
        _ as String     | stringOfLength(9)     || Optional.empty()
        _ as String     | stringOfLength(22)    || Optional.empty()
        _ as String     | stringOfLength(48)    || Optional.empty()
        _ as String     | stringOfLength(50)    || Optional.empty()
        _ as String     | stringOfLength(51)    || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_LONG)
        _ as String     | stringOfLength(52)    || Optional.of(PasswordChangeErrors.NEW_PASSWORD_TOO_LONG)
        _ as String     | null                  || Optional.of(PasswordChangeErrors.NEW_PASSWORD_MISSING)
        "exactlySame"   | "exactlySame"         || Optional.of(PasswordChangeErrors.NEW_PASSWORD_EQUALS_OLD)
        "something"     | "something"           || Optional.of(PasswordChangeErrors.NEW_PASSWORD_EQUALS_OLD)
    }
}
