package lodgings.domain.users.services.validators.atomic

import lodgings.domain.users.data.errors.UserCreationErrors
import lodgings.domain.users.data.requests.UserCreationRequest
import lodgings.domain.users.services.UserRepository
import spock.lang.Specification
import spock.lang.Unroll

class UserLoginValidatorSpec extends Specification {

    def userRepository = Stub(UserRepository)
    def validator = new UserLoginValidator(userRepository)

    @Unroll
    def "it should correctly validate user with login: #login"() {

        given: "only one user, with login 'existing' already exists"
        userRepository.userWithLoginExists(_ as String) >> (login == "existing") // or { String login -> login == "existing" } (for future reference)

        and: "we want to validate the request to create a new user"
        def request = new UserCreationRequest(login, _ as String, _ as List, false, false, false)

        expect: "the request gets validated correctly"
        validator.validate(request) == expected

        where:
        login                               || expected
        "existing"                          || Optional.of(UserCreationErrors.LOGIN_NOT_UNIQUE)
        ""                                  || Optional.of(UserCreationErrors.LOGIN_TOO_SHORT)
        "aaa"                               || Optional.of(UserCreationErrors.LOGIN_TOO_SHORT)
        "aaaa"                              || Optional.empty()
        "aaaaaaaaaaa"                       || Optional.empty()
        "aaaaaaaaaaaaaaa"                   || Optional.empty()
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"    || Optional.empty()
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"   || Optional.of(UserCreationErrors.LOGIN_TOO_LONG)
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" || Optional.of(UserCreationErrors.LOGIN_TOO_LONG)
        null                                || Optional.of(UserCreationErrors.LOGIN_MISSING)
    }
}
