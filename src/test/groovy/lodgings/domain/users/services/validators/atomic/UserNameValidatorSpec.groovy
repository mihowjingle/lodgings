package lodgings.domain.users.services.validators.atomic

import lodgings.domain.users.data.errors.UserPersistErrors
import lodgings.domain.users.data.requests.UserCreationRequest
import spock.lang.Specification
import spock.lang.Unroll

class UserNameValidatorSpec extends Specification {

    def validator = new UserNameValidator()

    @Unroll
    def "it should correctly validate user with name: #name"() {

        given: "that we want to validate the request to create a new user"
        def request = new UserCreationRequest(_ as String, name, _ as List, false, false, false)

        expect: "the request gets validated correctly"
        validator.validate(request) == expected

        where:
        name                                        || expected
        ""                                          || Optional.of(UserPersistErrors.NAME_TOO_SHORT)
        "W"                                         || Optional.of(UserPersistErrors.NAME_TOO_SHORT)
        "Wh"                                        || Optional.empty()
        "What"                                      || Optional.empty()
        "Whatever"                                  || Optional.empty()
        "Wwwwwwwwhatever"                           || Optional.empty()
        "Wwwwwwwwwwwwwwwwwwwwwhatever Whateverson"  || Optional.empty()
        "Wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwhatever" || Optional.of(UserPersistErrors.NAME_TOO_LONG)
        null                                        || Optional.of(UserPersistErrors.NAME_MISSING)
    }
}
