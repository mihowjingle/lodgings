package lodgings.domain.users.services.validators.atomic

import lodgings.domain.users.data.entities.Role
import lodgings.domain.users.data.errors.UserPersistErrors
import lodgings.domain.users.data.requests.UserCreationRequest
import spock.lang.Specification
import spock.lang.Unroll

class UserRolesValidatorSpec extends Specification {

    def validator = new UserRolesValidator()

    @Unroll
    def "it should correctly validate user with roles: #roles"() {

        given: "that we want to validate the request to create a new user"
        def request = new UserCreationRequest(_ as String, _ as String, roles, false, false, false)

        expect: "the request gets validated correctly"
        validator.validate(request) == expected

        where:
        roles                               || expected
        null                                || Optional.of(UserPersistErrors.ROLES_MISSING)
        []                                  || Optional.of(UserPersistErrors.ROLES_MISSING)
        [Role.ADMIN]                        || Optional.empty()
        [Role.USER]                         || Optional.empty()
        [Role.USER, Role.ADMIN]             || Optional.empty()
        ["HUE HUE"]                         || Optional.of(UserPersistErrors.ROLES_UNRECOGNIZED)
        [Role.ADMIN, Role.USER, "HUE HUE"]  || Optional.of(UserPersistErrors.ROLES_UNRECOGNIZED)
    }
}
