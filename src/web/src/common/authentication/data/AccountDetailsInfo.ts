import {Role} from "@/common/authentication/data/Role"

export interface AccountDetailsInfo {
    readonly loggedIn: boolean
    readonly login: string
    readonly id: number
    readonly name: string
    readonly roles: Array<Role>
    readonly expires: Date | null,
    readonly expiresInMilliseconds: number
}