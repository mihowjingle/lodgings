/**
 * Micronaut authentication response
 */
export interface AuthenticationResponse {
    readonly access_token: string
    readonly expires_in: number
    readonly roles: Array<string>
    readonly token_type: string
    readonly username: string
}