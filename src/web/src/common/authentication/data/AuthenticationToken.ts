import {Role} from "@/common/authentication/data/Role"

export class AuthenticationToken {
    readonly expiresAt: Date
    readonly issuedAt: Date
    readonly issuedBy: string
    readonly notBefore: Date
    readonly roles: Array<Role>
    readonly subject: string
    readonly userId: number
    readonly userName: string

    constructor(
        expiresAt: Date,
        issuedAt: Date,
        issuedBy: string,
        notBefore: Date,
        roles: Array<Role>,
        subject: string,
        userId: number,
        userName: string
    ) {
        this.expiresAt = expiresAt
        this.issuedAt = issuedAt
        this.issuedBy = issuedBy
        this.notBefore = notBefore
        this.roles = roles
        this.subject = subject
        this.userId = userId
        this.userName = userName
    }

    get isValid() {
        const now = new Date()
        return now < this.expiresAt
    }
}