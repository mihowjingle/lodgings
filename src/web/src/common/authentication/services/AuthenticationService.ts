import {AuthenticationRequest} from "@/common/authentication/data/AuthenticationRequest"
import {AuthenticationResponse} from "@/common/authentication/data/AuthenticationResponse"
import axios, {AxiosResponse} from "axios"
import router from "@/config/router"
import store from "@/config/store"
import {AuthenticationToken} from "@/common/authentication/data/AuthenticationToken"
import {PasswordChangeRequest} from "@/common/authentication/data/PasswordChangeRequest"
import {eventBus} from "@/common/events/EventBus"

function parseJwt(token: string): AuthenticationToken {
    const base64Url = token.split(".")[1]
    const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/")
    const jsonPayload = decodeURIComponent(atob(base64).split("").map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(""))

    const raw = JSON.parse(jsonPayload)
    return new AuthenticationToken(
        new Date(raw.exp * 1000),
        new Date(raw.iat * 1000),
        raw.iss,
        new Date(raw.nbf * 1000),
        raw.roles,
        raw.sub,
        raw.id,
        raw.name
    )
}

export const authenticationService = {
    getToken(): AuthenticationToken | null {
        const jwt = sessionStorage.getItem("token")
        return jwt == null ? null : parseJwt(jwt)
    },

    tryLogin(request: AuthenticationRequest): Promise<AxiosResponse<AuthenticationResponse>> {
        return axios.post("/api/login", request)
    },

    handleLoginSuccess(response: AuthenticationResponse) {
        store.commit("login", parseJwt(response.access_token))
        sessionStorage.setItem("token", response.access_token)
        router.push(sessionStorage.getItem("redirectAfterLogin") || "/")
    },

    logout() {
        store.commit("logout")
        sessionStorage.removeItem("token")
        sessionStorage.removeItem("redirectAfterLogin")
        if (router.currentRoute.path != "/login") {
            router.push("/login")
        }
        eventBus.logout()
    },

    tryChangePassword(request: PasswordChangeRequest): Promise<AxiosResponse<void>> {
        return axios.post("/api/users/change-password", request)
    }
}