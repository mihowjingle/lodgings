export class AsyncSuccess<T> {
    body: T

    constructor(body: T) {
        this.body = body
    }
}

export class AsyncFailure<T> {
    error: T

    constructor(error: T) {
        this.error = error
    }
}

export type AsyncResponse<A, B> = AsyncSuccess<A> | AsyncFailure<B>