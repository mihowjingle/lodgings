export interface VuetifyDictionaryEntry<T> {
    text: string
    value: T
}