export interface ApiError {
    key: string
    acceptableValue: any | undefined
}

const dictionary = {
    // user creation
    "LOGIN_NOT_UNIQUE": "Użytkownik o takim loginie już istnieje!",
    // user creation / update ...aaaand we have a collision here NAME_NOT_UNIQUE: todo #27
    "USER_NAME_NOT_UNIQUE": "Użytkownik o takim imieniu, lub pseudonimie just istnieje!",
    // user update / inclusion in settlement
    "USER_NOT_FOUND": "Nie odnaleziono takiego użytkownika!",
    // user update
    "ADMIN_REVOKED_FOR_ADMIN": "Oryginalnemu administratorowi nie można odebrać uprawnień! A Ty, a Ty... :)",
    "LOGIN_DISALLOWED_FOR_ADMIN": "Oryginalnemu administratorowi nie można zabronić się logować! A Ty, a Ty... :)",
    // password change
    "AUTHENTICATION_FAILED": "Niepoprawne (stare) hasło i/lub nieaktywne konto!",
    // item persist
    "NAME_NOT_UNIQUE": "Artykuł o takiej nazwie już istnieje!",
    // item deletion
    "ITEM_RELATED_TO_EXPENSES": "Nie można usunąć artykułu! Istnieją powiązane z nim wydatki.",
    // expense deletion
    "EXPENSE_NOT_FOUND_CANNOT_DETERMINE_SETTLEMENT": "Wygląda na to, że właśnie ktoś usunął ten wydatek! Wystarczy odświeżyć stronę :)"
}

function translateKey(apiErrorKey: string): string {
    const translatedKey = dictionary[apiErrorKey as keyof typeof dictionary]
    return translatedKey ? translatedKey : apiErrorKey
}

export function translate(apiError: ApiError) {
    let text = translateKey(apiError.key)
    if (apiError.acceptableValue) {
        text += ` Akceptowalna wartość: ${apiError.acceptableValue}`
    }
    return text
}