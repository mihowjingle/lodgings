import Vue from "vue"
import {NotificationConfig} from "@/common/events/Notifications"

const bus = new Vue()

export const eventBus = {

    logout() {
        bus.$emit("logout")
    },
    onLogout(action: () => void) {
        bus.$on("logout", action)
    },

    heartbeat() {
        bus.$emit("heartbeat")
    },
    onHeartbeat(action: () => void) {
        bus.$on("heartbeat", action)
    },

    notify(config: NotificationConfig) {
        bus.$emit("notify", config)
    },
    onNotify(action: (config: NotificationConfig) => void) {
        bus.$on("notify", action)
    },

    hideNotification() {
        bus.$emit("hideNotification")
    },
    onHideNotification(action: () => void) {
        bus.$on("hideNotification", action)
    }
}