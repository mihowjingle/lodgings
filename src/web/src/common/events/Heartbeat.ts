import store from "@/config/store"
import {authenticationService} from "@/common/authentication/services/AuthenticationService"
import {eventBus} from "@/common/events/EventBus"
import {notificationService} from "@/common/events/Notifications"

function refresh() {
    store.commit("heartbeat")
    eventBus.heartbeat()

    const userIsLoggedIn = store.getters.userIsLoggedIn
    if (!userIsLoggedIn) {
        authenticationService.logout()
    }

    const minutesUntilLogout = store.getters.loginExpiresInMilliseconds / 60000
    const warningNotShownYet = !store.getters.loginExpirationWarningAlreadyShown
    if (userIsLoggedIn && minutesUntilLogout <= 5 && warningNotShownYet) {
        store.commit("expirationWarningShown")
        notificationService.notify({
            message: "Niedługo nastąpi automatyczne wylogowanie!",
            type: "warning"
        })
    }
}

export const heartbeat = {
    start() {
        const token = authenticationService.getToken()
        if (token) {
            store.commit("login", token)
        }
        setInterval(refresh, 1000)
    }
}