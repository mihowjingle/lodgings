import store from "@/config/store"
import {randomString} from "@/common/functions/Random"

export const loadingService = {
    start(): string {
        const handle = randomString(20)      // todo maybe it doesn't need to be so random? we can allow a handle to appear twice in the same universe :P
        store.commit("startLoading", handle) //  or we can use an incrementing number, but... global variable?
        return handle
    },

    stop(handle: string) {
        store.commit("stopLoading", handle)
    }
}