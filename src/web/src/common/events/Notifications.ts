import {eventBus} from "@/common/events/EventBus"
import {AxiosError} from "axios"
import {ApiError, translate} from "@/common/events/Errors"

export type NotificationType = "success" | "info" | "warning" | "error"

export interface NotificationConfig {
    message: string
    details?: Array<string>
    type: NotificationType
    timeout?: number
    closeable?: boolean
}

export interface ApiErrorNotificationConfig {
    onStatus: number
    timeout?: number
    closeable?: boolean
}

const errorDetails = [
    "Co się ogólnie stało? Jaka akcja (np. kliknięcie przycisku) wywołała błąd?",
    "Opcjonalnie, bieżący czas. Spokojnie, nie musi być co do sekundy."
]

export const notificationService = {
    notify(config: NotificationConfig) {
        eventBus.notify(config)
    },
    unknownTechnicalError() {
        eventBus.notify({
            closeable: true,
            message: "Nieznany błąd aplikacji! Przekaż poniższe informacje administratorowi, dziękuję!",
            details: errorDetails,
            type: "error"
        })
    },
    unknownServerError(error: AxiosError) {
        const temporaryErrorDetails = [...errorDetails]
        const logCode = error.response?.data.logCode
        if (logCode) {
            temporaryErrorDetails.push(`I co najważniejsze, unikalny kod błędu: ${logCode}`)
        }
        eventBus.notify({
            closeable: true,
            message: `Nieznany błąd serwera! Przekaż poniższe informacje administratorowi, dziękuję!`,
            details: temporaryErrorDetails,
            type: "error"
        })
    },
    hide() {
        eventBus.hideNotification()
    },
    apiError(error: AxiosError<Array<ApiError>>, ...configs: Array<ApiErrorNotificationConfig>) {
        const matchedConfigs = configs.filter(config => config.onStatus == error.response?.status)
        if (matchedConfigs.length != 1) { // expect exact match
            notificationService.unknownServerError(error)
        } else {
            const config = matchedConfigs[0]
            const apiErrors = error.response!.data // at this point we know response will be there, because of .filter()
            if (apiErrors.length == 0) {
                notificationService.unknownServerError(error)
            } else {
                const title = apiErrors.length == 1 ? translate(apiErrors[0]) : "Wystąpiło kilka błędów:"
                eventBus.notify({
                    closeable: config.closeable,
                    message: title,
                    details: apiErrors.length > 1 ? apiErrors.map(translate) : undefined,
                    timeout: config.timeout,
                    type: "error"
                })
            }
        }
    }
}