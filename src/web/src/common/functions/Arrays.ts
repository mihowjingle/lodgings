export function pickRandomElement<T>(array: Array<T>): T {
    return array[Math.floor(Math.random() * array.length)]
}

export function isNotEmpty<T>(array: Array<T>): boolean {
    return array.length > 0
}