export function greetingBasedOnTimeOfDay(): string {
    const now = new Date()
    if (now.getHours() < 6) {
        return "Idź spać"
    } else if (now.getHours() > 18) {
        return "Dobry wieczór"
    }
    return "Witaj"
}