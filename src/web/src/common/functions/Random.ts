export function randomString(length: number) {
    let result = ""
    const source = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    for (let i = 0; i < length; i++) {
        result += source.charAt(Math.floor(Math.random() * source.length))
    }
    return result
}