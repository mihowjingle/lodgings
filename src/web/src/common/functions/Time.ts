import {YearMonth} from "@/settlements/data/YearMonth"

export function millisecondsUntil(when: Date): number {
    return when.getTime() - new Date().getTime()
}

export function atLeastTwoDigits(value: number): string | number {
    return value > 9 ? value : `0${value}`
}

export function format(milliseconds: number): string {
    const hours = Math.floor(milliseconds / 3600000)
    const minutes = Math.floor((milliseconds / 60000) % 60)
    const seconds = Math.floor((milliseconds / 1000) % 60)

    const mm = atLeastTwoDigits(minutes)
    const ss = atLeastTwoDigits(seconds)

    if (hours >= 1) {
        return `${hours}:${mm}:${ss}`
    }
    return `${mm}:${ss}`
}

export function notCurrentMonth(yearMonth: YearMonth): boolean {
    const now = new Date()
    const currentYear = now.getFullYear()
    const currentMonth = now.getMonth() + 1
    return yearMonth.month != currentMonth || yearMonth.year != currentYear
}