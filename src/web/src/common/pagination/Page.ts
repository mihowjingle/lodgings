export interface Page<ITEM> {
    readonly data: Array<ITEM>
    readonly pageIndex: number
    readonly pageSize: number
    readonly totalCount: number
    readonly totalPageCount: number
}