import {DataOptions} from "vuetify"
import {PaginationRequest} from "@/common/pagination/PaginationRequest"

export function translateVuetifyPagination(options: DataOptions): PaginationRequest {
    const {sortBy, sortDesc, page, itemsPerPage} = options
    return new PaginationRequest(
        page - 1,
        itemsPerPage,
        sortBy[0],
        sortDesc[0] ? "DESCENDING" : "ASCENDING"
    )
}