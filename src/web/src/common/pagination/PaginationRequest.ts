export class PaginationRequest {
    readonly index: number | null
    readonly size: number | null
    readonly property: string | null
    readonly order: string | null

    constructor(index: number | null, size: number | null, property: string | null, order: string | null) {
        this.index = index
        this.size = size
        this.property = property
        this.order = order
    }
}