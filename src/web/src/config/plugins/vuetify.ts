import Vue from "vue"
import Vuetify from "vuetify/lib"
import pl from "vuetify/src/locale/pl"

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#598aaf", // 598aaf, 847cb1, 696aaa, 643e2f, 8c8eaf, 318c6d, d46e2d, 92a733 ...? (3bafba/979f1e/7d93cb)
        secondary: "#424242",
        accent: "#72b2e3", // c48bc7, e89f22, bcbeec, 72b2e3 ...? or it should be just a slightly lighter version of "primary", i think (63d7e2/c9d150/afc5fd)
        error: "#f37979",
        info: "#77b8ee",
        success: "#5eba61",
        warning: "#efbd26"
      },
    },
  },
  lang: {
    locales: { pl },
    current: "pl",
  },
  icons: {
    iconfont: "fa",
  },
})
