import Vue from "vue"
import VueRouter, {Route, RouteConfig} from "vue-router"
import Settlements from "../../settlements/views/Settlements.vue"
import {authenticationService} from "@/common/authentication/services/AuthenticationService"
import Login from "@/app/views/Login.vue"
import Items from "@/items/views/Items.vue"
import NotFound from "@/app/views/NotFound.vue"
import {Role} from "@/common/authentication/data/Role"
import {notificationService} from "@/common/events/Notifications"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Settlements",
        component: Settlements,
        meta: {
            allowedRoles: ["USER", "ADMIN"] as Array<Role>,
            navigation: {
                label: "Rozliczenia",
                icon: "fas fa-calendar-alt"
            }
        }
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
        path: "/change-password",
        name: "ChangePassword",
        component: () => import("@/app/views/ChangePassword.vue"),
        meta: {
            allowedRoles: ["USER", "RESET_PASSWORD"] as Array<Role>
        }
    },
    {
        path: "/items",
        name: "Items",
        component: Items,
        meta: {
            allowedRoles: ["USER"] as Array<Role>,
            navigation: {
                label: "Artykuły",
                icon: "fas fa-check"
            }
        }
    },
    {
        path: "/users",
        name: "Users",
        component: () => import("@/users/views/Users.vue"),
        meta: {
            allowedRoles: ["ADMIN"] as Array<Role>,
            navigation: {
                label: "Użytkownicy",
                icon: "fas fa-user"
            }
        }
    },
    {
        path: "*",
        name: "NotFound",
        component: NotFound
    }
]

export interface NavigationRouteProjection {
    readonly label: string
    readonly icon: string
    readonly path: string
    readonly allowedRoles: Array<Role>
}

function toNavigationProjection(route: RouteConfig): NavigationRouteProjection | null {
    if (!route.meta || !route.meta.navigation) {
        return null
    }
    return {
        allowedRoles: route.meta.allowedRoles,
        icon: route.meta.navigation.icon,
        label: route.meta.navigation.label,
        path: route.path
    }
}

export const navigationRoutes = routes.map(route => toNavigationProjection(route)).filter(route => !!route) as Array<NavigationRouteProjection>

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.path != from.path) {
        notificationService.hide()
    }
    if (to.matched.some(route => !!route.meta.allowedRoles)) {
        const token = authenticationService.getToken()
        if (token?.isValid) {
            if (token.roles.every(role => role == "RESET_PASSWORD")) {
                if (to.path == "/change-password") {
                    next()
                } else {
                    next({path: "/change-password"})
                }
            } else {
                if (token.roles.some(role => to.meta.allowedRoles.includes(role))) {
                    next()
                } else {
                    next({path: "/not-found"})
                }
            }
        } else {
            sessionStorage.setItem("redirectAfterLogin", to.path)
            next({path: "/login"})
        }
    } else {
        next()
    }
})

export default router
