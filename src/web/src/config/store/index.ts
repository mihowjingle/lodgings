import Vue from "vue"
import Vuex from "vuex"
import {Role} from "@/common/authentication/data/Role"
import {AuthenticationToken} from "@/common/authentication/data/AuthenticationToken"
import {AccountDetailsInfo} from "@/common/authentication/data/AccountDetailsInfo"
import {millisecondsUntil} from "@/common/functions/Time"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginSession: {
      valid: false,
      validUntil: null as Date | null,
      validForMilliseconds: 0,
      login: "???",
      id: 0,
      name: "???",
      roles: [] as Array<Role>,
      expirationWarningAlreadyShown: false
    },
    loadingHandle: null as string | null,
    screenTitle: ""
  },
  mutations: { // sync
    login(state, token: AuthenticationToken) {
      state.loginSession.valid = millisecondsUntil(token.expiresAt) > 0
      state.loginSession.validUntil = token.expiresAt
      state.loginSession.validForMilliseconds = millisecondsUntil(token.expiresAt)
      state.loginSession.login = token.subject
      state.loginSession.id = token.userId
      state.loginSession.name = token.userName
      state.loginSession.roles = token.roles
    },
    heartbeat(state) {
      const until = state.loginSession.validUntil
      state.loginSession.validForMilliseconds = until ? millisecondsUntil(until) : 0
      state.loginSession.valid = until ? millisecondsUntil(until) > 0 : false
    },
    expirationWarningShown(state) {
      state.loginSession.expirationWarningAlreadyShown = true
    },
    logout(state) {
      state.loginSession = {
        valid: false,
        validUntil: null,
        validForMilliseconds: 0,
        login: "???",
        id: 0,
        name: "???",
        roles: [],
        expirationWarningAlreadyShown: false
      }
    },
    startLoading(state, handle: string) {
      state.loadingHandle = handle
    },
    stopLoading(state, handle: string) {
      if (handle == state.loadingHandle) {
        state.loadingHandle = null
      }
    },
    setScreenTitle(state, title: string) {
      state.screenTitle = title
    }
  },
  actions: { // async
  },
  modules: {
  },
  getters: {
    accountDetailsInfo(state): AccountDetailsInfo {
      return {
        expires: state.loginSession.validUntil,
        expiresInMilliseconds: state.loginSession.validForMilliseconds,
        loggedIn: state.loginSession.valid,
        login: state.loginSession.login,
        id: state.loginSession.id,
        name: state.loginSession.name,
        roles: state.loginSession.roles
      }
    },
    userIsLoggedIn(state): boolean {
      return state.loginSession.valid
    },
    userRoles(state): Array<Role> {
      return state.loginSession.roles
    },
    loginExpiresInMilliseconds(state) {
      return state.loginSession.validForMilliseconds
    },
    loginExpirationWarningAlreadyShown(state): boolean {
      return state.loginSession.expirationWarningAlreadyShown
    },
    showGlobalLoader(state): boolean {
      return state.loadingHandle != null
    },
    screenTitle(state): string {
      return state.screenTitle
    }
  }
})
