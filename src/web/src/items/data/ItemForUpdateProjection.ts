import {ItemProjection} from "@/items/data/ItemProjection"

export interface ItemForUpdateProjection {
    item: ItemProjection
    purposes: Array<string>
}