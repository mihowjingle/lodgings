export interface ItemProjection {
    readonly id: number
    readonly name: string
    readonly assessable: boolean
    readonly purposeName: string
    readonly lastModifiedBy: string
}