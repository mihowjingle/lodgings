export const nameRules = [
    (value: string | null) => !!value || "Nazwa jest wymagana!",
    (value: string | null) => (value || "").length >= 4 || "Nazwa musi mieć przynajmniej 4 znaki!",
    (value: string | null) => (value || "").length <= 50 || "Nazwa musi mieć co najwyżej 50 znaków!"
]

export const purposeNameRules = [
    (value: string | null) => !value || value.length >= 4 || "Nazwa zastosowania musi mieć przynajmniej 4 znaki!",
    (value: string | null) => !value || value.length <= 50 || "Nazwa zastosowania musi mieć co najwyżej 50 znaków!"
]