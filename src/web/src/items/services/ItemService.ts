import {PaginationRequest} from "@/common/pagination/PaginationRequest"
import axios, {AxiosResponse} from "axios"
import {ItemProjection} from "@/items/data/ItemProjection"
import {Page} from "@/common/pagination/Page"
import {ItemPersistRequest} from "@/items/data/ItemPersistRequest"
import {ItemForUpdateProjection} from "@/items/data/ItemForUpdateProjection"

export const itemService = {
    getItems(request: PaginationRequest): Promise<AxiosResponse<Page<ItemProjection>>> {
        return axios.get("/api/items", {params: request})
    },

    tryCreateItem(request: ItemPersistRequest): Promise<AxiosResponse<void>> {
        return axios.post("/api/items", request)
    },

    getPurposes(): Promise<AxiosResponse<Array<string>>> {
        return axios.get("/api/items/purposes")
    },

    tryDeleteItem(id: number): Promise<AxiosResponse<void>> {
        return axios.delete(`/api/items/${id}`)
    },

    getItemForUpdate(id: number): Promise<AxiosResponse<ItemForUpdateProjection>> {
        return axios.get(`/api/items/${id}/for-update`)
    },

    tryUpdateItem(id: number, request: ItemPersistRequest): Promise<AxiosResponse<void>> {
        return axios.put(`/api/items/${id}`, request)
    }
}