import Vue from "vue"
import App from "./app/views/App.vue"
import router from "./config/router"
import store from "./config/store"
import vuetify from "./config/plugins/vuetify"
import "roboto-fontface/css/roboto/roboto-fontface.css"
import "@fortawesome/fontawesome-free/css/all.css"
import axios from "axios"

Vue.config.productionTip = false

axios.interceptors.request.use(config => {
  const jwt = sessionStorage.getItem("token")
  if (jwt != null) {
    config.headers.Authorization = "Bearer " + jwt
  }
  return config
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app")
