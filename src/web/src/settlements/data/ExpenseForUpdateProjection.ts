export interface ExpenseForUpdateProjection {
    readonly id: number
    readonly totalPrice: number
    readonly whenBought: string
    readonly item: string
    readonly availableItems: Array<string>
}