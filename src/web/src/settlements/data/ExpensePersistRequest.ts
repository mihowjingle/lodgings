export class ExpensePersistRequest {
    itemName: string
    totalPrice: number

    constructor(itemName: string, totalPrice: number) {
        this.itemName = itemName
        this.totalPrice = totalPrice
    }
}