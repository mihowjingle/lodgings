export interface ExpenseProjection {
    id: number
    totalPrice: number
    whenBought: string
    item: string
    buyer: SettlementUserProjection
}

export type PaymentStatus = "FORECAST" | "SETTLED" | "RECEIVED"

export interface PaymentProjection {
    id: number
    from: SettlementUserProjection
    to: SettlementUserProjection
    amount: number
    status: PaymentStatus
}

export interface SettlementUserProjection {
    id: number
    name: string
}

export interface AdminSettlementUserProjection {
    id: number
    login: string
    name: string
}

export interface SettlementProjection {
    expenses: Array<ExpenseProjection>
    payments: Array<PaymentProjection>
    users: Array<SettlementUserProjection>
    yearMonth: string
    settled: boolean
}