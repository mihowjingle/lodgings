import {atLeastTwoDigits} from "@/common/functions/Time"

export class YearMonth {
    year: number
    month: number

    constructor(year: number, month: number) {
        this.year = year
        this.month = month
    }

    static now(): YearMonth {
        const now = new Date()
        return new YearMonth(now.getFullYear(), now.getMonth() + 1)
    }

    get value() {
        const month = atLeastTwoDigits(this.month)
        return `${this.year}-${month}`
    }

    set value(val: string) {
        if (val.length != 7) {
            throw new Error("Unexpected YearMonth format! (expected: yyyy-MM)")
        }
        this.year = Number(val.split("-")[0])
        this.month = Number(val.split("-")[1])
    }

    increment() {
        if (this.month == 12) {
            this.year++
            this.month = 1
        } else {
            this.month++
        }
    }

    decrement() {
        if (this.month == 1) {
            this.month = 12
            this.year--
        } else {
            this.month--
        }
    }
}