export function zloty(value: number): string {
    return value.toFixed(2).replaceAll(".", ",") + " zł"
}

// very "dumb" implementation,
// since dates come from the backend as yyyy-MM-ddThh:mm:ss.micro
// but I think I'd rather have this, instead of pulling a whole big library
// such as moment, for such a little use case (up to change)
export function date(value: string): string {
    const chunks = value.replaceAll("T", " ").split(":") // [yyyy-MM-ddThh, mm, ss.micro]
    return chunks[0] + ":" + chunks[1]
}
