export function fractionDigits(n: number): number {
    const chunk = n.toString().split('.')[1]
    return chunk ? chunk.length : 0
}

export const itemRules = [
    (value: string | null) => !!value || "Artykuł jest wymagany!"
]

export const totalPriceRules = [
    (value: number | null) => !!value || "Cena (całkowita) jest wymagana!",
    (value: number | null) => (value && value <= 9999.99) || "Cena (całkowita) jest za duża!",
    (value: number | null) => (value && value >= 0.01) || "Cena (całkowita) jest za mała!",
    (value: number | null) => (value && fractionDigits(value) <= 2) || "Liczba groszy może być co najwyżej dwucyfrowa!"
]