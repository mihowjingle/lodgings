import axios, {AxiosError, AxiosResponse} from "axios"
import {AdminSettlementUserProjection, SettlementProjection} from "@/settlements/data/SettlementProjections"
import {ItemDictionaryProjection} from "@/settlements/data/ItemDictionaryProjection"
import {ExpensePersistRequest} from "@/settlements/data/ExpensePersistRequest"
import {ExpenseForUpdateProjection} from "@/settlements/data/ExpenseForUpdateProjection"
import {AsyncFailure, AsyncResponse, AsyncSuccess} from "@/common/data/AsyncResponse"

export const settlementService = {
    getSettlement(year: number, month: number): Promise<AxiosResponse<SettlementProjection>> {
        return axios.get(`/api/settlements/year/${year}/month/${month}`)
    },

    getItemDictionary(): Promise<AxiosResponse<Array<ItemDictionaryProjection>>> {
        return axios.get("/api/items/dictionary")
    },

    trySaveExpense(request: ExpensePersistRequest): Promise<AxiosResponse<SettlementProjection>> {
        return axios.post("/api/expenses", request)
    },

    tryDeleteExpense(id: number): Promise<AxiosResponse<SettlementProjection>> {
        return axios.delete(`/api/expenses/${id}`)
    },

    tryUpdateExpense(id: number, request: ExpensePersistRequest): Promise<AxiosResponse<SettlementProjection>> {
        return axios.put(`/api/expenses/${id}`, request)
    },

    getExpenseForUpdate(id: number): Promise<AxiosResponse<ExpenseForUpdateProjection>> {
        return axios.get(`/api/expenses/${id}/for-update`)
    },

    tryConfirmPayment(id: number, received: boolean): Promise<AxiosResponse<SettlementProjection>> {
        return axios.post(`/api/settlements/confirm-payment/${id}`, {received})
    },

    // experiment
    getUsersNotInCurrentSettlement(): Promise<AsyncResponse<Array<AdminSettlementUserProjection>, AxiosError>> {
        return axios.get("/api/users/not-in-current-settlement")
            .then(response => new AsyncSuccess(response.data))
            .catch((error: AxiosError) => new AsyncFailure(error))
    },

    tryIncludeUserInCurrentSettlement(id: number, include: boolean = true): Promise<AxiosResponse<SettlementProjection>> {
        return axios.post(`/api/settlements/user/${id}/include/${include}`)
    }
}