import {Role} from "@/common/authentication/data/Role"

export class UserCreationRequest {
    name: string | null = null
    roles: Array<Role> = []
    login: string | null = null
    allowedToLogin = true
    autoIncludeInNextSettlement = false
    addToCurrentSettlement = false
}