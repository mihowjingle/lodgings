import {Role} from "@/common/authentication/data/Role"

export interface UserProjection {
    readonly id: number
    readonly login: string
    readonly name: string
    readonly passwordResetRequired: boolean
    readonly allowedToLogin: boolean
    readonly autoIncludeInNextSettlement: boolean
    readonly roles: Array<Role>
}