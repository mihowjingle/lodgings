import {Role} from "@/common/authentication/data/Role"

export class UserUpdateRequest {
    name = ""
    roles: Array<Role> = []
    passwordResetRequired = false
    allowedToLogin = false
    autoIncludeInNextSettlement = false
}