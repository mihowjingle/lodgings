import {Role} from "@/common/authentication/data/Role"
import {VuetifyDictionaryEntry} from "@/common/data/VuetifyDictionaryEntry"

function translate(role: Role) {
    if (role == "RESET_PASSWORD") {
        return "Tylko zmiana hasła"
    }
    if (role == "ADMIN") {
        return "Administrator"
    }
    if (role == "USER") {
        return "Użytkownik"
    }
    throw new Error("Unexpected user role!")
}

export function translateRoles(roles: Array<Role>) {
    return roles.map(role => translate(role)).join(", ")
}

export type AssignableRole = VuetifyDictionaryEntry<Role>

export const ASSIGNABLE_ROLES: Array<AssignableRole> = [
    {
        text: "Administrator",
        value: "ADMIN"
    },
    {
        text: "Użytkownik",
        value: "USER"
    }
]