import {Role} from "@/common/authentication/data/Role"

export const nameRules = [
    (value: string | null) => !!value || "Imię lub pseudonim jest wymagany!",
    (value: string | null) => (value || "").length >= 2 || "Imię lub pseudonim musi mieć przynajmniej 2 znaki!",
    (value: string | null) => (value || "").length <= 40 || "Imię lub pseudonim musi mieć co najwyżej 40 znaków!"
]

export const roleRules = [
    (value: Array<Role>) => value.length > 0 || "Użytkownik musi mieć przynajmniej jedną rolę!"
]