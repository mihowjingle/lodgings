import axios, {AxiosResponse} from "axios"
import {PaginationRequest} from "@/common/pagination/PaginationRequest"
import {UserProjection} from "@/users/data/UserProjection"
import {Page} from "@/common/pagination/Page"
import {UserCreationRequest} from "@/users/data/UserCreationRequest"
import {UserUpdateRequest} from "@/users/data/UserUpdateRequest"

export const userService = {
    getUsers(request: PaginationRequest): Promise<AxiosResponse<Page<UserProjection>>> {
        return axios.get("/api/users", {params: request})
    },

    tryCreateUser(request: UserCreationRequest): Promise<AxiosResponse<string>> {
        return axios.post("/api/users", request)
    },

    getUser(id: number): Promise<AxiosResponse<UserProjection>> {
        return axios.get(`/api/users/${id}`)
    },

    tryUpdateUser(id: number, request: UserUpdateRequest): Promise<AxiosResponse<void>> {
        return axios.put(`/api/users/${id}`, request)
    }
}