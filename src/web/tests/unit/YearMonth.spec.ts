import {YearMonth} from "@/settlements/data/YearMonth"

interface Test {
    year: number
    month: number
    value: string
}

const tests: Array<Test> = [
    {year: 2001, month: 11, value: "2001-11"},
    {year: 2021, month: 7, value: "2021-07"},
    {year: 1992, month: 12, value: "1992-12"},
    {year: 2038, month: 11, value: "2038-11"},
    {year: 2009, month: 1, value: "2009-01"},
    {year: 2000, month: 8, value: "2000-08"}
]

describe("output", () => {
    for (const test of tests) {
        it(`year: ${test.year} and month: ${test.month} -> ${test.value}`, () => {
            expect(new YearMonth(test.year, test.month).value).toBe(test.value)
        })
    }
})

describe("input", () => {
    for (const test of tests) {
        it(`${test.value} -> year: ${test.year} and month: ${test.month}`, () => {
            const yearMonth = new YearMonth(1111, 11) // some date not in test cases
            yearMonth.value = test.value
            expect(yearMonth.year).toBe(test.year)
            expect(yearMonth.month).toBe(test.month)
        })
    }
})

describe("mutability", () => {
    it("should synchronize changes between (month and year) and value", () => {
        const yearMonth = new YearMonth(2000, 9)
        expect(yearMonth.value).toBe("2000-09")
        yearMonth.month = 3
        expect(yearMonth.value).toBe("2000-03")
        yearMonth.year = 1997
        expect(yearMonth.value).toBe("1997-03")
        yearMonth.value = "2006-10"
        expect(yearMonth.year).toBe(2006)
        expect(yearMonth.month).toBe(10)
    })
    it("should increment correctly", () => {
        const yearMonth = new YearMonth(2008, 11)
        yearMonth.increment()
        expect(yearMonth.value).toBe("2008-12")
        yearMonth.increment()
        expect(yearMonth.value).toBe("2009-01")
        yearMonth.increment()
        yearMonth.increment()
        yearMonth.increment()
        expect(yearMonth.value).toBe("2009-04")
    })
    it("should decrement correctly", () => {
        const yearMonth = new YearMonth(2024, 1)
        yearMonth.decrement()
        expect(yearMonth.value).toBe("2023-12")
        yearMonth.decrement()
        expect(yearMonth.value).toBe("2023-11")
        yearMonth.decrement()
        yearMonth.decrement()
        expect(yearMonth.value).toBe("2023-09")
    })
})