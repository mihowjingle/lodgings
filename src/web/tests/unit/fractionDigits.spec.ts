import {fractionDigits} from "@/settlements/functions/Validation"

interface Test {
    value: number
    expectedDigits: number
}

const tests: Array<Test> = [
    {value: 0,      expectedDigits: 0},
    {value: 1,      expectedDigits: 0},
    {value: 0.9,    expectedDigits: 1},
    {value: 0.11,   expectedDigits: 2},
    {value: 0.01,   expectedDigits: 2},
    {value: 0.9999, expectedDigits: 4},
    {value: 77,     expectedDigits: 0},
    {value: 900,    expectedDigits: 0},
    {value: 900.00, expectedDigits: 0}, // js, I guess
    {value: 900.20, expectedDigits: 1},
]

describe("fractionDigits(number) should correctly determine the number of digits after decimal point", () => {
    for (const test of tests) {
        it(`${test.value} -> ${test.expectedDigits}`, () => {
            expect(fractionDigits(test.value)).toBe(test.expectedDigits)
        })
    }
})